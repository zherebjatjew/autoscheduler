School timetable creator
========================

Intro
-----

A web application for managing learning process. Schedules learning table, distributes resources (rooms, teachers) across learning groups respecting learning plan.

Details
-------

The implementation is based on constraint programming. Uses mysql database. Has HAL-styled API.

Links
-----

* [Closest analog](http://www.galaktika.ru/vuz/)
* [CP framework](http://choco-solver.org/)
* [Spring Boot](http://projects.spring.io/spring-boot/)
* [HAL REST API](http://stateless.co/hal_specification.html)

![Build status](https://codeship.com/projects/0846e270-717f-0133-5599-06b1c29ec1d7/status?branch=develop)
