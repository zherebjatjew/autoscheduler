package org.autoschedule.domain;

import java.util.Set;

public interface LimitationAware {
    Set<Limitation> getLimitations();
}
