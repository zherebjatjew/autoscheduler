package org.autoschedule.domain;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.UUID;

/**
 * Base of all repositories.
 */
@NoRepositoryBean
public interface StreamedRepository<T> extends PagingAndSortingRepository<T, UUID> {
    @PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
    <S extends T> S save(S var1);
    @PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
    <S extends T> Iterable<S> save(Iterable<S> var1);
    @PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
    void delete(UUID var1);
    @PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
    void delete(T var1);
    @PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
    void delete(Iterable<? extends T> var1);
    @PreAuthorize("hasRole('SUPERUSER')")
    void deleteAll();
}

