package org.autoschedule.domain;

import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base for all models that have name.
 *
 * {@see org.autoschedule.api.AbstractNamedRepository}
 */
@NoRepositoryBean
public interface NamedStreamedRepository<T> extends StreamedRepository<T> {
	T findByName(String name);
}
