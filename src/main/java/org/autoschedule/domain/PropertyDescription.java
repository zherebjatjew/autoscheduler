package org.autoschedule.domain;

import lombok.Builder;
import lombok.Getter;

/**
 * Description of single property.
 */
@Builder
@Getter
public class PropertyDescription {

    public static final transient String VALUE_PROVIDER_SUFFIX = ".provider";
    public static final transient String NAME_SUFFIX = ".name";
    public static final transient String DESCRIPTION_SUFFIX = ".description";
    public static final transient String UUID_SUFFIX = ".uuid";

    /**
     * Original property name.
     */
    private String id;
    /**
     * Localized property name.
     */
	private String name;

    /**
     * Localized property description.
     */
	private String description;

    /**
     * Property value.
     */
    private String value;

    /**
     * URI returning list of possible values.
     */
    private String valueProvider;
}
