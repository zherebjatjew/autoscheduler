package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.autoschedule.service.util.TemplateBuilder;
import org.autoschedule.util.NumericComparator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * Team: group of students that has consistent set of subjects.
 * <p>A student may belong to more than one group: main group, foreign language, PE, etc.</p>
 */
@Entity
@Getter
@Table(name = "xgroup")
@ToString(of = {"title", "year"})
public class Team extends AbstractEntity implements Comparable<Team> {

    private static final long serialVersionUID = -8726352905384886318L;

    /**
     * Participants of the team.
     */
    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinTable(name = "student_groups",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private Set<Student> students = new HashSet<>();

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "team",
            orphanRemoval = true,
            cascade = {CascadeType.REFRESH, CascadeType.PERSIST})
    private List<Event> events = new ArrayList<>();

    @OneToMany(
            mappedBy = "team",
            orphanRemoval = true)
    private List<SubjectHour> hours;

    /**
     * Studying year. Can be used in {@link Team#getTitle()}: <code>"{year}A"</code>.
     */
    @Setter
    private Integer year;

    /**
     * Parent team if the team is a sub-team.
     * E.g. {@code 8A -> 8A English}
     */
    @OneToOne
    @Setter
    private Team parent;

    /**
     * Format string to show team title.
     *
     * @see Team#getYear()
     * @see Team#getFormattedTitle()
     */
    @Setter
    private String title;

    public String getFormattedTitle() {
        return new TemplateBuilder()
                .attribute("year", getYear())
                .build(getTitle());
    }

    /**
     * @return number of participants.
     */
    public int count() {
        return students.size();
    }

    public int getDepth() {
        return getParent() == null ? 1 : 1 + getParent().getDepth();
    }

    @Override
    public int compareTo(final Team o) {
        if ((getParent() == null && o.getParent() == null) || (getParent() != null && getParent().equals(o.getParent()))) {
            return NumericComparator.compare(getFormattedTitle(), o.getFormattedTitle());
        } else {
            Team ancestor = this;
            List<Team> chain1 = new LinkedList<>();
            while (ancestor != null) {
                chain1.add(0, ancestor);
                ancestor = ancestor.getParent();
            }
            ancestor = o;
            List<Team> chain2 = new LinkedList<>();
            while (ancestor != null) {
                chain2.add(0, ancestor);
                ancestor = ancestor.getParent();
            }
            final Iterator<Team> i1 = chain1.iterator();
            final Iterator<Team> i2 = chain2.iterator();
            while (i1.hasNext() && i2.hasNext()) {
                final Team t1 = i1.next();
                final Team t2 = i2.next();
                if (!t1.equals(t2)) {
                    return NumericComparator.compare(t1.getFormattedTitle(), t2.getFormattedTitle());
                }
            }
            return Integer.compare(chain1.size(), chain2.size());
        }
    }
}
