package org.autoschedule.domain;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

/**
 * {@link Event} repository.
 */
public interface EventRepository extends StreamedRepository<Event> {
	@Modifying
	@Query("delete from Event as e where e.plan.id=:owner")
	void clean(@Param("owner") UUID ownerPlanId);
}
