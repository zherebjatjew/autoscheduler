package org.autoschedule.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyClass;
import javax.persistence.MapKeyColumn;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Generic limitations.
 *
 * <p>
 * Specific implementations of this interface introduce schedule limitations. Some examples:
 * <ul>
 *     <li>no more than <code>n</code> events per day</li>
 *     <li>no empty time between events</li>
 * </ul>
 * </p>
 * <h3>How to implement</h3>
 * <p>
 *     Derive your class from {@link Limitation} oneOf {@link org.autoschedule.service.limitations.AbstractLimitation}.
 *     Use the latter if your needs solving context. Do not declare any extra non-transient fields.
 *     Put your data into {@link Limitation#properties}.
 * </p>
 * <p>
 *     Annotate with {@link org.springframework.stereotype.Component} if you need spring dependencies.
 *     Annotate with
 *     <code>
 *         \@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
 *         \@DiscriminatorValue(value = "Overlapping")
 *     </code>
 *     to enable reading your class from generic limitations table.
 *     Add your class to hibernate.cfg.xml.
 * </p>
 * <p>
 *     If you want your limitations to be added to any plan, create DB record of your limitations with plan=0.
 * </p>
 * <p>
 *     Create DB records of your limitations for each plan you want to have your limitations.
 *     Add limitations properties to constraint_properties table.
 * </p>
 */
@Entity
@Table(name = "xconstraint")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Limitation extends AbstractEntity {

	private static final long serialVersionUID = 8475731245746872781L;

	public static final Integer DEFAULT_WEIGHT = 9999;

	// http://stackoverflow.com/questions/3393649/storing-a-mapstring-string-using-jpa
	@ElementCollection(fetch = FetchType.LAZY)
	@MapKeyClass(String.class)
	@MapKeyColumn(name = "param")
	@CollectionTable(name = "constraint_meta", joinColumns = @JoinColumn(name = "constraint_id"))
	@Column(name = "value")
	private Map<String, String> properties = new HashMap<>();

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter
	@Setter
	private Plan plan;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private Teacher teacher;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private Subject subject;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private Team team;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private Student student;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private Room room;

	/**
	 * Weight of limitation.
	 * <p>All limitations of the same type must have the same weights.</p>
	 * <p>Weight defines limitation's priority, so limitations with higher weights
	 * are satisfied first.</p>
	 */
	@Getter
	@Setter
	private Integer weight = DEFAULT_WEIGHT;

	@PostLoad
	private void onLoad() {
		if (weight == null) {
			weight = DEFAULT_WEIGHT;
		}
	}

	/**
	 * Get limitations-specific property. Properties are used to tune limitations.
	 * E.g. you can define which classes you'd like to keep sequential.
	 *
	 * @param name property name
	 * @return property value
	 */
	public String getProperty(@NonNull final String name) {
		return properties.get(name);
	}


	/**
	 * Get property names.
	 *
	 * @return names of all existing properties.
	 */
	@NonNull
    @JsonIgnore
	public Collection<String> getPropertyNames() {
		return Collections.unmodifiableSet(properties.keySet());
	}

    /**
     * Get detailed properties.
     *
     * @return map property id to property description
     */
    @NonNull
    public Map<String, PropertyDescription> getProperties() {
        return properties.entrySet().stream()
                .map(name -> PropertyDescription.builder().id(name.getKey()).value(name.getValue()).build())
                .collect(Collectors.toMap(PropertyDescription::getId, v -> v));
    }

    public void setProperties(final Map<String, String> properties) {
        this.properties = new HashMap<>(properties);
    }

	public void setProperty(@NonNull final String name, final String value) {
		properties.put(name, value);
	}

    /**
     * @return Localized name of the limitation.
     */
    @NonNull
    public abstract String getName();

    /**
     * @return limitation's category.
     */
    @NonNull
    public abstract String getCategory();

    /**
     * @return Description of the limitation's purpose.
     */
    @NonNull
    @Transient
    public abstract String getDescription();

    protected Map<String, String> getPropertyValues() {
        return properties;
    }
}
