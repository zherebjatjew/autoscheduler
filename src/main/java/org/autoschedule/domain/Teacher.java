package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Single teacher.
 *
 * <p>Is not derived from {@link AbstractNamedEntity} to allow duplicate names</p>
 */
@Entity
@Getter
@Table(name = "teacher")
public class Teacher extends AbstractEntity {

	private static final long serialVersionUID = 3786377013243475153L;

	/**
	 * Teacher's name.
	 */
	@Setter
	@Column(unique = true, nullable = false, length = 255)
	private String name;

	/**
	 * Subject that the teacher is able to lead.
	 */
	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinTable(name = "teacher_subjects",
			joinColumns = {@JoinColumn(name = "teacher")},
			inverseJoinColumns = {@JoinColumn(name = "subject")})
	private Set<Subject> subjects;

	@OneToMany(
			cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH},
			orphanRemoval = true,
			mappedBy = "teacher")
	private Set<Limitation> limitations;

	@OneToMany(
			cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
	@JoinColumn(name = "teacher")
	private Set<Event> events;
}
