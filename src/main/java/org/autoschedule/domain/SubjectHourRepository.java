package org.autoschedule.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface SubjectHourRepository extends StreamedRepository<SubjectHour> {

    @Query("select s from SubjectHour h join h.subject s join s.types t "
           + "where h.plan = :plan and h.team = :team and :type in (t)")
    Set<Subject> getSubjectsOf(
            @Param("plan") final Plan plan,
            @Param("team") final Team team,
            @Param("type") final SubjectType type);

    @Query("select sum(h.hours) from SubjectHour h where h.team = :team and h.plan = :plan and h.subject = :subject "
           + "group by h.plan, h.team, h.subject")
    Integer getHours(
            @Param("plan") final Plan plan,
            @Param("team") final Team team,
            @Param("subject") final Subject subject);
}
