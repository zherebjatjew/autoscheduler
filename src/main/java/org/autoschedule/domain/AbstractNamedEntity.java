package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Base entity for models that have name.
 */
@MappedSuperclass
@Getter
@Setter
@ToString(of = {"name"}, callSuper = true)
public class AbstractNamedEntity extends AbstractEntity implements Comparable<AbstractNamedEntity> {

	private static final long serialVersionUID = 3600476221070051037L;

	/**
	 * Entity name.
	 */
	@Column(unique = true, nullable = false, length = 255)
	private String name;

	@Override
	public int compareTo(final AbstractNamedEntity o) {
		return o == null ? -1 : getName().compareTo(o.getName());
	}
}
