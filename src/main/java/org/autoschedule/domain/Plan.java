package org.autoschedule.domain;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NonNull;

/**
 * Learning plan: set of learning subjects with amount of hours per period.
 */
@Entity
@Getter
@Table(name = "plan")
public class Plan extends AbstractEntity {

	private static final long serialVersionUID = -3473489599556764601L;

	@OneToMany(
			targetEntity = SubjectHour.class,
			cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "plan")
	private Set<SubjectHour> subjects = new HashSet<>();
	@OneToMany(orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "plan")
	private Set<Event> events;
	@OneToMany(orphanRemoval = true, mappedBy = "plan", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<Limitation> limitations;

	public void setEvents(@NonNull final Iterable<Event> timetable) {
		events.clear();
		for (final Event evt : timetable) {
			evt.setPlan(this);
			events.add(evt);
		}
	}

	public void setHours(@NonNull final Subject subject, @NonNull final Team team, @NonNull final Integer hours) {
		if (hours == 0) {
			SubjectHour sh = new SubjectHour();
			sh.setPlan(this);
			sh.setSubject(subject);
			sh.setTeam(team);
			subjects.remove(sh);
		} else {
			if (hours < 0) {
				throw new IllegalArgumentException("Negative hour counts are not allowed");
			}
			SubjectHour sh = new SubjectHour();
			sh.setPlan(this);
			sh.setSubject(subject);
			sh.setTeam(team);
			Optional<SubjectHour> item = subjects.stream().filter(it -> it.equals(sh)).findFirst();
			if (item.isPresent()) {
				item.get().setHours(hours);
			} else {
				sh.setHours(hours);
				subjects.add(sh);
			}
		}
	}

	@NonNull
	public Integer getHoursOf(@NonNull final Subject subject, final Team team) {
		final SubjectHour sh = new SubjectHour();
		sh.setPlan(this);
		sh.setSubject(subject);
		sh.setTeam(team);
		final Optional<SubjectHour> item = subjects
				.stream()
				.filter(it -> subject.equals(it.getSubject()) && (team == null || team.equals(it.getTeam())))
				.findFirst();
		return item.isPresent() ? item.get().getHours() : 0;
	}
}
