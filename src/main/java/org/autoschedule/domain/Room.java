package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;


/**
 * Learning room.
 */
@Entity
@Getter
@Setter
@Table(name = "room")
public class Room extends AbstractNamedEntity implements LimitationAware {

	private static final long serialVersionUID = -1910889214426350948L;

	/**
	 * How many seats it has.
	 */
	private int capacity;

	@OneToMany(orphanRemoval = true,
			mappedBy = "room",
			cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private Set<Limitation> limitations;

	@OneToMany(
			cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
	@JoinColumn(name = "room")
	private Set<Event> events;
}
