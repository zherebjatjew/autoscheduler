package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;
import org.autoschedule.dao.MapToEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.UUID;


/**
 * {@link Team} repository.
 */
public interface TeamRepository extends StreamedRepository<Team> {

    /**
     * Batch getter of team sizes.
     *
     * @param concernedTeams teams to examine
     * @return {teamId, teamSize} tuples
     */
    @RestResource(exported = false)
    @MapToEntity(targetClass = TeamSize.class, value = {"teamId", "studentCount"})
    @Query("select t.id, count(*) from Team as t join t.students where t.id in :teamIds group by t.id")
    Iterable<TeamSize> getTeamSizes(@Param("teamIds") Iterable<UUID> concernedTeams);

    Iterable<Team> findByTitle(String title);

    /**
     * Tuple for returning team size.
     */
    @Getter
    @Setter
    class TeamSize {
        private UUID teamId;
        private Long studentCount;
    }
}
