package org.autoschedule.domain;

/**
 * This interface marks limitation as default.
 * <p>Default limitations are created while DB reset. They do not have mandatory properties and do not
 * rely on plan.</p>
 */
public interface DefaultLimitation {
}
