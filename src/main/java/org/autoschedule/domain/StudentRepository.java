package org.autoschedule.domain;


/**
 * {@link Student} repository.
 */
public interface StudentRepository extends NamedStreamedRepository<Student> {
}

