package org.autoschedule.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Holds how many hours of subject should have team according to learning {@link Plan}.
 */
@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"plan", "team", "subject"}, callSuper = false)
@Table(name = "subject_hours")
public class SubjectHour extends AbstractEntity {

	private static final long serialVersionUID = -933499655035719790L;

	/**
	 * Source learning plan.
	 */
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JsonIgnore
	private Plan plan;

	/**
	 * Subject enforced.
	 */
	@ManyToOne(cascade = CascadeType.REFRESH)
	private Subject subject;

	/**
	 * The Team this instance belongs to.
	 */
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "xgroup")
	private Team team;

	/**
	 * How many hours of the {@link SubjectHour#getSubject()} has to have {@link SubjectHour#getTeam()}.
	 */
	private Integer hours;
}
