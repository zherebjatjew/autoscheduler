package org.autoschedule.domain;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * {@link Subject} repository.
 */
public interface SubjectRepository extends NamedStreamedRepository<Subject> {
    @Query("select s from Subject s join s.types t where t.name=:typeName")
    Set<Subject> findSubjectsOfType(@Param("typeName") String typeName);
}
