package org.autoschedule.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.autoschedule.api.Index;
import org.autoschedule.dao.IndexConverter;

/**
 * Timetable entry.
 * <p>Hour can have more than one events: for each room</p>
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "event")
public class Event extends AbstractEntity implements Comparable<Event> {

	private static final long serialVersionUID = 8195855278216210735L;

	/**
	 * Host plan.
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	private Plan plan;

	/**
	 * Event location.
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	private Room room;

	/**
	 * Teacher involved.
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	private Teacher teacher;

	/**
	 * Team involved.
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	private Team team;

	/**
	 * Subject of the event.
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	private Subject subject;

	/**
	 * Event week/day/hour.
	 */
	@Convert(converter = IndexConverter.class)
	private Index position;

	@Override
	public int compareTo(@NonNull final Event o) {
		if (getPosition() == null) {
			return o.getPosition() == null ? 0 : 1;
		}
		return position.compareTo(o.getPosition());
	}
}
