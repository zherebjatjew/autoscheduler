package org.autoschedule.domain;


/**
 * {@link Teacher} repository.
 */
public interface TeacherRepository extends NamedStreamedRepository<Teacher> {
}
