package org.autoschedule.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

/**
 * {@link Limitation} repository.
 */
public interface LimitationRepository extends StreamedRepository<Limitation> {

	@Query("select l from Limitation l where l.plan=null or l.plan.id=:id")
	Iterable<Limitation> findByPlan(@Param("id") UUID planId);
}
