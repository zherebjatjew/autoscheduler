package org.autoschedule.domain;


/**
 * {@link Room} repository.
 */
public interface RoomRepository extends NamedStreamedRepository<Room> {
}
