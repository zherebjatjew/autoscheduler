package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * Account.
 */
@Getter
@Setter
@Entity
public class Account {
    @Id
    private String login;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
}
