package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Entity
@Table(name = "subject_type")
public class SubjectType extends AbstractNamedEntity {
    private static final long serialVersionUID = -6817244670879512639L;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    @JoinTable(name = "subject_types_subject",
            joinColumns = {@JoinColumn(name = "type_id")},
            inverseJoinColumns = {@JoinColumn(name = "subject_id")})
    private Set<Subject> subjects;

    /**
     * Defines weight for subjects of the type.
     * <p>Subjects with higher weight will "sink" in timetable: they tend to be put lower.</p>
     * <p>Sequence for subjects with the same weight is not defined.</p>
     * <p>{@code null} weights are ignored.</p>
     * <p>If subject has multiple weights, minimal value is taken.</p>
     */
    @Setter
    private Integer weight;
}
