package org.autoschedule.domain;

import org.autoschedule.dao.MapToEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Set;
import java.util.UUID;

/**
 * {@link Plan} repository.
 */
public interface PlanRepository extends StreamedRepository<Plan> {

	/**
	 * Calculates total number of hours on each {team, subject} in given plan.
	 *
	 * @param id id of {@link Plan}
	 * @return totals
	 */
	@RestResource(exported = false)
	@MapToEntity(targetClass = SubjectHour.class, value = {"plan", "team", "subject", "hours"})
	@Query("select s.plan, s.team, s.subject, sum(s.hours) as hours from SubjectHour as s "
			+ "join s.plan as p where p.id=:id group by s.plan, s.team, s.subject")
	Iterable<SubjectHour> getHourSubtotals(@Param("id") UUID id);

	@RestResource(exported = false)
	@Query("select distinct s.team from Plan as p join p.subjects as s where p.id=:id")
	Set<Team> getConcernedTeams(@Param("id") UUID planId);

	@RestResource(exported = false)
	@Query("select distinct s.subject from Plan as p join p.subjects as s where p.id=:id")
	Set<Subject> getConcernedSubjects(@Param("id") UUID planId);

	@RestResource(exported = false)
	@Query("select distinct t from Plan as p join p.subjects as s join s.subject as c join c.teachers as t where p.id=:id")
	Set<Teacher> getConcernedTeachers(@Param("id") UUID planId);
}
