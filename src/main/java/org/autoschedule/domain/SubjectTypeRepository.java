package org.autoschedule.domain;

/**
 * {@link Subject} types.
 */
public interface SubjectTypeRepository extends NamedStreamedRepository<SubjectType> {
}
