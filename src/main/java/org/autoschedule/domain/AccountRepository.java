package org.autoschedule.domain;

import org.springframework.data.repository.Repository;

public interface AccountRepository extends Repository<Account, String> {
    Account findOne(String login);
}
