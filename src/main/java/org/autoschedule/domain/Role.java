package org.autoschedule.domain;

public enum Role {
    STUDENT,
    TEACHER,
    MANAGER,
    SUPERUSER
}
