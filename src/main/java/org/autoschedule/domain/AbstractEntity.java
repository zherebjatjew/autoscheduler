package org.autoschedule.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.hateoas.Identifiable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.UUID;

/**
 * Base entity for all persistent objects.
 */
@MappedSuperclass
@Getter
@ToString
@EqualsAndHashCode(of = {"id"})
public class AbstractEntity implements Identifiable<UUID>, Serializable {

	private static final long serialVersionUID = 435434371996231368L;

	/**
	 * Internal ID.
	 */
    // http://vladmihalcea.com/2014/07/01/hibernate-and-uuid-identifiers/
    @Id
    @GeneratedValue(generator = "uuid-gen")
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    // We could leave id uninitialized, but then two new entities
    // will be considered as versions of one instance.
    private UUID id = UUID.randomUUID();

	/**
	 * Object version.
	 */
	@Version
	@JsonIgnore
	private Long version;

	protected AbstractEntity() {
	}
}
