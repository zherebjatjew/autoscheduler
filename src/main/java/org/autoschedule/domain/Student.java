package org.autoschedule.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


/**
 * Single student.
 *
 * <p>Is not derived from {@link AbstractNamedEntity} to allow duplicate names</p>
 *
 */
@Entity
@Getter
@Table(name = "student")
public class Student extends AbstractEntity {

	private static final long serialVersionUID = -6424497972125150869L;

	/**
	 * Student's name.
	 */
	@Setter
	@Column(unique = false, nullable = false, length = 255)
	private String name;

	/**
	 * Sex.
	 */
	@Setter
	private Boolean male;

	/**
	 * List of {@link Team}s the student belongs to.
	 */
	@ManyToMany(mappedBy = "students", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Team> teams = new HashSet<>();
}
