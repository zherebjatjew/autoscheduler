package org.autoschedule.domain;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Learning subject.
 */
@Entity
@Getter
@Table(name = "subject")
public class Subject extends AbstractNamedEntity {

	private static final long serialVersionUID = 1978042679638547206L;

	/**
	 * How to show the subject in timetime.
	 */
	private String alias;

	/**
	 * Teachers who is able to lead the subject.
	 */
	@ManyToMany(cascade = CascadeType.REFRESH, mappedBy = "subjects", fetch = FetchType.LAZY)
	private Set<Teacher> teachers = new HashSet<>();

	@OneToMany(
			orphanRemoval = true,
			cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH},
			mappedBy = "subject")
	private List<Event> events;

	@OneToMany(
			orphanRemoval = true,
			cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH},
			mappedBy = "subject")
	private Set<Limitation> limitations;

	/**
	 * Subject type.
	 * Joins similar kinds of subjects into sets. E.g., type "math" could include arithmetic, geometry.
	 * <p>The power of type system is you can group subject in any way you want.</p>
	 */
	@ManyToMany(mappedBy = "subjects", cascade = {CascadeType.REFRESH, CascadeType.DETACH})
	private Set<SubjectType> types;

	public String getAlias() {
		return StringUtils.isBlank(alias) ? getName() : alias;
	}

	public void setAlias(final String value) {
		alias = getName().equals(value) ? null : value;
	}
}
