package org.autoschedule.controllers;

import lombok.NonNull;
import org.autoschedule.ApplicationConfig;
import org.autoschedule.domain.Event;
import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectRepository;
import org.autoschedule.domain.Teacher;
import org.autoschedule.domain.TeacherRepository;
import org.autoschedule.service.impl.ScheduleImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * Teacher controller.
 */
@Controller
@RequestMapping("/teachers")
public class TeacherController extends AbstractController {

    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private ApplicationConfig configuration;

    @ModelAttribute("newTeacher")
    public Teacher newTeacher() {
        final Teacher teacher = new Teacher();
        // Set defaults here
        return teacher;
    }

    @ModelAttribute("plan")
    public Plan getActivePlan() {
        return planRepository.findAll().iterator().next();
    }

    @RequestMapping
    public String mainPage(final Model model, final HttpSession session) {
        Page<Teacher> activeTeacher = teacherRepository.findAll(new PageRequest(0, 1, new Sort("name")));
        if (activeTeacher.getTotalElements() == 0) {
            setMessage(session, model);
            model.addAttribute("currentResults", teacherRepository.findAll());
            return "teachers";
        } else {
            return "redirect:/teachers/" + activeTeacher.iterator().next().getId();
        }
    }

    @RequestMapping("/{id}")
    public String subjects(@PathVariable("id") @NonNull final UUID id, final Model model) {
        final Teacher teacher = teacherRepository.findOne(id);
        model.addAttribute("teacher", teacher);
        model.addAttribute("currentResults", teacherRepository.findAll(new Sort("name")));
        final Set<Subject> subjects = StreamSupport.stream(subjectRepository.findAll().spliterator(), false)
                .filter(it -> !teacher.getSubjects().contains(it))
                .collect(Collectors.toSet());
        model.addAttribute("subjects", subjects);
        final Event[][] events = new ScheduleImpl(teacher.getEvents())
                .asTable(configuration.getPeriod(), configuration.getDayLength());
        model.addAttribute("events", events);
        return "teachers";
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String insertTeacher(final Teacher newTeacher, final Model model, final HttpSession session) {
        Teacher created = newTeacher;
        try {
            created = teacherRepository.save(newTeacher);
            setMessageId(MessageType.SUCCESS, session, "message.teacher.created", newTeacher.getName());
        } catch (final RuntimeException e) {
            setMessageId(MessageType.WARNING, session, "message.teacher.error", e.getMessage());
        }
        return "redirect:/teachers/" + created.getId();
    }

    @Override
    protected String getEntityName(final UUID teacherId) {
        final Teacher teacher = teacherRepository.findOne(teacherId);
        return teacher.getName();
    }

    @Override
    protected String setEntityName(final UUID teacherId, final String value) {
        final Teacher teacher = teacherRepository.findOne(teacherId);
        teacher.setName(value);
        teacherRepository.save(teacher);
        return null;
    }
}
