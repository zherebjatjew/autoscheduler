package org.autoschedule.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.ApplicationConfig;
import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.domain.Student;
import org.autoschedule.domain.StudentRepository;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectHour;
import org.autoschedule.domain.SubjectRepository;
import org.autoschedule.domain.Team;
import org.autoschedule.domain.TeamRepository;
import org.autoschedule.service.impl.ScheduleImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Team and student management.
 */
@Slf4j
@Controller
@RequestMapping("/teams")
public class TeamController extends AbstractController {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private ApplicationConfig configuration;

    public TeamData newTeam(final Team parent) {
        final TeamData result = new TeamData();
        // Set defaults here
        if (parent != null) {
            result.setYear(parent.getYear());
            result.setTitle(parent.getTitle() + " - ");
            result.setParent(parent.getId());
            result.setUseParent(true);
        } else {
            result.setTitle("${year}A");
            result.setUseParent(false);
        }
        return result;
    }

    @ModelAttribute("students")
    public Iterable<Student> getStudents() {
        return studentRepository.findAll();
    }

    @ModelAttribute("teams")
    public List<Team> getTeams() {
        return StreamSupport.stream(teamRepository.findAll().spliterator(), false)
                .sorted()
                .collect(Collectors.toList());
    }

    @ModelAttribute("plan")
    public Plan getActivePlan() {
        return planRepository.findAll().iterator().next();
    }

    @RequestMapping
    public String landing(final Model model) {
        final Iterator<Team> iterator = teamRepository.findAll().iterator();
        final Team team;
        if (iterator.hasNext()) {
            team = iterator.next();
            return "redirect:/teams/" + team.getId();
        } else {
            model.addAttribute("newTeam", newTeam(null));
            return "teams";
        }
    }

    @RequestMapping("/{id}")
    public String mainPage(@PathVariable("id") final UUID teamId, final Model model) {
        return mainPage(teamRepository.findOne(teamId), model);
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String insertTeam(final TeamData newTeam, final HttpSession session) {
        Team team = new Team();
        team.setTitle(newTeam.getTitle());
        team.setYear(newTeam.getYear());
        if (newTeam.isUseParent()) {
            team.setParent(teamRepository.findOne(newTeam.getParent()));
        }
        try {
            team = teamRepository.save(team);
            setMessageId(AbstractController.MessageType.SUCCESS, session, "message.team.created", team.getFormattedTitle());
        } catch (final RuntimeException e) {
            setMessageId(AbstractController.MessageType.WARNING, session, "message.team.error", e.getMessage());
        }
        return "redirect:/teams/" + team.getId();
    }

    @RequestMapping(path = "/import", method = RequestMethod.POST)
    public String importTeams() {
        return "redirect:/teams/";
    }

    @RequestMapping("/{id}/formattedTitle")
    public void getFormattedTitle(@PathVariable("id") final UUID teamId, final HttpServletResponse response)
            throws IOException
    {
        Team team = teamRepository.findOne(teamId);
        response.getWriter().print(team.getFormattedTitle());
    }

    @RequestMapping("/{id}/year")
    public void getYear(@PathVariable("id") final UUID teamId, final HttpServletResponse response) throws IOException {
        Team team = teamRepository.findOne(teamId);
        response.getWriter().print(team.getYear());
    }

    @RequestMapping(value = "{id}/year", method = RequestMethod.POST)
    public void setName(@PathVariable("id") final UUID id,
                        @RequestBody final String form,
                        final HttpSession session,
                        final HttpServletResponse response) throws IOException
    {
        final String value = getValue(form);
        Team team = teamRepository.findOne(id);
        try {
            team.setYear(Integer.parseInt(value));
            teamRepository.save(team);
            response.getWriter().print(value);
        } catch (NumberFormatException e) {
            log.warn("Not a number: " + value, e);
        }
    }

    private String mainPage(final Team team, final Model model) {
        model.addAttribute("team", team);
        final Set<SubjectHour> usedSubjects = getActivePlan().getSubjects();
        model.addAttribute("planSubjects", usedSubjects.stream()
                        .filter(it -> it.getTeam() != null)
                        .filter(it -> it.getTeam().getId().equals(team.getId()))
                        .sorted((o1, o2) -> o1.getSubject().getName().compareTo(o2.getSubject().getName()))
                        .collect(Collectors.toList())
        );
        model.addAttribute("events", new ScheduleImpl(team.getEvents())
                .asTable(configuration.getPeriod(), configuration.getDayLength()));
        model.addAttribute("teamMembers", team.getStudents().stream()
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList()));
        model.addAttribute("availableStudents", StreamSupport.stream(studentRepository.findAll().spliterator(), false)
                .filter(it -> !team.getStudents().contains(it))
                .filter(it -> (team.getParent() == null || team.getParent().getStudents().contains(it)))
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList()));
        final Set<UUID> usedSubjectIds = usedSubjects.stream()
                .filter(it -> it.getSubject() != null)
                .map(SubjectHour::getSubject)
                .map(Subject::getId)
                .collect(Collectors.toSet());
        model.addAttribute("subjects", StreamSupport.stream(subjectRepository.findAll().spliterator(), false)
                .filter(it -> !usedSubjectIds.contains(it.getId()))
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList()));
        model.addAttribute("newTeam", newTeam(team));
        return "teams";
    }

    @Override
    protected String getEntityName(final UUID id) {
        return teamRepository.findOne(id).getTitle();
    }

    @Override
    protected String setEntityName(final UUID id, final String value) {
        final Team team = teamRepository.findOne(id);
        team.setTitle(value);
        teamRepository.save(team);
        return null;
    }

    @Getter
    @Setter
    private static class TeamData {
        private String title;
        private Integer year;
        private boolean useParent;
        private UUID parent;
    }
}

