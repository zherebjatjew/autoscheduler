package org.autoschedule.controllers;

import org.autoschedule.ApplicationConfig;
import org.autoschedule.domain.Event;
import org.autoschedule.domain.LimitationRepository;
import org.autoschedule.domain.Room;
import org.autoschedule.domain.RoomRepository;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectRepository;
import org.autoschedule.service.impl.ScheduleImpl;
import org.autoschedule.service.limitations.custom.SubjectToRoomCompatibilityLimitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * Managing rooms.
 */
@Controller
@RequestMapping("/rooms")
public class RoomController extends AbstractController {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private LimitationRepository limitationRepository;
    @Autowired
    private ApplicationConfig configuration;

    @ModelAttribute("newRoom")
    public Room prepareRoomModel() {
        final Room result = new Room();
        result.setCapacity(20);
        return result;
    }

    @ModelAttribute("limitationBySubjectComparator")
    public Comparator<SubjectToRoomCompatibilityLimitation> getLimitationBySUbjectComparator() {
        return (o1, o2) -> o1.getSubject().getName().compareTo(o2.getSubject().getName());
    }

    @RequestMapping("/create")
    public String createRoom(final HttpSession session) {
        session.setAttribute("redirectOnSuccess", "redirect:/ui/rooms");
        session.setAttribute("redirectOnFail", "redirect:/ui/rooms/create");
        return "room-dlg";
    }

    @RequestMapping
    public String landing() {
        final Iterator<Room> rooms = roomRepository.findAll().iterator();
        if (rooms.hasNext()) {
            final Room room = rooms.next();
            return "redirect:/rooms/" + room.getId();
        } else {
            return "empty-rooms";
        }
    }

    @RequestMapping("/{roomId}")
    public String mainPage(@PathVariable("roomId") final UUID roomId, final Model model, final HttpSession session) {
        final Iterable<Room> rooms = roomRepository.findAll(new Sort("name"));
        model.addAttribute("rooms", rooms);
        final Room currentRoom = roomRepository.findOne(roomId);
        model.addAttribute("room", currentRoom);
        final Event[][] events = new ScheduleImpl(currentRoom.getEvents())
                .asTable(configuration.getPeriod(), configuration.getDayLength());
        model.addAttribute("events", events);
        model.addAttribute("availableSubjects", subjectRepository.findAll(new Sort("name")));
        model.addAttribute("subjectToRoomLimitations", currentRoom.getLimitations().stream()
            .filter(it -> it instanceof SubjectToRoomCompatibilityLimitation)
            .map(it -> (SubjectToRoomCompatibilityLimitation) it)
            .filter(it -> it.getSubject() != null)
            .sorted((o1, o2) -> o1.getSubject().getName().compareTo(o2.getSubject().getName()))
            .collect(Collectors.toList())
        );
        setMessage(session, model);
        return "rooms";
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String insertRoom(final Room newRoom, final HttpSession session) {
        Room created = newRoom;
        try {
            created = roomRepository.save(newRoom);
            setMessageId(MessageType.SUCCESS, session, "message.room.created", newRoom.getName());
        } catch (final DataIntegrityViolationException e) {
            setMessageId(MessageType.WARNING, session, "message.room.duplicate");
        }
        return "redirect:/rooms/" + created.getId();
    }

    @RequestMapping("/{roomId}/capacity")
    public String getCapacity(@PathVariable("roomId") final UUID roomId) {
        return Integer.toString(roomRepository.findOne(roomId).getCapacity());
    }

    @RequestMapping(value = "/{roomId}/limitations", method = RequestMethod.POST)
    public void addLimitation(
            @PathVariable final UUID roomId,
            @RequestParam("subject") final UUID subjectId,
            final HttpServletResponse response) throws IOException
    {
        final Room room = roomRepository.findOne(roomId);
        SubjectToRoomCompatibilityLimitation limitation = new SubjectToRoomCompatibilityLimitation();
        limitation.setSubject(subjectRepository.findOne(subjectId));
        limitation = limitationRepository.save(limitation);
        room.getLimitations().add(limitation);
        roomRepository.save(room);
        response.getWriter().print(limitation.getId());
    }

    @RequestMapping(value = "/{roomId}/capacity", method = RequestMethod.POST)
    public void setCapacity(
            @PathVariable("roomId") final UUID roomId,
            @RequestParam("value") final Integer value,
            final HttpServletResponse response) throws IOException
    {
        final Room room = roomRepository.findOne(roomId);
        room.setCapacity(value);
        roomRepository.save(room);
        response.getWriter().print(value);
    }

    @RequestMapping("/subjects")
    public void getSubjects(final HttpServletResponse response) throws IOException {
        final String json = "[" + StreamSupport.stream(subjectRepository.findAll().spliterator(), false)
                .sorted()
                .map(it -> String.format("[\"%s\",\"%s\"]", it.getId(), it.getName()))
                .collect(Collectors.joining(","))
                + ']';
        response.getWriter().print(json);
        response.addHeader("Content-Type", "text/json");
    }

    @RequestMapping(value = "/{roomId}/subjects", method = RequestMethod.POST)
    public void setSubject(
            @PathVariable("roomId") final UUID roomId,
            @RequestParam("value") final UUID subjectId,
            final HttpServletResponse response) throws IOException
    {
        final SubjectToRoomCompatibilityLimitation limitation = new SubjectToRoomCompatibilityLimitation();
        final Subject subject = subjectRepository.findOne(subjectId);
        limitation.setSubject(subject);
        final Room room = roomRepository.findOne(roomId);
        limitation.setRoom(room);
        room.getLimitations().add(limitation);
        roomRepository.save(room);
        response.getWriter().print(subject.getName());
    }

    @Override
    protected String getEntityName(final UUID roomId) {
        final Room room = roomRepository.findOne(roomId);
        return room.getName();
    }

    @Override
    protected String setEntityName(final UUID roomId, final String value) {
        final Room room = roomRepository.findOne(roomId);
        room.setName(value);
        try {
            roomRepository.save(room);
        } catch (final DataIntegrityViolationException e) {
            return "message.room.duplicate";
        }
        return null;
    }
}
