package org.autoschedule.controllers;

import org.autoschedule.domain.Student;
import org.autoschedule.domain.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.UUID;


/**
 * Student controller.
 */
@Controller
@RequestMapping("/students")
public class StudentController extends AbstractController {

    @Autowired
    private StudentRepository studentRepository;

    @ModelAttribute("newStudent")
    public Student prepareStudentModel() {
        final Student student = new Student();
        // Set defaults here
        return student;
    }

    @RequestMapping
    public String mainPage(final Pageable pageable, final Model model, final HttpSession session) {
        Page currentResults = studentRepository.findAll(
                new PageRequest(pageable.getPageNumber() == 0 ? 0 : pageable.getPageNumber() - 1,
                        pageable.getPageSize(),
                        new Sort("name")));
        model.addAttribute("currentResults", currentResults);
        setupPagination(pageable, model, currentResults);
        setMessage(session, model);
        return "students";
    }

    @RequestMapping("/create")
    public String createStudent(final Model model, final HttpSession session) {
        session.setAttribute("redirectOnSuccess", "redirect:/ui/students");
        session.setAttribute("redirectOnFail", "redirect:/ui/students/create");
        return "student-dlg";
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String insertStudent(final Student newStudent, final Model model, final HttpSession session) {
        try {
            studentRepository.save(newStudent);
            setMessageId(MessageType.SUCCESS, session, "message.student.created", newStudent.getName());
        } catch (final RuntimeException e) {
            setMessageId(MessageType.WARNING, session, "message.student.error", e.getMessage());
        }
        return "redirect:/students";
    }

    @Override
    protected String getEntityName(final UUID studentId) {
        final Student student = studentRepository.findOne(studentId);
        return student.getName();
    }

    @Override
    protected String setEntityName(final UUID studentId, final String value) {
        final Student student = studentRepository.findOne(studentId);
        student.setName(value);
        studentRepository.save(student);
        return null;
    }
}
