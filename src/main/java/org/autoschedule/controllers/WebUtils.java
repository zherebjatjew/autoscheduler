package org.autoschedule.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.autoschedule.domain.LimitationAware;
import org.autoschedule.domain.Subject;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.DiscriminatorValue;
import java.lang.reflect.InvocationTargetException;


@Component
@Slf4j
public class WebUtils {
    public boolean entityHasLimitation(
            @Nonnull final LimitationAware entity,
            @Nullable final String limitationType,
            @Nullable final String propertyName,
            @Nullable final String expectedValue)
    {
        return entity.getLimitations().stream()
                .filter(it -> limitationType == null
                        || it.getClass().getSimpleName().equals(limitationType))
                .filter(it -> {
                    if (propertyName == null) {
                        return true;
                    } else {
                        try {
                            final Object realValue = BeanUtils.getProperty(it, propertyName);
                            return expectedValue == null ? realValue == null : expectedValue.equals(realValue);
                        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                            log.error("Error reading property " + propertyName + " of class " + it.getClass().getName(), e);
                            return false;
                        }
                    }
                }).count() > 0;
    }

    public boolean subjectHasConstraint(final Subject subject, final String name) {
        assert subject != null;
        assert name != null;
        return subject.getLimitations().stream()
                .filter(lim -> lim.getClass().getAnnotation(DiscriminatorValue.class).value().equals(name))
                .count() > 0;
    }
}
