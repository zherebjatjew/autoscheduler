package org.autoschedule.controllers;

import lombok.NonNull;
import org.autoschedule.api.Schedule;
import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.UUID;

/**
 * Solution resource.
 */
@RestController
@RequestMapping(path = "/solutions")
public class SolvingController {

    @Autowired
    private SchedulerService service;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private EntityLinks entityLinks;

    @RequestMapping(path = "/{planUuid}")
    public String solve(@PathParam("planUuid") @NonNull final UUID planUuid) {
        final Plan plan = planRepository.findOne(planUuid);
        List<Schedule> timetable = service.build(plan);
        plan.setEvents(timetable.get(0));
        planRepository.save(plan);
        return entityLinks.linkToSingleResource(plan).getHref();
    }
}
