package org.autoschedule.controllers;

import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/plans")
public class PlanController {
    @Autowired
    private PlanRepository planRepository;

    @ModelAttribute("newPlan")
    public Plan newPlan() {
        final Plan plan = new Plan();
        return plan;
    }

    @RequestMapping
    public String mainPage(final Pageable pageable, final Model model) {
        Page currentResults = planRepository.findAll(
                new PageRequest(pageable.getPageNumber() == 0 ? 0 : pageable.getPageNumber() - 1,
                        1,
                        new Sort("name")));
        model.addAttribute("currentResults", currentResults);
        return "plans";
    }
}
