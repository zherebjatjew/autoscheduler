package org.autoschedule.controllers;

import lombok.Getter;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectRepository;
import org.autoschedule.domain.SubjectType;
import org.autoschedule.domain.SubjectTypeRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/subjects")
public class SubjectController extends AbstractController {
    @Resource
    private SubjectRepository subjectRepository;
    @Resource
    private SubjectTypeRepository typeRepository;

    @Override
    protected String getEntityName(final UUID id) {
        return subjectRepository.findOne(id).getName();
    }

    @Override
    protected String setEntityName(final UUID id, final String value) {
        final Subject subject = subjectRepository.findOne(id);
        subject.setName(value);
        try {
            subjectRepository.save(subject);
        } catch (final DataIntegrityViolationException e) {
            return "message.subject.duplicate";
        }
        return null;
    }

    @ModelAttribute("newSubject")
    public Subject newSubject() {
        final Subject result = new Subject();
        // Set defaults here
        return result;
    }

    @RequestMapping("/{id}")
    public String subjectPage(@PathVariable("id") final UUID id, final Model model) {
        final Subject subject = subjectRepository.findOne(id);
        model.addAttribute("subject", subject);
        model.addAttribute("theOthers", StreamSupport.stream(subjectRepository.findAll().spliterator(), false)
                .filter(it -> !it.equals(subject))
                .sorted((s1, s2) -> s1.getAlias().compareTo(s2.getAlias()))
                .collect(Collectors.toList())
        );
        model.addAttribute("availableTypes", StreamSupport.stream(typeRepository.findAll().spliterator(), false)
                .filter(it -> !subject.getTypes().contains(it))
                .sorted((t1, t2) -> t1.getName().compareTo(t2.getName()))
                .map(it -> new SelectOptionDef(it.getName(), it.getId().toString()))
//                .map(AbstractNamedEntity::getName)
                .collect(Collectors.toList())
        );
        model.addAttribute("constraints", getConstraints(subject));
        return "subject";
    }

    private List<ConstraintDef> getConstraints(final Subject subject) {
        return Arrays.asList(
                new ConstraintDef("Put to top", true),
                new ConstraintDef("Spare across week", false),
                new ConstraintDef("Prefer start of week", false)
        );
    }

    @RequestMapping
    public String mainPage(final Pageable pageable, final Model model, final HttpSession session) {
        Page currentResults = subjectRepository.findAll(
                new PageRequest(pageable.getPageNumber() == 0 ? 0 : pageable.getPageNumber() - 1,
                        pageable.getPageSize(),
                        new Sort("name")));
        model.addAttribute("currentResults", currentResults);
        setupPagination(pageable, model, currentResults);
        setMessage(session, model);
        return "subjects";
    }

    @RequestMapping("/{id}/alias")
    public void getAlias(@PathVariable("id") final UUID id, final HttpServletResponse response) throws IOException {
        final String result = subjectRepository.findOne(id).getAlias();
        response.getWriter().print(result);
    }

    @RequestMapping(value = "/{id}/alias", method = RequestMethod.POST)
    public void setAlias(@PathVariable("id") final UUID id,
                         @RequestBody final String form,
                         final HttpServletResponse response) throws IOException
    {
        final String value = getValue(form);
        final Subject subject = subjectRepository.findOne(id);
        subject.setAlias(value);
        subjectRepository.save(subject);
        response.getWriter().print(value);
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String insertSubject(final Subject newSubject, final Model model, final HttpSession session) {
        try {
            subjectRepository.save(newSubject);
            setMessageId(MessageType.SUCCESS, session, "message.subject.created", newSubject.getName());
        } catch (final RuntimeException e) {
            setMessageId(MessageType.WARNING, session, "message.subject.error", e.getMessage());
        }
        return "redirect:/subjects";
    }

    @RequestMapping(path = "/{id}/types", method = RequestMethod.POST)
    @ResponseBody
    public String addType(final UUID subjectId, @RequestParam("type") final UUID typeId) {
        final Subject subject = subjectRepository.findOne(subjectId);
        final SubjectType type = typeRepository.findOne(typeId);
        if (subject.getTypes().add(type)) {
            subjectRepository.save(subject);
        }
        return type.getName();
    }

    @Getter
    private static class ConstraintDef {
        private String name;
        private boolean applied;

        ConstraintDef(final String name, final boolean applied) {
            this.name = name;
            this.applied = applied;
        }
    }

    @Getter
    public static class SelectOptionDef {
        private String value;
        private String label;

        SelectOptionDef(final String name, final String id) {
            label = name;
            value = id;
        }
    }
}
