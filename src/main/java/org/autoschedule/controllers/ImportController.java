package org.autoschedule.controllers;

import com.opencsv.CSVReader;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.StringReader;

@RestController
@RequestMapping("${spring.data.rest.base-path}/import")
public class ImportController {
    private static final int COL_STUDENT_NAME = 0;
    private static final int COL_STUDENT_TEAM = 1;

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public void importStudents(@RequestBody final String csv) throws IOException {
        final CSVReader reader = new CSVReader(new StringReader(csv));
        String[] line;
        while ((line = reader.readNext()) != null) {
            final String name = line[COL_STUDENT_NAME];
            final String team = line[COL_STUDENT_TEAM];
        }
    }
}
