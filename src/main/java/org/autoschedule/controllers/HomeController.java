package org.autoschedule.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home page controller.
 * <p>
 *     see
 *     <ul>
 *         <li><a href="http://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html">Thymeleaf guide</a></li>
 *         <li><a href="https://github.com/spring-projects/spring-webflow-samples/blob/master/booking-mvc/src/main/java/org/springframework/webflow/samples/booking/config/WebFlowConfig.java">WebFlow example</a></li>
 *     </ul>
 * </p>
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @RequestMapping
    public String home(final Model model) {
        return "hello";
    }
}
