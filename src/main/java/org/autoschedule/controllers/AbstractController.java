package org.autoschedule.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.UUID;

/**
 * Base of controllers.
 */
@Component
@ControllerAdvice
@RequestMapping("/${spring.data.rest.base-path}")
@PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
public abstract class AbstractController {
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "messageType";

    /**
     * Returns name of entity by ID.
     *
     * @param id entity ID
     * @return name
     */
    protected abstract String getEntityName(final UUID id);

    /**
     * Updates name of entity, addressed by ID.
     *
     * @param id entity ID
     * @param value new name
     * @return null if OK or id of error string
     */
    protected abstract String setEntityName(final UUID id, final String value);

    @ModelAttribute("currentUser")
    public UserDetails getCurrentUser(final Authentication authentication) {
        return (authentication == null) ? null : (UserDetails) authentication.getPrincipal();
    }

    @RequestMapping("/{id}/name")
    public void getName(@PathVariable("id") final UUID id, final HttpServletResponse response) throws IOException {
        final String name = getEntityName(id);
        response.getWriter().print(name);
    }

    @RequestMapping(value = "{id}/name", method = RequestMethod.POST)
    public void setName(@PathVariable("id") final UUID id,
                        @RequestBody final String form,
                        final HttpSession session,
                        final HttpServletResponse response) throws IOException
    {
        final String value = getValue(form);
        final String message = setEntityName(id, value);
        if (message != null) {
            setMessageId(MessageType.INFO, session, message);
            response.sendRedirect(getClass().getAnnotation(RequestMapping.class).value()[0]);
        } else {
            response.getWriter().print(value);
        }
    }

    @Autowired
    private MessageSource messageSource;

    protected void setMessageId(final MessageType type, final HttpSession session,
                                final String messageId, final String ... params)
    {
        final Locale locale = LocaleContextHolder.getLocale();
        session.setAttribute(MESSAGE, messageSource.getMessage(messageId, params, locale));
        session.setAttribute(MESSAGE_TYPE, type.name().toLowerCase(locale));
    }

    protected void setMessage(final HttpSession session, final Model model) {
        model.addAttribute(MESSAGE, session.getAttribute(MESSAGE));
        model.addAttribute(MESSAGE_TYPE, session.getAttribute(MESSAGE_TYPE));
        session.removeAttribute(MESSAGE);
        session.removeAttribute(MESSAGE_TYPE);
    }

    protected void setupPagination(final Pageable pageable, final Model model, final Page currentResults) {
        int cur = pageable.getPageNumber() == 0 ? 1 : pageable.getPageNumber();
        int begin = Math.max(1, cur - 5);
        int end = Math.min(begin + 10, currentResults.getTotalPages());

        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", cur);
        model.addAttribute("totalPages", currentResults.getTotalPages());
        String path = getClass().getAnnotation(RequestMapping.class).value()[0];
        if (!path.startsWith("/")) {
            path = '/' + path;
        }
        model.addAttribute("url", path);
    }

    protected String getValue(final String form) {
        final String[] pairs = form.split("\\&");
        for (final String item : pairs) {
            if (item.startsWith("value=")) {
                return URLDecoder.decode(item.substring("value=".length()));
            }
        }
        throw new IllegalArgumentException("Key 'value' was not found");
    }

    public enum MessageType {
        INFO,
        SUCCESS,
        WARNING,
        DANGER
    };
}
