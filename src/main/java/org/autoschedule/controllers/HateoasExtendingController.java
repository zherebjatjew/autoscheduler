package org.autoschedule.controllers;

import org.autoschedule.domain.SubjectHour;
import org.autoschedule.domain.SubjectHourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequestMapping("${spring.data.rest.base-path}/")
public class HateoasExtendingController {

    @Autowired
    private SubjectHourRepository hoursRepository;

    @RequestMapping(value = "hours/{id}/count")
    public Integer getHourCount(@PathVariable("id") final UUID id) {
        return hoursRepository.findOne(id).getHours();
    }

    @Transactional
    @RequestMapping(value = "hours/{id}/count", method = RequestMethod.POST)
    public Integer setSubjectHours(@PathVariable("id") final UUID id, @RequestParam("value") final Integer value) {
        final SubjectHour hours = hoursRepository.findOne(id);
        hours.setHours(value);
        hoursRepository.save(hours);
        return value;
    }
}
