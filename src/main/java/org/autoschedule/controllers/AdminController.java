package org.autoschedule.controllers;

import lombok.extern.slf4j.Slf4j;
import org.autoschedule.api.Schedule;
import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.domain.EventRepository;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.LimitationRepository;
import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.service.SchedulerService;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.InputSource;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Implementation of {@link AdminController}'s custom methods.
 */
@Slf4j
@RestController
@RequestMapping(path = "/admin")
@PreAuthorize("hasAnyRole('SUPERUSER', 'MANAGER')")
public class AdminController implements ApplicationContextAware {

    private ApplicationContext appContext;
    @Autowired
    private LimitationRepository limitationRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private SchedulerService schedulerService;

    /**
     * Reset database. Create default objects.
     */
    @Transactional
    @RequestMapping(value = "/db-init", method = RequestMethod.POST)
    public void initializeDatabase() {
        clearDatabase();
        createDefaultLimitations();
    }

    /**
     * Fill database with sample data.
     *
     * @throws IOException
     * @throws DatabaseUnitException
     * @throws SQLException
     */
    @RequestMapping(value = "/sampledata", method = RequestMethod.POST)
    public void createSampleData(@RequestBody final String data)
            throws IOException, DatabaseUnitException, SQLException
    {
        try (Connection connection = DataSourceUtils.getConnection(dataSource)) {
            IDatabaseConnection con = new DatabaseConnection(connection);
            final DatabaseConfig dbConfig = con.getConfig();
            switch (connection.getMetaData().getDatabaseProductName()) {
                case "HSQL Database Engine":
                    dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
                    break;
                case "MySQL":
                    dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
                    break;
                case "PostgreSQL":
                    dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory());
                    break;
                default:
                    log.warn("No matching database product found when setting DBUnit DATATYPE_FACTORY");
            }
            IDataSet dataSet;
            if (!data.isEmpty()) {
                try (final StringReader reader = new StringReader(data)) {
                    final FlatXmlProducer producer = new FlatXmlProducer(new InputSource(reader));
                    producer.setColumnSensing(true);
                    dataSet = new FlatXmlDataSet(producer);
                }
            } else {
                try (final InputStream xmlResource = ClassLoader.class.getResourceAsStream("/sample-data.xml")) {
                    dataSet = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(xmlResource)));
                }
            }
            eventRepository.delete(eventRepository.findAll());
            DatabaseOperation.CLEAN_INSERT.execute(con, dataSet);
        }
    }

    @Transactional
    @RequestMapping(value = "/schedule/{planId}", method = RequestMethod.POST)
    public void schedule(@PathVariable("planId") final UUID planId) {
        final Plan plan = planRepository.findOne(planId);
        eventRepository.clean(planId);
        final List<Schedule> result = schedulerService.build(plan);
        if (!result.isEmpty()) {
            final Schedule accepted = result.get(0);
            eventRepository.save(accepted);
        } else {
            plan.setEvents(Collections.emptyList());
        }
    }

    protected void createDefaultLimitations() {
        appContext.getBeansOfType(DefaultLimitation.class).values().stream()
                .map(it -> (Limitation) it)
                .forEach(limitationRepository::save);
    }

    protected void clearDatabase() {
        limitationRepository.deleteAll();
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }
}
