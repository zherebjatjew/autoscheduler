package org.autoschedule.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;
import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

/**
 * Most of the entities have {@link javax.persistence.FetchType#LAZY} mode for related sub-objects.
 * This is OK, because I do not want them to be loaded during solving.
 * However, it becomes a problem for REST, because we need them all to be passed over HTTP.
 * One of solutions is simply iterate over all collection fields recursively.
 * <p>
 *     The implementation works with fields of type {@link Collection} oneOf {@link Map}, which are
 *     not marked with {@link JsonIgnore} nor {@link Transient} annotations.
 * </p>
 */
@Component
@Slf4j
public class LazyCollectionFetcher {

	/**
	 * Fetch lazy-loaded fields of single object.
	 *
	 * @param entity object to fetch
	 */
	public void fetch(@NonNull final Object entity) {
		Class cls = entity.getClass();
		while (cls != null && cls != Object.class) {
			StreamSupport.stream(
					Spliterators.<Field>spliterator(cls.getDeclaredFields(),
							Spliterator.IMMUTABLE), false)
					.filter(this::isSerializableCollection)
					.filter(field -> isSerializableProperty(entity, field))
					.forEach(field -> fetchSingleProperty(entity, field));
			cls = cls.getSuperclass();
		}
	}

	private void fetchSingleProperty(final Object entity, final Field field) {
		try {
			final Object value = PropertyUtils.getProperty(entity, field.getName());
			final Collection collection = (value instanceof Collection)
					? (Collection) value
					: ((Map) value).values();
			collection.forEach(this::fetch);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassCastException ex) {
			log.warn("Algorithmic error", ex);
		}
	}

	private boolean isSerializableProperty(final Object entity, final Field field) {
		try {
			final PropertyDescriptor descriptor = PropertyUtils.getPropertyDescriptor(entity, field.getName());
			return descriptor.getReadMethod() != null
					&& descriptor.getReadMethod().getAnnotation(JsonIgnore.class) == null
					&& descriptor.getReadMethod().getAnnotation(Transient.class) == null
					&& (descriptor.getWriteMethod() == null
					|| descriptor.getWriteMethod().getAnnotation(JsonIgnore.class) == null
					|| descriptor.getWriteMethod().getAnnotation(Transient.class) == null);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.warn("Error fetching field \"" + field.getName() + '"', e);
		} catch (ClassCastException ex) {
			log.warn("Algorithmic error", ex);
		}
		return false;
	}

	private boolean isSerializableCollection(final Field field) {
		return (field.getModifiers() & Modifier.STATIC) == 0
				&& (Collection.class.isAssignableFrom(field.getType()) || Map.class.isAssignableFrom(field.getType()))
				&& field.getAnnotation(Transient.class) == null
				&& field.getAnnotation(JsonIgnore.class) == null;
	}
}
