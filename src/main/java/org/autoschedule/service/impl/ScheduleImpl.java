package org.autoschedule.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.autoschedule.domain.Event;
import org.autoschedule.api.Index;
import org.autoschedule.api.Schedule;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


/**
 * Default implementation of {@link Schedule}.
 */
public class ScheduleImpl implements Schedule {
	private final List<Event> events = new ArrayList<>();

	/**
	 * Empty constructor.
	 */
	public ScheduleImpl() {
	}

	/**
	 * Construct from event list.
	 *
	 * @param events event list
	 */
	public ScheduleImpl(final Collection<Event> events) {
		this.events.addAll(events);
		Collections.sort(this.events);
	}

	@Override
	public Schedule getSchedule(final Index from, final Index to) throws ArrayIndexOutOfBoundsException {
		final ScheduleImpl result = new ScheduleImpl();
		events.stream().filter(it -> it.getPosition().isWithin(from, to)).forEach(result::add);
		return result;
	}

	@Override
	public Iterable<Schedule> byHours() {
		final MultiValueMap<Index, Event> groupedByHours = new LinkedMultiValueMap<>();
		events.stream().forEach(it -> groupedByHours.add(it.getPosition(), it));
		return groupedByHours.values().stream()
				.map(ScheduleImpl::new)
				.sorted((o1, o2) -> o1.events.get(0).compareTo(o2.events.get(0)))
				.collect(Collectors.toList());
	}

	@Override
	public Iterable<Schedule> byDays() {
		final MultiValueMap<Integer, Event> groupedByDays = new LinkedMultiValueMap<>();
		events.stream().forEach(it -> groupedByDays.add(it.getPosition().getDay(), it));
		return groupedByDays.values().stream()
				.map(ScheduleImpl::new)
				.sorted((o1, o2) -> o1.events.get(0).compareTo(o2.events.get(0)))
				.collect(Collectors.toList());
	}

	@Override
	public Event[][] asTable(final int days, final int hours) {
		if (isEmpty()) {
			return new Event[0][];
		}
		final Event[][] result = new Event[hours][days];
		events.stream().forEach(it -> result[it.getPosition().getHour()][it.getPosition().getDay()] = it);
		return result;
	}

	@Override
	public Iterator<Event> iterator() {
		return events.iterator();
	}

	public void add(final Event event) {
		events.add(event);
	}

	@Override
	public boolean isEmpty() {
		return events.isEmpty();
	}
}
