package org.autoschedule.service.impl;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.LimitationStrategy;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * Strict limitation strategy: applies all limitations.
 */
@Component
public class StrictLimitationStrategy implements LimitationStrategy {
    @Override
    public IntVar apply(
            @NonNull final SolverContext context,
            @NonNull final Map<LimitationHandler, List<Limitation>> limitations)
    {
        limitations.forEach((k, v) -> k.apply(context, v));
        return null;
    }
}
