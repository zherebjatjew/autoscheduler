package org.autoschedule.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.autoschedule.ApplicationConfig;
import org.autoschedule.domain.AbstractEntity;
import org.autoschedule.domain.Event;
import org.autoschedule.domain.Plan;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.domain.RoomRepository;
import org.autoschedule.domain.Team;
import org.autoschedule.domain.TeamRepository;
import org.autoschedule.service.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.api.Index;
import org.autoschedule.service.VarAccessor;
import org.autoschedule.service.VarSelector;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VariableFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * Default implementation of {@link SolverContext}.
 */
@Component
@Scope("prototype")
@Slf4j
public class SolverContextImpl implements SolverContext {

	private static final String VAR_NAME_PREFIX = "V";
	@Autowired
	private TeamRepository teamRepository;
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private RoomRepository roomRepository;
	@Autowired
	private ApplicationConfig configuration;

    private Solver solver;
    private Plan plan;
    // Cache
    private final Map<String, IntVar> vars = new HashMap<>();
    private ObjectMapper<AbstractEntity> uuidMapper = new ObjectMapper<>();
    private Set<Integer> concernedTeams;
    private Set<Integer> concernedRooms;
    private Set<Integer> concernedTeachers;

    private Set<Integer> concernedSubjects;
	private final AtomicInteger nameIndexCounter = new AtomicInteger(0);

    public SolverContextImpl(@NonNull final Plan plan, @NonNull final Solver solver) {
		this.solver = solver;
		this.plan = plan;
	}

	@PostConstruct
	private void initialize() {
        uuidMapper.clear();
        createAllVariables();
	}

	@NonNull
	@Override
	public VarSelector selector() {
		return new VarSelectorImpl(this);
	}

	@Override
	public IntVar getVar(@NonNull final String name) {
		final IntVar res = vars.get(name);
		if (res == null) {
			throw new NullPointerException("Variable " + name + " has not been defined");
		}
		return res;
	}

	@Override
	public IntVar getVar(final Integer max) {
		final String name = VAR_NAME_PREFIX + nameIndexCounter.incrementAndGet();
		return VariableFactory.integer(name, 0, max == null ? Integer.MAX_VALUE : max, getSolver());
	}

	@Override
	public IntVar getVar(@NonNull final String name, @NonNull final Function<Solver, IntVar> ifNotDefined) {
		IntVar res = vars.get(name);
		if (res == null) {
			res = ifNotDefined.apply(getSolver());
			vars.put(name, res);
		}
		return res;
	}

	@NonNull
	@Override
	public IntVar getConst(final int value) {
		String name = "C";
		if (value < 0) {
			name += ".MINUS";
		}
		name += value;
		IntVar res = vars.get(name);
		if (res == null) {
			res = VariableFactory.fixed(name, value, solver);
			vars.put(name, res);
		}
		return res;
	}

    @NonNull
    public Plan getPlan() {
        return plan;
    }

	@NonNull
	@Override
	public ScheduleImpl convert(@NonNull final Solution solution) {
		final ScheduleImpl result = new ScheduleImpl();
		for (int d = 0; d < configuration.getPeriod(); d++) {
			for (int h = 0; h < configuration.getDayLength(); h++) {
				for (int roomId : getConcernedRooms()) {
					int teamId = solution.getIntVal(getVar(
							VarAccessor.getVarName(d, h, roomId, VarAccessor.VARTYPE.GROUP)));
					int teacherId = solution.getIntVal(getVar(
							VarAccessor.getVarName(d, h, roomId, VarAccessor.VARTYPE.TEACHER)));
					int subjectId = solution.getIntVal(getVar(
							VarAccessor.getVarName(d, h, roomId, VarAccessor.VARTYPE.SUBJECT)));
					if (teamId != 0 && teacherId != 0 && subjectId != 0) {
						final Event event = new Event();
						event.setPlan(getPlan());
						event.setTeam(indexToEntity(teamId));
						event.setSubject(indexToEntity(subjectId));
						event.setRoom(indexToEntity(roomId));
						event.setTeacher(indexToEntity(teacherId));
						event.setPosition(new Index(d, h));
						result.add(event);
					} else if (teamId != 0 || teacherId != 0 || subjectId != 0) {
						log.warn(String.format(
								"Corrupted event:\n\tRoom = %d\n\tTeam = %d\n\tTeacher = %d\n\t"
								+ "Subject = %d.\nMost possible reason is incomplete limitations.",
								roomId, teamId, teacherId, subjectId)
						);
					}
				}
			}
		}
		return result;
	}

	@Override
	public void setVar(@NonNull final IntVar val) {
		vars.put(val.getName(), val);
	}

	@Override
	public int getPlanningPeriod() {
		return configuration.getPeriod();
	}

	@Override
	public int getMaxHours() {
		return configuration.getDayLength();
	}

	@NonNull
	@Override
	public Solver getSolver() {
		return solver;
	}

	private void createAllVariables() {
		for (int d = 0; d < configuration.getPeriod(); d++) {
			for (int h = 0; h < configuration.getDayLength(); h++) {
				for (int r : getConcernedRooms()) {
					IntVar var = VariableFactory.enumerated(
							VarAccessor.getVarName(d, h, r, VarAccessor.VARTYPE.TEACHER),
							IntStream.concat(IntStream.of(0), getConcernedTeachers()
									.stream()
									.mapToInt(it -> it))
							.toArray(),
							solver);
					setVar(var);
					var = VariableFactory.enumerated(
							VarAccessor.getVarName(d, h, r, VarAccessor.VARTYPE.GROUP),
							IntStream.concat(IntStream.of(0), getConcernedTeams()
									.stream()
									.mapToInt(it -> it))
								.toArray(),
							solver);
					setVar(var);
					var = VariableFactory.enumerated(
							VarAccessor.getVarName(d, h, r, VarAccessor.VARTYPE.SUBJECT),
							IntStream.concat(IntStream.of(0), getConcernedSubjects()
									.stream()
									.mapToInt(it -> it))
								.toArray(),
							solver);
					setVar(var);
				}
			}
		}
	}

    @NonNull
    @Override
    public Set<Integer> getConcernedTeachers() {
        if (concernedTeachers == null) {
            concernedTeachers = planRepository.getConcernedTeachers(plan.getId()).stream()
                    .map(this::registerEntity)
                    .collect(Collectors.toSet());
			if (concernedTeachers.isEmpty()) {
				throw new IllegalStateException("exception.no.concerned.teachers");
			}
        }
        return concernedTeachers;
    }

    @NonNull
	@Override
	public Set<Integer> getConcernedSubjects() {
        if (concernedSubjects == null) {
            concernedSubjects = planRepository.getConcernedSubjects(plan.getId()).stream()
                    .map(this::registerEntity)
                    .collect(Collectors.toSet());
			if (concernedSubjects.isEmpty()) {
				throw new IllegalStateException("exception.no.concerned.subjects");
			}
        }
        return concernedSubjects;
	}

    @NonNull
    @Override
    public Set<Integer> getConcernedRooms() {
        if (concernedRooms == null) {
            final Collection<UUID> teams = planRepository.getConcernedTeams(plan.getId()).stream()
                    .map(Team::getId)
                    .collect(Collectors.toList());
            final long minCapacity = teams.isEmpty()
					? 0L
					: StreamSupport.stream(teamRepository.getTeamSizes(teams).spliterator(), false)
							.mapToLong(TeamRepository.TeamSize::getStudentCount)
							.min().getAsLong();
            concernedRooms = StreamSupport.stream(roomRepository.findAll().spliterator(), false)
                    .filter(it -> it.getCapacity() >= minCapacity)
                    .map(this::registerEntity)
                    .collect(Collectors.toSet());
			if (concernedRooms.isEmpty()) {
				throw new IllegalStateException("exception.no.concerned.rooms");
			}
        }
        return concernedRooms;
    }

    @NonNull
	@Override
	public Set<Integer> getConcernedTeams() {
        if (concernedTeams == null) {
            concernedTeams = planRepository.getConcernedTeams(plan.getId()).stream()
                    .map(this::registerEntity)
                    .collect(Collectors.toSet());
			if (concernedTeams.isEmpty()) {
				throw new IllegalStateException("exception.no.concerned.teams");
			}
        }
        return concernedTeams;
	}

    @Override
    public Integer entityToIndex(final AbstractEntity entity) {
        return uuidMapper.getIndex(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractEntity> T indexToEntity(final Integer index) {
        return (T) uuidMapper.getObject(index);
    }

    protected Integer registerEntity(final AbstractEntity entity) {
        return uuidMapper.registerObject(entity);
    }

}
