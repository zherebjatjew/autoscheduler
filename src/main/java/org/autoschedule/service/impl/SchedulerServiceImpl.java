package org.autoschedule.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.LimitationRepository;
import org.autoschedule.domain.Plan;
import org.autoschedule.api.Schedule;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SchedulerService;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.LimitationStrategy;
import org.chocosolver.solver.ResolutionPolicy;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;


@Service
@Slf4j
public class SchedulerServiceImpl implements SchedulerService, ApplicationContextAware {

    private ApplicationContext appContext;

    @Autowired
    private LimitationRepository limitationRepository;
    @Autowired
    private WeakLimitationStrategy weakStrategy;
    @Autowired
    private OptimizingLimitationStrategy optimizingStrategy;

    @Override
    @NonNull
    @Transactional(readOnly = true)
    public List<Schedule> build(@NonNull final Plan plan) {
        Schedule solution = calculate(plan, context -> solve(context, weakStrategy));
        if (solution == null) {
            log.info("No solutions. Try to ease your constraints.");
            return Collections.emptyList();
        }
        return Collections.singletonList(solution);
    }

    private void logInputData(final SolverContext solverContext) {
        log.info("Solver model prepared");
        log.debug(solverContext.getSolver().toString());
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }

    private Schedule calculate(
            final Plan plan,
            final Function<SolverContext, Boolean> solvingFunction)
    {
        final Solver solver = new Solver("PLAN:" + plan.getId());
        final SolverContext solverContext = appContext.getBean(SolverContext.class, plan, solver);

        logInputData(solverContext);

        if (solvingFunction.apply(solverContext)) {
            final ScheduleImpl schedule = solverContext.convert(solver.getSolutionRecorder().getLastSolution());
            if (!schedule.isEmpty()) {
                log.info("Found a solution");
                return schedule;
            }
        }
        return null;
    }

    private boolean solve(final SolverContext solverContext, final LimitationStrategy solvingStrategy) {
        final Map<LimitationHandler, List<Limitation>> limitations = getLimitations(solverContext);
        solvingStrategy.apply(solverContext, limitations);
        log.debug(solverContext.getSolver().toString());
        final IntVar optimizer = optimizingStrategy.apply(solverContext, limitations);
        if (optimizer != null) {
            final int max = optimizer.getUB();
            log.debug("Optimizing relative to {}", optimizer.getName());
            final long startTime = System.currentTimeMillis();
            solverContext.getSolver().findAllOptimalSolutions(ResolutionPolicy.MAXIMIZE, optimizer, true);
            final long endTime = System.currentTimeMillis();
            if (solverContext.getSolver().isFeasible() != ESat.TRUE) {
                log.warn("Solution not found\n{}", solverContext.getSolver());
                return false;
            }
            log.debug("Optimization criterion is {} of {}. Calculation time {}",
                    optimizer.getValue(), max, (endTime - startTime)/1000.0);
        } else {
            return solverContext.getSolver().findSolution();
        }
        return solverContext.getSolver().getMeasures().getSolutionCount() > 0;
    }

    private Map<LimitationHandler, List<Limitation>> getLimitations(final SolverContext context) {
        final MultiValueMap<Class<? extends Limitation>, Limitation> byClass = new LinkedMultiValueMap<>();
        for (final Limitation limitation : limitationRepository.findByPlan(context.getPlan().getId())) {
            log.debug("Adding limitation " + limitation.getClass().getSimpleName());
            byClass.add(limitation.getClass(), limitation);
        }
        final Map<LimitationHandler, List<Limitation>> result = new HashMap<>(byClass.size());
        byClass.forEach((k, v) -> {
            final LimitationHandler handler = getHandler(k);
            if (handler == null) {
                log.error("No handler for limitation " + k.getCanonicalName());
            } else {
                result.put(handler, v);
            }
        });
        return result;
    }

    private LimitationHandler getHandler(final Class<? extends Limitation> clazz) {
        final String name = clazz.getSimpleName().replace("Limitation", "Handler");
        final String beanName = name.substring(0, 1).toLowerCase() + name.substring(1);
        try {
            return (LimitationHandler) appContext.getBean(beanName);
        } catch (final NoSuchBeanDefinitionException e) {
            return null;
        }
    }
}
