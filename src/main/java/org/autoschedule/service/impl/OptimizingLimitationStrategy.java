package org.autoschedule.service.impl;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.OptimizationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.LimitationStrategy;
import org.autoschedule.util.CustomizableCollector;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;


/**
 * Takes only optimizing limitations and combines them into a single variable, ready to use in optimization.
 */
@Component
public class OptimizingLimitationStrategy implements LimitationStrategy {
    @Override
    public IntVar apply(
            @NonNull final SolverContext context,
            @NonNull final Map<LimitationHandler, List<Limitation>> limitations)
    {
        return limitations.entrySet().stream()
                .filter(entry -> entry.getKey() instanceof OptimizationHandler)
                .map(entry -> {
                    final OptimizationHandler handler = (OptimizationHandler) entry.getKey();
                    // WARNING!
                    // Inconsistent weights of limitations with the same handler are not yet supported.
                    return new OptimizationVar(
                            entry.getValue().get(0).getWeight(),
                            handler.getFactor(context, entry.getValue()));
                })
                .filter(it -> it.getFactor() != null)
                // Higher weights first
                .sorted()
                // Sum neighbour items of equal weight
                .collect(new CustomizableCollector<>((list, item) -> {
                    if (list.isEmpty()) {
                        list.add(item);
                    } else {
                        final OptimizationVar existing = list.get(list.size() - 1);
                        if (existing.compareTo(item) == 0) {
                            final IntVar sum = context.getVar(existing.getFactor().getUB() + item.getFactor().getUB());
                            context.getSolver().post(ICF.sum(new IntVar[]{existing.getFactor(), item.getFactor()}, "=", sum));
                            existing.setFactor(sum);
                        } else {
                            list.add(item);
                        }
                    }
                })).stream()
                .map(OptimizationVar::getFactor)
                // Combine items respecting weights:
                // R = ((r1*r2.max + r2)*r3.max + r3)*r4.max + r4 ... + rN
                .reduce((r1, r2) -> {
                    final long upperBound = ((long) r1.getUB() + 1) * r2.getUB();
                    if (upperBound > Integer.MAX_VALUE) {
                        throw new RuntimeException("Integer overflow in calculation optimization criteria. "
                                + "Try to decrease number of distinct criterion weights.");
                    }
                    final IntVar result = context.getVar((int) upperBound);
                    context.getSolver().post(ICF.scalar(new IntVar[]{r1, r2}, new int[]{r2.getUB(), 1}, "=", result));
                    return result;
                }).orElse(null);
    }

    @Getter
    private static class OptimizationVar implements Comparable<OptimizationVar> {
        OptimizationVar(final Integer order, final IntVar factor) {
            this.order = order;
            this.factor = factor;
        }
        private Integer order;
        @Setter
        private IntVar factor;

        @Override
        public int compareTo(@NotNull final OptimizationVar o) {
            return o.getOrder().compareTo(getOrder());
        }
    }
}
