package org.autoschedule.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarAccessor;
import org.autoschedule.service.VarSelector;
import org.autoschedule.service.VariableIterator;
import org.chocosolver.solver.variables.IntVar;

/**
 * Default implementation of {@link VarSelector}.
 */
public class VarSelectorImpl implements VarSelector, Cloneable {
	private Set<Integer> days = new HashSet<>();
	private Set<Integer> hours = new HashSet<>();
	private Set<Integer> rooms = new HashSet<>();
	private SolverContext context;

	public VarSelectorImpl(final SolverContext context) {
		this.context = context;
	}

	public VarSelectorImpl clone() throws CloneNotSupportedException {
		VarSelectorImpl copy = (VarSelectorImpl) super.clone();
		copy.context = context;
		copy.days = new HashSet<>(days);
		copy.hours = new HashSet<>(hours);
		copy.rooms = new HashSet<>(rooms);
		return copy;
	}

	@Override
	public Stream<IntVar> teachers() {
		return getVariableStream(VarAccessor.VARTYPE.TEACHER);
	}

	@Override
	public Stream<IntVar> subjects() {
		return getVariableStream(VarAccessor.VARTYPE.SUBJECT);
	}

	@Override
	public Map<Integer, VarSelector> rooms() {
		final Set<Integer> r = getRooms();
		final Map<Integer, VarSelector> result = new HashMap<>(r.size());
		r.forEach(it -> {
			try {
				final VarSelectorImpl sel = clone();
				sel.rooms = new HashSet<>(Collections.singletonList(it));
				result.put(it, sel);
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException("Error cloning VarSelectorImpl");
			}
		});
		return result;
	}

	@Override
	public Map<Integer, VarSelector> days() {
		final Set<Integer> d = getDays();
		final Map<Integer, VarSelector> result = new HashMap<>(d.size());
		d.forEach(it -> {
			try {
				final VarSelectorImpl sel = clone();
				sel.days = new HashSet<>(Collections.singletonList(it));
				result.put(it, sel);
			} catch (final CloneNotSupportedException e) {
				throw new RuntimeException("Error cloning VarSelectorImpl");
			}
		});
		return result;
	}

	@Override
	public Map<Integer, VarSelector> hours() {
		final Set<Integer> h = getHours();
		final Map<Integer, VarSelector> result = new HashMap<>(h.size());
		h.forEach(it -> {
			try {
				final VarSelectorImpl sel = clone();
				sel.hours = new HashSet<>(Collections.singletonList(it));
				result.put(it, sel);
			} catch (final CloneNotSupportedException e) {
				throw new RuntimeException("Error cloning VarSelectorImpl");
			}
		});
		return result;
	}

	@Override
	public Stream<IntVar> teams() {
		return getVariableStream(VarAccessor.VARTYPE.GROUP);
	}

	@Override
	public VarSelector hour(final int hourIndex) {
		try {
			VarSelectorImpl copy = clone();
			if (copy.hours == null) {
				copy.hours = new HashSet<>();
			}
			copy.hours.add(hourIndex);
			return copy;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException("Error cloning VarSelectorImpl");
		}
	}

	@Override
	public VarSelector day(final int dayIndex) {
		try {
			VarSelectorImpl copy = clone();
			if (copy.days == null) {
				copy.days = new HashSet<>();
			}
			copy.days.add(dayIndex);
			return copy;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException("Error cloning VarSelectorImpl");
		}
	}

	@Override
	public VarSelector room(final int roomId) {
		try {
			VarSelectorImpl copy = clone();
			if (copy.rooms == null) {
				copy.rooms = new HashSet<>();
			}
			copy.rooms.add(roomId);
			return copy;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException("Error cloning VarSelectorImpl");
		}
	}

	private Stream<IntVar> getVariableStream(final VarAccessor.VARTYPE suffix) {
		Set<Integer> dd = getDays();
		Set<Integer> hh = getHours();
		Set<Integer> rr = getRooms();
		final int length = dd.size() * hh.size() * rr.size();
		if (length == 0) {
			return Stream.empty();
		} else {
			return StreamSupport.stream(
					Spliterators.spliterator(
							new VariableIterator(context, suffix, dd, hh, rr),
							length,
							Spliterator.DISTINCT), false);
		}
	}

	private Set<Integer> getHours() {
		final Set<Integer> h;
		if (hours == null || hours.isEmpty()) {
			h = new HashSet<>();
			IntStream.range(0, context.getMaxHours()).forEach(h::add);
		} else {
			h = hours;
		}
		return h;
	}

	private Set<Integer> getDays() {
		final Set<Integer> d;
		if (days == null || days.isEmpty()) {
			d = new HashSet<>();
			IntStream.range(0, context.getPlanningPeriod()).forEach(d::add);
		} else {
			d = days;
		}
		return d;
	}

	private Set<Integer> getRooms() {
		if (rooms == null || rooms.isEmpty()) {
			return context.getConcernedRooms();
		} else {
			return rooms;
		}
	}

	public int day() {
		return getDays().iterator().next();
	}
}
