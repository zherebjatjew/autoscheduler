package org.autoschedule.service.impl;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.OptimizationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.LimitationStrategy;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * Weak limitation strategy: applies only limitations, that have no optimization alternative.
 */
@Component
public class WeakLimitationStrategy implements LimitationStrategy {
    @Override
    public IntVar apply(
            @NonNull final SolverContext context,
            @NonNull final Map<LimitationHandler, List<Limitation>> limitations)
    {
        limitations.entrySet().stream()
                .filter(e -> !(e.getKey() instanceof OptimizationHandler))
                .forEach(e -> e.getKey().apply(context, e.getValue()));
        return null;
    }
}
