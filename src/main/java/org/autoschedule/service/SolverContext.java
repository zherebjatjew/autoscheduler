package org.autoschedule.service;

import lombok.NonNull;
import org.autoschedule.domain.AbstractEntity;
import org.autoschedule.domain.Plan;
import org.autoschedule.service.impl.ScheduleImpl;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.variables.IntVar;

import java.util.Set;
import java.util.function.Function;

/**
 * Solution context.
 * <p>Contains information about {@link Plan} is being solved and some related data.</p>
 */
public interface SolverContext {
	@NonNull
	VarSelector selector();

	IntVar getVar(@NonNull String name);

	/**
	 * Create a variable with random unique name.
	 *
	 * @param max optional upper bound (lower bound is always 0)
	 * @return the variable created
	 */
	IntVar getVar(final Integer max);

	IntVar getVar(@NonNull String name, @NonNull Function<Solver, IntVar> ifNotDefined);

	@NonNull
	IntVar getConst(int value);

	@NonNull
	ScheduleImpl convert(@NonNull Solution solution);

	void setVar(@NonNull IntVar val);

	int getPlanningPeriod();

	int getMaxHours();

	@NonNull
	Solver getSolver();

    @NonNull
    Plan getPlan();

	@NonNull
	Set<Integer> getConcernedRooms();

	@NonNull
	Set<Integer> getConcernedTeachers();

	@NonNull
	Set<Integer> getConcernedSubjects();

	@NonNull
	Set<Integer> getConcernedTeams();

    Integer entityToIndex(AbstractEntity entity);

    <T extends AbstractEntity> T indexToEntity(Integer index);
}
