package org.autoschedule.service;

import java.util.Iterator;
import java.util.Set;

import org.chocosolver.solver.variables.IntVar;

/**
 * {@link Iterator} over choco variables.
 * <p>Is constructed by {@link VarSelector}</p>
 */
public class VariableIterator implements Iterator<IntVar> {
	private final Set<Integer> hours;
	private final Set<Integer> rooms;

	private final Iterator<Integer> day;
	private Iterator<Integer> hour;
	private Iterator<Integer> room;

	private final VarAccessor.VARTYPE suffix;
	private final SolverContext context;
	private int curDay = -1;
	private int curHour = -1;

	public VariableIterator(final SolverContext context, final VarAccessor.VARTYPE suffix,
	                 final Set<Integer> days, final Set<Integer> hours, final Set<Integer> rooms)
	{
		this.context = context;
		this.suffix = suffix;
		this.hours = hours;
		this.rooms = rooms;
		day = days.iterator();
		hour = hours.iterator();
		room = rooms.iterator();
	}

	@Override
	public boolean hasNext() {
		return hour.hasNext() || day.hasNext() || room.hasNext();
	}

	@Override
	public IntVar next() {
		if (curDay == -1) {
			curDay = day.next();
		}
		if (curHour == -1) {
			curHour = hour.next();
		}
		if (!room.hasNext()) {
			room = rooms.iterator();
			if (!hour.hasNext()) {
				curDay = day.next();
				hour = hours.iterator();
				curHour = hour.next();
			} else {
				curHour = hour.next();
			}
		}
		final String name = VarAccessor.getVarName(curDay, curHour, room.next(), suffix);
		return context.getVar(name);
	}
}
