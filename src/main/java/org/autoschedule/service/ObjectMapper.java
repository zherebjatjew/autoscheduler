package org.autoschedule.service;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.api.ObjectNotFoundException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Maps objects to integers.
 * <ul>
 *     <li>minimizes absolute values of the integers</li>
 *     <li>fast</li>
 *     <li>thread-safe</li>
 * </ul>
 */
@Slf4j
public class ObjectMapper<T> {
    @Getter
    @Setter
    private int origin = 1;
    private final AtomicInteger index = new AtomicInteger();
    private final Map<Integer, T> indexToObjectMap = new ConcurrentHashMap<>();
    private final Map<T, Integer> objectToIndexMap = new ConcurrentHashMap<>();
    private final Lock lock = new ReentrantLock();

    public <C extends T> C getObject(final Integer idx) {
        if (idx == origin - 1) {
            return null;
        }
        final T result = indexToObjectMap.get(idx);
        if (result == null) {
            throw new ObjectNotFoundException("Object with index " + idx + " was not found.");
        }
        return (C) result;
    }

    public Integer getIndex(final T object) {
        if (object == null) {
            return origin - 1;
        }
        final Integer result = objectToIndexMap.get(object);
        if (result == null) {
            // Some objects might not be pulled in by {@link getConcernedWhatever}, e.g. ones referenced
            // by limitations.
//            throw new ObjectNotFoundException("Object " + object.toString() + " has not been registered.");
            log.debug("Registering object {} by demand", object);
            return registerObject(object);
        }
        return result;
    }

    public Integer registerObject(final T object) {
        log.trace("Registering {}", object);
        if (object == null) {
            log.warn("Registering null");
            return origin - 1;
        }
        lock.lock();
        try {
            final Integer idx = index.getAndIncrement();
            if (objectToIndexMap.put(object, idx) != null || indexToObjectMap.put(idx, object) != null) {
                throw new IllegalArgumentException("Object " + object.toString() + " is registered twice");
            }
            log.trace("Registered with index {}", idx);
            return idx;
        } finally {
            lock.unlock();
        }
    }

    public void clear() {
        lock.lock();
        try {
            indexToObjectMap.clear();
            objectToIndexMap.clear();
            index.set(origin);
        } finally {
            lock.unlock();
        }
    }
}
