package org.autoschedule.service;


import lombok.NonNull;
import org.autoschedule.domain.Limitation;

import java.util.Collection;

/**
 * Base class for converting limitations to solver constraints.
 *
 * Handler for class {@code MyLimitation} must be called {@code MyHandler}.
 */
public interface LimitationHandler {
    void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations);
}
