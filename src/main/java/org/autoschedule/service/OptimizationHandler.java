package org.autoschedule.service;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.chocosolver.solver.variables.IntVar;

import java.util.Collection;


/**
 * Interface is to be applied to implementations of {@link LimitationHandler}.
 * <p>
 *     Calculation goes in one or two steps:
 * </p>
 * <ol>
 *     <li>All {@link LimitationHandler#apply(SolverContext, Collection)} methods are invoked.
 *     {@link OptimizationHandler#getFactor(SolverContext, Collection)} are collected and used as
 *     optimization criteria.</li>
 *     <li>If the previous step didn't give any solution, only {@code apply} methods of handler not implementing
 *     {@code OptimizableHandler} are called. Result of {@code getTotal} are used like before.</li>
 * </ol>
 * <p>
 *     So, there are three types of handler exist:
 * </p>
 * <ul>
 *     <li>Strict: provides {@code apply}, does not implement {@code OptimizableHandler}</li>
 *     <li>Flexible: provides {@code apply}, implements {@code OptimizableHandler}</li>
 *     <li>Weaak: has empty {@code apply}, implements {@code OptimizableHandler}</li>
 * </ul>
 */
public interface OptimizationHandler {
    /**
     * Returns optimization variable. The bigger value the better is the solution.
     * It is highly recommended to use upper limit no more than 1000.
     *
     * @param context
     * @param limitations
     * @return
     */
    IntVar getFactor(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations);
}
