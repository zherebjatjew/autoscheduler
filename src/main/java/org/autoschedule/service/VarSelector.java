package org.autoschedule.service;

import java.util.Map;
import java.util.stream.Stream;

import org.chocosolver.solver.variables.IntVar;

/**
 * This builder provides lists of variables to use while defining conditions.
 *
 * <p>
 *     Example: get all teachers for 1st day, 2nd hour
 * </p>
 * <code>
 *     Collection<Variable> vars = selector.day(1).hour(2).teachers();
 * </code>
 * <p>
 *     Order of calling days and hour does not matter. Also both day and hour are optional: if you omit hour, you'll
 *     get all hours, similarly is for days and rooms.
 * </p>
 * <p>
 *     You are able to select multiple days, rooms, hours by calling day/room/hour multiple times.
 * </p>
 * <p>
 *     You can iterate over teachers, subjects, rooms oneOf teams.
 * </p>
 */
public interface VarSelector {
	Stream<IntVar> teachers();
	Stream<IntVar> subjects();
	Stream<IntVar> teams();

	/**
	 * Allows to iterate over rooms.
	 *
	 * <p>
	 *     Example:
	 * </p>
	 * <code>
	 *     selector.rooms().forEach((roomId, sel) -> sel.teams().forEach(team -> ..));
	 * </code>
	 *
	 * @return map, where key is room identifier, and value is selector, bound by restrictions of current selector + this room
	 */
	Map<Integer, VarSelector> rooms();
	Map<Integer, VarSelector> days();
	Map<Integer, VarSelector> hours();

	VarSelector hour(int hourIndex);
	VarSelector day(int dayIndex);
	VarSelector room(int roomId);
}
