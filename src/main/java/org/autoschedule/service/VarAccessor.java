package org.autoschedule.service;

/**
 * Helper class to construct variable names.
 */
public final class VarAccessor {
	public static String getVarName(final int day, final int hour, final int roomId, final VARTYPE type) {
		return String.format("DAY%d.HOUR%d.ROOM%d.%s", day, hour, roomId, type.name());
	}

	public enum VARTYPE {
		TEACHER,
		SUBJECT,
		GROUP
	}

	private VarAccessor() {}
}
