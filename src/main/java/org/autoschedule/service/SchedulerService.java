package org.autoschedule.service;

import java.util.List;

import lombok.NonNull;
import org.autoschedule.domain.Plan;
import org.autoschedule.api.Schedule;

/**
 * Schedule planning interface.
 */
public interface SchedulerService {
	/**
	 * Build schedules for given learning plans. The process respects existing schedules: new schedules
	 * will not interfere with stored ones.
	 *
	 * @param plan learning plan for all teams
	 * @return built schedules in the same order than the plans
	 */
	@NonNull
	List<Schedule> build(@NonNull final Plan plan);
}
