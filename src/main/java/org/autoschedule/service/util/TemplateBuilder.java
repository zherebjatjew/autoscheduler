package org.autoschedule.service.util;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple template engine.
 */
public class TemplateBuilder {
    private final Map<String, String> attributes = new HashMap<>();
    private final Pattern pattern = Pattern.compile("(?i)(?<!\\\\)\\$\\{([^}]+)(?<!\\\\)\\}");

    public TemplateBuilder attribute(final String name, final Object value) {
        attributes.put(name.toLowerCase(), value == null ? "" : value.toString());
        return this;
    }

    public String build(final String template) {
        if (StringUtils.isEmpty(template)) {
            return "";
        }
        String result = template;
        do {
            final Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                final String name = matcher.group(1).toLowerCase();
                final String value = attributes.get(name);
                result = result.substring(0, matcher.start()) + (value == null ? "" : value) + result.substring(matcher.end());
            } else {
                break;
            }
        } while (true);
        return result;
    }
}
