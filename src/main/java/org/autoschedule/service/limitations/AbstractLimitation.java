package org.autoschedule.service.limitations;

import javax.persistence.DiscriminatorValue;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.AbstractEntity;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.PropertyDescription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.EntityLinks;

import java.util.Map;
import java.util.stream.Collectors;


/**
 * Basic implementation of {@link Limitation}.
 */
@MappedSuperclass
@DiscriminatorValue("Abstract")
@Slf4j
@Configurable
public abstract class AbstractLimitation extends Limitation {

	private static final long serialVersionUID = -4399433807435425444L;

    @JsonIgnore
	@Autowired
    @Transient
	private transient MessageSource messageSource;

    @Autowired
    @Transient
    @JsonIgnore
    private transient EntityLinks entityLinks;

    /**
     * Default implementation returns string with id {@code classname.name}.
     * @return localized name of limitation
     */
    @Override
    public String getName() {
        return getLocalizedMessage(getClass().getSimpleName() + ".name");
    }

    /**
     * Default implementation returns string with id {@code classname.category}.
     * @return localized category name of limitation
     */
    @Override
    public String getCategory() {
        return getLocalizedMessage(getClass().getSimpleName() + ".category");
    }

    /**
     * Default implementation returns string with id {@code classname.description}.
     * @return localized description of limitation
     */
    @Override
    public String getDescription() {
        return getLocalizedMessage(getClass().getSimpleName() + ".description");
    }

    /**
     * Default implementation of {@code getPropertyDescriptions}.
     * <p>It's enough for most of cases, you rarely need to override it.</p>
     * <p>The logic is to obtain add property names and construct </p>
     * @return list of property descriptions
     */
	@Override
	public Map<String, PropertyDescription> getProperties() {
        final String prefix = getClass().getSimpleName() + '.';
        return getPropertyValues().entrySet().stream()
                .map(entry -> {
                    final String code = prefix + entry.getKey();
                    return PropertyDescription.builder()
                            .id(entry.getKey())
                            .name(getLocalizedMessage(code + PropertyDescription.NAME_SUFFIX))
                            .description(getLocalizedMessage(code + PropertyDescription.DESCRIPTION_SUFFIX))
                            .value(entry.getValue())
                            .valueProvider(getValueProvider(code))
                            .build();
                })
                .collect(Collectors.toMap(PropertyDescription::getId, c -> c));
	}

    /**
     * Constructs link to object list from provides name, given in localized messages.
     * @param propertyName spring id in messages
     * @return hyperlink
     */
    @NonNull
    protected String getValueProvider(final String propertyName) {
        if (propertyName.endsWith(PropertyDescription.UUID_SUFFIX)) {
            final String rel = getLocalizedMessage(propertyName + PropertyDescription.VALUE_PROVIDER_SUFFIX);
            if (!rel.isEmpty()) {
                final String className = rel.contains(".")
                        ? rel
                        : AbstractEntity.class.getPackage().getName() + '.' + StringUtils.capitalize(rel);
                try {
                    final Class<?> clazz = AbstractEntity.class.getClassLoader().loadClass(className);
                    return getEntityLinks().linkToCollectionResource(clazz).getHref();
                } catch (final ClassNotFoundException e) {
                    log.error("Class " + className + " was not found", e);
                }
            }
        }
        return "";
    }

    protected String getLocalizedMessage(final String id) {
        try {
            return messageSource.getMessage(id, null, LocaleContextHolder.getLocale());
        } catch (final NoSuchMessageException e) {
            log.warn("Message {} not found", id);
            return "";
        }
	}

    protected EntityLinks getEntityLinks() { return entityLinks; }
}
