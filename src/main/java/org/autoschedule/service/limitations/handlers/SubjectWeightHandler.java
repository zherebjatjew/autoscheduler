package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectType;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.OptimizationHandler;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.WeightPropagator;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Gives estimation of subject position in timetable according to calculated subject weights.
 */
@Component
public class SubjectWeightHandler implements LimitationHandler, OptimizationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        // The handler brings no strict constraints.
    }

    @Override
    public IntVar getFactor(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        // limitations do not define any runtime information, but just initiate using of the handler
        final Map<Integer, Integer> weights = new HashMap<>();
        context.getConcernedSubjects().stream()
                .map(context::indexToEntity)
                .map(subject -> (Subject) subject)
                .forEach(subject -> subject.getTypes().stream()
                        .map(SubjectType::getWeight)
                        .filter(w -> w != null).min(Integer::compare)
                        .ifPresent(v -> weights.put(context.entityToIndex(subject), v)));
        if (weights.isEmpty()) {
            return null;
        }
        final int[][] ww = new int[weights.size()][2];
        final int[] i = new int[]{0};
        weights.forEach((k, v) -> {
            ww[i[0]][0] = k;
            ww[i[0]][1] = v;
            i[0]++;
        });
        final IntVar[] estimations = context.selector().days().entrySet().stream()
                .map(e -> {
                    final IntVar[] vv = e.getValue().subjects().toArray(IntVar[]::new);
                    final IntVar res = context.getVar(vv.length);
                    context.getSolver().post(new Constraint("Weights", WeightPropagator.create(vv, ww, res)));
                    return res;
                }).toArray(IntVar[]::new);
        final int upperBound = estimations[0].getUB()*estimations.length;
        final IntVar res = context.getVar(upperBound);
        context.getSolver().post(ICF.sum(estimations, res));
        return res;
    }
}

