package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Component
public class TeacherTimeCollisionHandler implements LimitationHandler {

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final int[] teacherIds = context.getConcernedTeachers()
                .stream()
                .mapToInt(it -> it)
                .toArray();
        final IntVar[] cardinality = new IntVar[teacherIds.length];
        final VarSelector selector = context.selector();

        selector.days().forEach((d, ds) -> ds.hours().forEach((h, hs) -> {
            final IntVar[] teacherVariables = hs.teachers().toArray(IntVar[]::new);
            // Each teacherId-to-teacherVar comparison must have it's own copy of LE1.
            // Otherwise we would have dead loop inside of solver.
            for (int i = 0; i < teacherIds.length; i++) {
                final String le1 = "LE1" + d + '.' + h + '.' + teacherIds[i];
                cardinality[i] = VF.enumerated(le1, 0, 1, context.getSolver());
            }
            context.getSolver().post(
                    ICF.global_cardinality(teacherVariables, teacherIds, cardinality, false)
            );
        }));
    }
}
