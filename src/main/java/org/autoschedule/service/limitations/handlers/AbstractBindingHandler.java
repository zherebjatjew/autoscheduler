package org.autoschedule.service.limitations.handlers;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.constraints.IntConstraintFactory;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

import java.util.Collection;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

public abstract class AbstractBindingHandler implements LimitationHandler {

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final Tuples tuples = getUniqueTuples(context);
        context.selector().days().forEach((day, daySelector) ->
            daySelector.hours().forEach((hour, hourSelector) ->
                hourSelector.rooms().forEach((room, roomSelector) -> {
                    try {
                        context.getSolver().post(IntConstraintFactory.table(
                                getFirstParameter(roomSelector),
                                getSecondParameter(roomSelector),
                                tuples, ""));
                    } catch (final NoSuchElementException e) {
                        throw new RuntimeException("Corrupted VarSelector: must have one item but is empty");
                    }
                })
            )
        );
    }

    public abstract void collectPairs(final SolverContext context, final Set<Tuple> receiver);
    public abstract IntVar getFirstParameter(final VarSelector selector);
    public abstract IntVar getSecondParameter(final VarSelector selector);

    private Tuples getUniqueTuples(final SolverContext context) {
        final Set<Tuple> tps = new HashSet<>();
        collectPairs(context, tps);
        final Tuples tuples = new Tuples();
        tuples.add(0, 0);
        tps.forEach(it -> tuples.add(it.firstId, it.secondId));
        return tuples;
    }

    @EqualsAndHashCode
    @AllArgsConstructor
    public static class Tuple {
        private Integer firstId;
        private Integer secondId;
    }
}
