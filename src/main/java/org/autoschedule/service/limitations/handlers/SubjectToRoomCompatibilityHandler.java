package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Room;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.custom.SubjectToRoomCompatibilityLimitation;
import org.chocosolver.solver.constraints.CustomConstraintFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collection;
import java.util.stream.IntStream;


@Component
public class SubjectToRoomCompatibilityHandler implements LimitationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final MultiValueMap<Room, SubjectToRoomCompatibilityLimitation> byRoom
                = new LinkedMultiValueMap<>(limitations.size());
        limitations.forEach(item -> {
            final SubjectToRoomCompatibilityLimitation lim = (SubjectToRoomCompatibilityLimitation) item;
            byRoom.add(lim.getRoom(), lim);
        });
        byRoom.forEach((room, lims) -> {
            if (room != null) {
                final int[] subjectIdxs = IntStream.concat(IntStream.of(0), lims.stream()
                        .map(SubjectToRoomCompatibilityLimitation::getSubject)
                        .mapToInt(context::entityToIndex))
                        .toArray();
                final Integer roomIdx = context.entityToIndex(room);
                context.selector().room(roomIdx).subjects().forEach(
                        var -> context.getSolver().post(CustomConstraintFactory.member(var, subjectIdxs)));
            }
        });
        final int[] allSubjects = byRoom.values().stream()
                .flatMap(Collection::stream)
                .map(SubjectToRoomCompatibilityLimitation::getSubject)
                .mapToInt(context::entityToIndex)
                .distinct()
                .toArray();
        context.selector().rooms().entrySet().stream()
                .filter(it -> !byRoom.containsKey((Room) context.indexToEntity(it.getKey())))
                .forEach(it -> it.getValue().subjects().forEach(
                        var -> context.getSolver().post(CustomConstraintFactory.not_member(var, allSubjects))));
    }
}
