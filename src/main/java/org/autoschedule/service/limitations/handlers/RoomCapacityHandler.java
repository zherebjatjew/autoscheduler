package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.domain.Room;
import org.autoschedule.domain.Team;
import org.autoschedule.domain.TeamRepository;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.constraints.CustomConstraintFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;


@Component
public class RoomCapacityHandler implements LimitationHandler {

    @Autowired
    private transient PlanRepository planRepository;

    @Autowired
    private transient TeamRepository teamRepository;

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final Collection<UUID> concernedTeams = planRepository.getConcernedTeams(context.getPlan().getId()).stream()
                .map(Team::getId)
                .collect(Collectors.toList());
        if (concernedTeams.isEmpty()) {
            return;
        }
        final Iterable<TeamRepository.TeamSize> teams = teamRepository.getTeamSizes(concernedTeams);
        context.selector().rooms().forEach((roomId, selector) -> {
            final Room room = context.indexToEntity(roomId);
            final int[] teamsThatFit = getTeamsSmallerThan(room, teams, context);
            selector.teams().forEach(t -> context.getSolver().post(CustomConstraintFactory.member(t, teamsThatFit)));
        });
    }

    private int[] getTeamsSmallerThan(
            @NonNull final Room room,
            @NonNull final Iterable<TeamRepository.TeamSize> teams,
            @NonNull final SolverContext context)
    {
        return IntStream.concat(IntStream.of(0), StreamSupport.stream(teams.spliterator(), false)
                .filter(it -> it.getStudentCount() <= room.getCapacity())
                .map(TeamRepository.TeamSize::getTeamId)
                .map(teamRepository::findOne)
                .mapToInt(context::entityToIndex))
                .toArray();
    }
}
