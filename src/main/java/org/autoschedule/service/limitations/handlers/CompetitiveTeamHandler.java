package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Student;
import org.autoschedule.domain.Team;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.nary.automata.FA.FiniteAutomaton;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.autoschedule.service.limitations.AutomationUtils.oneOf;
import static org.autoschedule.service.limitations.AutomationUtils.or;
import static org.autoschedule.service.limitations.AutomationUtils.wrap;


@Component
public class CompetitiveTeamHandler implements LimitationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final Set<Integer> concernedTeams = context.getConcernedTeams();
        final Set<Integer> remainingTeams = new HashSet<>(concernedTeams);
        final Collection<String> competitors = concernedTeams.stream()
                .map(team -> getCompetitors(context, team))
                .filter(it -> it.size() > 1)
                .distinct()
                .map(it -> {
                    remainingTeams.removeAll(it);
                    return wrap(oneOf(it, null)) + '?';
                })
                .collect(Collectors.toList());
        if (competitors.isEmpty()) {
            // No competitive teams found
            return;
        }
        final String nonCompetitors = wrap(or(oneOf(remainingTeams, null), 0)) + '*';
        final String expression = nonCompetitors + oneOf(competitors.stream()
                .map(it -> it + nonCompetitors)
                .collect(Collectors.toList()));

        final VarSelector selector = context.selector();

        selector.days().forEach((d, ds) -> ds.hours().forEach((h, hs) -> {
            final IntVar[] teamVariables = hs.teams().toArray(IntVar[]::new);
            context.getSolver().post(
                    ICF.regular(teamVariables, new FiniteAutomaton(expression))
            );
        }));
    }

    private Collection<Integer> getCompetitors(final SolverContext context, final Integer teamId) {
        final Collection<Student> students = new ArrayList<>(
                ((Team) context.indexToEntity(teamId)).getStudents()
        );
        return Stream.concat(
                Stream.of(teamId),
                context.getConcernedTeams().stream()
                        .filter(id -> !(id.equals(teamId)))
                        .filter(id -> new ArrayList<>(
                                        ((Team) context.indexToEntity(id)).getStudents()).retainAll(students)
                        )
        ).sorted().collect(Collectors.toSet());
    }
}
