package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Component
public class EventIntegrityHandler implements LimitationHandler {

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        context.selector().days().forEach((di, day) ->
            day.hours().forEach((hi, hour) ->
                hour.rooms().forEach((ri, room) -> {
                    final IntVar[] variables = {
                            room.teachers().findFirst().get(),
                            room.subjects().findFirst().get(),
                            room.teams().findFirst().get()
                    };
                    final String name = "ALL.OR.NOTHING." + di + '.' + hi + '.' + ri;
                    final int[] available = {0, 3};
                    final Solver solver = context.getSolver();
                    final IntVar cardinality = VF.enumerated(name, available, solver);
                    solver.post(ICF.count(0, variables, cardinality));
                })
            )
        );
    }
}
