package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.IntConstraintFactory;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Component
public class OverlappingHandler implements LimitationHandler {

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final VarSelector selector = context.selector();
        selector.days().forEach((d, ds) -> ds.hours().forEach((h, hs) -> {
            final IntVar[] teachers = hs.teachers().toArray(IntVar[]::new);
            final IntVar[] groups = hs.teams().toArray(IntVar[]::new);
            final Solver solver = context.getSolver();
            if (groups.length > 0) {
                solver.post(IntConstraintFactory.alldifferent_except_0(groups));
            }
            if (teachers.length > 0) {
                solver.post(IntConstraintFactory.alldifferent_except_0(teachers));
            }
        }));
    }
}
