package org.autoschedule.service.limitations.handlers;

import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Set;


@Component
public class TeamToSubjectBindingHandler extends AbstractBindingHandler {

    @Override
    public void collectPairs(final SolverContext context, final Set<Tuple> receiver) {
        context.getPlan().getSubjects().forEach(it ->
            receiver.add(new Tuple(
                    context.entityToIndex(it.getSubject()),
                    context.entityToIndex(it.getTeam()))
            )
        );
    }

    @Override
    public IntVar getFirstParameter(final VarSelector selector) {
        return selector.subjects().findFirst().get();
    }

    @Override
    public IntVar getSecondParameter(final VarSelector selector) {
        return selector.teams().findFirst().get();
    }

}
