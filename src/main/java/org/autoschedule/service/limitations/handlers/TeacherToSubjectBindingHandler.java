package org.autoschedule.service.limitations.handlers;

import org.apache.commons.collections.CollectionUtils;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectHour;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;


@Component
public class TeacherToSubjectBindingHandler extends AbstractBindingHandler {

    @Override
    public void collectPairs(final SolverContext context, final Set<Tuple> receiver) {
        // gather unique subjects
        final Set<Subject> subjects = context.getPlan().getSubjects().stream()
                .map(SubjectHour::getSubject)
                .filter(it -> it != null && !CollectionUtils.isEmpty(it.getTeachers()))
                .collect(Collectors.toSet());
        // bind them to suitable groups
        final Set<Integer> concernedTeachers = context.getConcernedTeachers();
        subjects.forEach(
                subject -> subject.getTeachers().stream()
                        .filter(it -> concernedTeachers.contains(context.entityToIndex(it)))
                        .forEach(teacher -> receiver.add(new Tuple(
                                                context.entityToIndex(subject),
                                                context.entityToIndex(teacher))
                                )
                        )
        );
    }

    @Override
    public IntVar getFirstParameter(final VarSelector selector) {
        return selector.subjects().findFirst().get();
    }

    @Override
    public IntVar getSecondParameter(final VarSelector selector) {
        return selector.teachers().findFirst().get();
    }

}
