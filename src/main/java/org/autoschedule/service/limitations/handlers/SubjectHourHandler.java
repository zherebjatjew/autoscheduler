package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.domain.SubjectHour;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.constraints.CustomConstraintFactory;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Slf4j
@Component
public class SubjectHourHandler implements LimitationHandler {

    @Autowired
    private transient PlanRepository planRepository;

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final UUID planId = context.getPlan().getId();
        final Iterable<SubjectHour> hours = planRepository.getHourSubtotals(planId);
        final IntVar[] teams = context.selector().teams().toArray(IntVar[]::new);
        final IntVar[] subjects = context.selector().subjects().toArray(IntVar[]::new);

        hours.forEach(it -> {
            final int teamIdx = context.entityToIndex(it.getTeam());
            final int subjectIdx = context.entityToIndex(it.getSubject());
            context.getSolver().post(CustomConstraintFactory.mapcount(
                    teams, subjects, teamIdx, subjectIdx, context.getConst(it.getHours())));
        });

        // These constraints are excessive, but they make mapcount degrade faster.
        StreamSupport.stream(hours.spliterator(), false)
                .collect(Collectors.toMap(SubjectHour::getTeam, SubjectHour::getHours, (v1, v2) -> v1 + v2))
                .forEach((k, v) -> context.getSolver().post(
                        ICF.count(context.entityToIndex(k), teams, context.getConst(v))));

        StreamSupport.stream(hours.spliterator(), false)
                .collect(Collectors.toMap(SubjectHour::getSubject, SubjectHour::getHours, (v1, v2) -> v1 + v2))
                .forEach((k, v) -> context.getSolver().post(
                        ICF.count(context.entityToIndex(k), subjects, context.getConst(v))));
    }
}
