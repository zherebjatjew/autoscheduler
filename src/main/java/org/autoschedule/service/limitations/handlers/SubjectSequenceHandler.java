package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Subject;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.AutomationUtils;
import org.autoschedule.service.limitations.custom.SubjectSequenceLimitation;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.nary.automata.FA.FiniteAutomaton;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
@Slf4j
public class SubjectSequenceHandler implements LimitationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final MultiValueMap<Subject, SubjectSequenceLimitation> bySubject
                = new LinkedMultiValueMap<>(limitations.size());
        limitations.forEach(item -> {
            final SubjectSequenceLimitation lim = (SubjectSequenceLimitation) item;
            bySubject.add(lim.getSubject(), lim);
        });
        bySubject.forEach((subject, lims) -> {
            if (subject != null) {
                final int count = lims.stream().map(SubjectSequenceLimitation::getCount).max(Integer::compare).get();
                if (count > 1) {
                    final String subjRegexp = getExpression(context, subject, count);
                    final String teamRegexp = getGroundExpression(context, subject, context.getConcernedTeams(), count);
                    final String teacherRegexp = getGroundExpression(context, subject, context.getConcernedTeachers(), count);
                    log.debug("subject regex = {}", subjRegexp);
                    log.debug("team regex = {}", teamRegexp);
                    log.debug("teacher regex = {}", teacherRegexp);
                    context.selector().rooms()
                        .forEach((room, rs) -> rs.days()
                            .forEach((d, sel) -> {
                                        context.getSolver().post(ICF.regular(
                                                sel.subjects().toArray(IntVar[]::new),
                                                new FiniteAutomaton(subjRegexp)));
                                        context.getSolver().post(ICF.regular(
                                                interleaved(sel.teams(), sel.subjects()),
                                                new FiniteAutomaton(teamRegexp)));
                                        context.getSolver().post(ICF.regular(
                                                interleaved(sel.teachers(), sel.subjects()),
                                                new FiniteAutomaton(teacherRegexp)));
                                    }
                            )
                        );
                }
            }
        });
    }

    /**
     * Returns regular expression describing subject sequence:
     * <p>
     *     0 or <code>count</code> in a row of <code>subject</code> classes.
     * </p>
     * <code>
     *     (&lt;all subjects&gt;)|((&lt;all subjects but the subject&gt;)*
     *     &lt;subject&gt;{&lt;count&gt;}(&lt;all subjects but the subject&gt;)*)
     * </code>
     * @param context solution context
     * @param subject id of subject
     * @param count sequence length
     * @return regular expression for automation constraint
     */
    @NonNull
    private String getExpression(@NonNull final SolverContext context, @NonNull final Subject subject, final int count) {
        final Set<Integer> concernedSubjects = context.getConcernedSubjects();
        final Integer subjectIdx = context.entityToIndex(subject);
        String theOthers = AutomationUtils.or(AutomationUtils.oneOf(concernedSubjects, subjectIdx), 0);
        final String thisOne = AutomationUtils.intToReg(subjectIdx);
        theOthers = AutomationUtils.wrap(theOthers);
        return AutomationUtils.wrap(theOthers + '*'
                + AutomationUtils.wrap(AutomationUtils.times(thisOne,  count)))
                + '*' + theOthers + '*';
    }

    /**
     * Compiles regular expression.
     * <code>
     * (.[^subject])*(((room1subject){count})|(..)|((roomNsubject){0,count})(.[^subject])*)*
     * </code>
     *
     * @param context solution context
     * @param subject subject
     * @param objects objects related to the subject (teachers or teams)
     * @param count number of subjects in a row
     * @return regular expression for automation constraint
     */
    @NonNull
    private String getGroundExpression(
            @NonNull final SolverContext context,
            @NonNull final Subject subject,
            @NonNull final Set<Integer> objects,
            final int count)
    {
        final Set<Integer> concernedSubjects = context.getConcernedSubjects();
        final Integer subjectIdx = context.entityToIndex(subject);
        final String allButThis = AutomationUtils.wrap(
                AutomationUtils.wrap(AutomationUtils.or("0", AutomationUtils.oneOf(objects, 0)))
                + AutomationUtils.wrap(AutomationUtils.or("0", AutomationUtils.oneOf(concernedSubjects, subjectIdx)))) + '*';
        final String thisOnly = AutomationUtils.wrap(objects.stream()
                .map(it -> AutomationUtils.intToReg(it) + AutomationUtils.intToReg(subjectIdx))
                        .map(it -> AutomationUtils.times(it, count))
                        .map(AutomationUtils::wrap)
                        .collect(Collectors.joining("|")));
        return allButThis + AutomationUtils.wrap(thisOnly + allButThis) + '*';
    }

    @NonNull
    private IntVar[] interleaved(@NonNull final Stream<IntVar> s1, @NonNull final Stream<IntVar> s2) {
        final List<IntVar> l1 = s1.collect(Collectors.toList());
        final List<IntVar> l2 = s2.collect(Collectors.toList());
        assert l1.size() == l2.size();
        final IntVar[] result = new IntVar[l1.size()*2];
        for (int i = 0; i < l1.size(); i++) {
            result[i*2 + 0] = l1.get(i);
            result[i*2 + 1] = l2.get(i);
        }
        return result;
    }
}
