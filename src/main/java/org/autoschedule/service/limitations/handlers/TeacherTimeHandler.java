package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.api.Index;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Teacher;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.custom.TeacherTimeLimitation;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

@Component
public class TeacherTimeHandler implements LimitationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        // Group by teachers
        final MultiValueMap<Teacher, TeacherTimeLimitation> byTeachers = new LinkedMultiValueMap<>(limitations.size());
        limitations.forEach(it -> {
            final TeacherTimeLimitation lim = (TeacherTimeLimitation) it;
            byTeachers.add(lim.getTeacher(), lim);
        });
        // Apply teacher by teacher
        final Set<IntVar> teacherVars = new ConcurrentSkipListSet<>();
        byTeachers.forEach((teacher, limits) -> {
            final Collection<Index> denied = limits.stream().map(TeacherTimeLimitation::getPosition).collect(Collectors.toSet());
            context.selector().days().forEach((d, dsel) -> {
                dsel.hours().forEach((h, hsel) -> {
                    if (denied.contains(new Index(d, h))) {
                        teacherVars.addAll(hsel.teachers().collect(Collectors.toList()));
                    }
                });
            });
            if (!denied.isEmpty()) {
                context.getSolver().post(ICF.count(
                        context.entityToIndex(teacher),
                        teacherVars.stream().toArray(IntVar[]::new),
                        context.getConst(0)
                ));
            }
        });
    }
}
