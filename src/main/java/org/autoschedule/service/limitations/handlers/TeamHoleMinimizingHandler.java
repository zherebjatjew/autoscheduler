package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.OptimizationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.defaults.TeamHoleMinimizingLimitation;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.TwoDimensionalWindowCountPropagator;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Collection;


/**
 * Minimizes number of empty hours between classes for each team.
 *
 * @see {@link TeamHoleMinimizingLimitation}.
 */
@Component
public class TeamHoleMinimizingHandler implements LimitationHandler, OptimizationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
    }

    @Override
    public IntVar getFactor(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final int maxWindows = getMaxWindows(context);
        final IntVar[] counters = context.selector().days().entrySet().stream().map(day -> {
            final IntVar[][] vars = day.getValue().hours().entrySet().stream()
                    .sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                    .map(e -> e.getValue().teams().toArray(IntVar[]::new))
                    .toArray(IntVar[][]::new);
            // Time windows for groups separately for each day
            final IntVar counter = context.getVar(maxWindows);
            context.getSolver().post(new Constraint(
                    "WindowsOfDay" + day.getKey(),
                    TwoDimensionalWindowCountPropagator.create(vars, counter, 0)));
            return counter;
        }).toArray(IntVar[]::new);
        return getTotal(context, maxWindows, counters);
    }

    protected IntVar getTotal(final @NonNull SolverContext context, final int maxWindows, final IntVar[] counters) {
        if (counters.length == 0) {
            return null;
        }
        final int maxWindowsTotal = maxWindows * counters.length;
        final IntVar sum = context.getVar(maxWindowsTotal);
        context.getSolver().post(ICF.sum(counters, sum));
        final IntVar unified = context.getVar(maxWindowsTotal);
        context.getSolver().post(ICF.arithm(unified, "-", sum, "=", maxWindowsTotal));
        return unified;
    }

    protected int getMaxWindows(final @NonNull SolverContext context) {
        return context.getConcernedTeams().size()*context.getMaxHours() - 1;
    }
}
