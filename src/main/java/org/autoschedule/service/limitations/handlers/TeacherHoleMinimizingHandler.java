package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.TwoDimensionalWindowCountPropagator;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class TeacherHoleMinimizingHandler extends TeamHoleMinimizingHandler {
    @Override
    public IntVar getFactor(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final int maxWindows = getMaxWindows(context);
        final IntVar[] counters = context.selector().days().entrySet().stream().map(day -> {
            final IntVar[][] vars = day.getValue().hours().entrySet().stream()
                    .sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                    .map(e -> e.getValue().teams().toArray(IntVar[]::new))
                    .toArray(IntVar[][]::new);
            // Time windows for teachers separately for each day
            final IntVar counter = context.getVar(maxWindows);
            context.getSolver().post(new Constraint(
                    "WindowsOfDay" + day.getKey(),
                    TwoDimensionalWindowCountPropagator.create(vars, counter, 0)));
            return counter;
        }).toArray(IntVar[]::new);
        return getTotal(context, maxWindows, counters);
    }
}
