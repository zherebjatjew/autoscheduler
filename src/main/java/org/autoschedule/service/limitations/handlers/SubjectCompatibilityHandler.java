package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.SubjectHourRepository;
import org.autoschedule.domain.SubjectRepository;
import org.autoschedule.domain.Team;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.OptimizationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.VarSelector;
import org.autoschedule.service.limitations.defaults.SubjectCompatibilityLimitation;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.CustomConstraintFactory;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.LogicalConstraintFactory;
import org.chocosolver.solver.constraints.arithm.ComparisonPropagator;
import org.chocosolver.solver.variables.IntVar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;


@Component
@Slf4j
public class SubjectCompatibilityHandler implements LimitationHandler, OptimizationHandler {

    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private SubjectHourRepository subjectHourRepository;

    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
    }

    @Override
    public IntVar getFactor(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final IntVar[] factors = limitations.stream().map(it -> (SubjectCompatibilityLimitation) it).map(lim -> {
            final Set<Subject> subjectsOfA = subjectRepository.findSubjectsOfType(lim.getTypeA());
            if (subjectsOfA.isEmpty()) {
                return Collections.emptyList();
            }
            final Set<Subject> subjectsOfB = subjectRepository.findSubjectsOfType(lim.getTypeB());
            if (subjectsOfB.isEmpty()) {
                return Collections.emptyList();
            }
            return context.selector().days().entrySet().stream().map(entry -> {
                final VarSelector selector = entry.getValue();
                final IntVar[] teams = selector.teams().toArray(IntVar[]::new);
                final IntVar[] subjects = selector.subjects().toArray(IntVar[]::new);
                final IntVar[] presenceOfA = getPresenceOf(subjectsOfA, teams, subjects, context);
                if (presenceOfA.length == 0) {
                    return null;
                }
                final IntVar[] presenceOfB = getPresenceOf(subjectsOfB, teams, subjects, context);
                if (presenceOfB.length == 0) {
                    return null;
                }
                final IntVar[] successes = cross(presenceOfA, presenceOfB, context);
                return sum(successes, context);
            }).filter(it -> it != null).collect(Collectors.toList());
        }).flatMap(Collection::stream).toArray(IntVar[]::new);
        return sum(factors, context);
    }

    private IntVar[] cross(final IntVar[] a, final IntVar[] b, final SolverContext context) {
        final IntVar[] successes = new IntVar[a.length * b.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                final IntVar sum = context.getVar(2);
                context.getSolver().post(ICF.sum(new IntVar[]{a[i], b[j]}, sum));
                final IntVar satisfied = context.getVar(1);
                LogicalConstraintFactory.ifThenElse(
                        ICF.arithm(sum, "<", 2),
                        ICF.arithm(satisfied, "=", 1),
                        ICF.arithm(satisfied, "=", 0));
                successes[i * b.length + j] = satisfied;
            }
        }
        return successes;
    }

    private IntVar[] getPresenceOf(
            final Set<Subject> subjectVals, final IntVar[] teams, final IntVar[] subjects, final SolverContext context)
    {
        return subjectVals.stream()
                .map(subject -> context.getConcernedTeams().stream().map(teamIdx -> {
                    final int maxHours = getMaxHours(context.indexToEntity(teamIdx), subject, context);
                    if (maxHours == 0) {
                        return null;
                    }
                    final IntVar count = context.getVar(maxHours);
                    context.getSolver().post(CustomConstraintFactory.mapcount(
                            teams, subjects, teamIdx, context.entityToIndex(subject), count));
                    IntVar presented = context.getVar(1);
                    context.getSolver().post(new Constraint("Comparison", new ComparisonPropagator(presented, count, ">", 0)));
                    return presented;
                })
                .filter(it -> it != null)
                .collect(Collectors.toList())).flatMap(Collection::stream).toArray(IntVar[]::new);
    }

    private int getMaxHours(final Team team, final Subject subject, final SolverContext context) {
        final Integer res = subjectHourRepository.getHours(context.getPlan(), team, subject);
        return res == null ? 0 : res;
    }

    private IntVar sum(final IntVar[] values, final SolverContext context) {
        assert values != null;
        if (values.length == 0) {
            return null;
        } else if (values.length == 1) {
            return values[0];
        } else {
            final int max = Arrays.stream(values).mapToInt(IntVar::getUB).sum();
            final IntVar result = context.getVar(max);
            context.getSolver().post(ICF.sum(values, result));
            return result;
        }
    }
}
