package org.autoschedule.service.limitations.handlers;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.Teacher;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.autoschedule.service.limitations.custom.TeacherLoadLimitation;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collection;


@Component
public class TeacherLoadHandler implements LimitationHandler {
    @Override
    public void apply(@NonNull final SolverContext context, @NonNull final Collection<Limitation> limitations) {
        final MultiValueMap<Teacher, TeacherLoadLimitation> byTeachers = new LinkedMultiValueMap<>(limitations.size());
        limitations.forEach(it -> {
            final TeacherLoadLimitation lim = (TeacherLoadLimitation) it;
            byTeachers.add(lim.getTeacher(), lim);
        });
        byTeachers.forEach((teacher, lims) -> {
            final Integer min = lims.stream().map(TeacherLoadLimitation::getMinHours).max(Integer::compareTo).get();
            final Integer max = lims.stream().map(TeacherLoadLimitation::getMinHours).min(Integer::compareTo).get();
            if (min != 0 || max != 0) {
                final IntVar[] allHours = context.selector().teachers().toArray(IntVar[]::new);
                final IntVar limits = VF.bounded(
                        getVarName(context, teacher, min, max),
                        min,
                        max == 0
                                ? Integer.MAX_VALUE / 2
                                : max,
                        context.getSolver());

                context.getSolver().post(ICF.count(
                        context.getConst(context.entityToIndex(teacher)), allHours, limits));
            }
        });
    }

    private String getVarName(final SolverContext context, final Teacher teacher, final Integer min, final Integer max) {
        final StringBuilder builder = new StringBuilder();
        if (teacher == null) {
            builder.append("TEACHER.ALL.");
        } else {
            builder.append("TEACHER.");
            builder.append(context.entityToIndex(teacher));
            builder.append('.');
        }
        builder.append(min);
        if (max != null) {
            builder.append("TO");
            builder.append(max);
        }
        return builder.toString();
    }
}
