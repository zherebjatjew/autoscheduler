package org.autoschedule.service.limitations.custom;


import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Room;
import org.autoschedule.domain.Subject;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

/**
 * Defines with {@link org.autoschedule.domain.Room}s
 * are allowed to have particular {@link org.autoschedule.domain.Subject}.
 * <p>
 *     Until subject has no <code>SubjectToRoomCompatibilityLimitation</code> assigned,
 *     if can be placed to any room that fits other constraints such as
 *     {@link org.autoschedule.service.limitations.defaults.RoomCapacityLimitation}.
 * </p>
 * <p>
 *     If there are multiple SubjectToRoomCompatibilityLimitation for one subject, it's rooms will be concatenated.
 * </p>
 */
@Entity
@Inheritance
@DiscriminatorValue("SubjectToRoomCompatibility")
@Slf4j
public class SubjectToRoomCompatibilityLimitation extends AbstractLimitation {

	private static final long serialVersionUID = -126595974899251058L;

	@Override
	public Subject getSubject() {
        return super.getSubject();
	}

	@Override
	public void setSubject(final Subject subject) {
		super.setSubject(subject);
	}

	@Override
	public Room getRoom() {
		return super.getRoom();
	}

	@Override
	public void setRoom(final Room room) {
		super.setRoom(room);
	}

}
