package org.autoschedule.service.limitations.custom;

import lombok.Getter;
import lombok.Setter;
import org.autoschedule.api.Index;
import org.autoschedule.dao.IndexConverter;
import org.autoschedule.domain.Teacher;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

/**
 * Defines days/hours when particular teacher is not available.
 * {@see TeacherLoadLimitation}
 */
@Getter
@Setter
@Entity
@Inheritance
@DiscriminatorValue("TeacherBusy")
public class TeacherTimeLimitation extends AbstractLimitation {

	private static final long serialVersionUID = -3748279504426645387L;

	@Convert(converter = IndexConverter.class)
	private Index position;

	@Override
	public Teacher getTeacher() {
		return super.getTeacher();
	}

	@Override
	public void setTeacher(final Teacher teacher) {
		super.setTeacher(teacher);
	}

}
