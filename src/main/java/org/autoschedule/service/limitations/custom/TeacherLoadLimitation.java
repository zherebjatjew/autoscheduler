package org.autoschedule.service.limitations.custom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NonNull;
import org.autoschedule.domain.Teacher;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.Transient;

/**
 * Defines amount of hours for particular {@link Teacher}.
 */
@Entity
@Inheritance
@DiscriminatorValue("TeacherLoad")
public class TeacherLoadLimitation extends AbstractLimitation {

	private static final long serialVersionUID = -8486143578332001921L;
	private static final String MIN_HOUR_PROPERTY = "hours.min";
	private static final String MAX_HOUR_PROPERTY = "hours.max";

	/**
	 * Get minial load of teacher.
	 *
	 * @return minimal load
	 */
	@NonNull
    @Transient
    @JsonIgnore
	public Integer getMinHours() {
		final String value = getProperty(MIN_HOUR_PROPERTY);
		if (value == null) {
			return 0;
		}
		return Integer.parseInt(value);
	}

    @Transient
    @JsonIgnore
	public void setMinHours(final Integer value) {
		setProperty(MIN_HOUR_PROPERTY, value == null ? null : value.toString());
	}

	/**
	 * @return maximal allowed load. <code>null</code> - unlimited
	 */
    @NonNull
    @Transient
    @JsonIgnore
	public Integer getMaxHours() {
		final String value = getProperty(MAX_HOUR_PROPERTY);
		if (value == null) {
			return null;
		}
		return Integer.parseInt(value);
	}

    @Transient
    @JsonIgnore
	public void setMaxHours(final Integer value) {
		setProperty(MAX_HOUR_PROPERTY, value == null ? null : value.toString());
	}

	/**
	 * I do not like having soft links in RDB, as they are not considered during data modifications.
	 * Maybe it makes sense to add reference to teacher table right into limitations.
	 * <p>
	 * Be careful with global {@link TeacherLoadLimitation}: if you also have teacher-specific limitations,
	 * they can not extend load, but even decrease.
	 * </p>
	 *
	 * @return teacher related to this instance of limitations. <code>null</code> stands for 'all teachers'.
	 */
	@NonNull
	@Override
	public Teacher getTeacher() {
		return super.getTeacher();
	}

	@Override
	public void setTeacher(final Teacher teacher) {
		super.setTeacher(teacher);
	}

}
