package org.autoschedule.service.limitations.custom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.Subject;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.Transient;

/**
 * Limitation that enforces length of subject sequence. E.g., PE classes usually have to be two in a row.
 */
@Entity
@Inheritance
@DiscriminatorValue("SubjectSequence")
@Slf4j
public class SubjectSequenceLimitation extends AbstractLimitation {

	private static final long serialVersionUID = -2561808994576162770L;
	private static final String SUBJECT_COUNT_PROPERTY = "sequence";

	@Override
	public Subject getSubject() {
		return super.getSubject();
	}

	@Override
	public void setSubject(final Subject subject) {
		super.setSubject(subject);
	}

	/**
	 * Subject chain length. Subjects will be arranged in sequences of length <code>count</code>.
	 * @return count
	 */
    @JsonIgnore
    @Transient
	public int getCount() {
		final String value = getProperty(SUBJECT_COUNT_PROPERTY);
		if (value == null) {
			return 2;
		}
		final int count = Integer.parseInt(value);
		if (count < 2) {
			throw new IllegalStateException("Subject sequence limitations " + getId() + " has count less than 1: "
					+ count);
		}
		return count;
	}

    @JsonIgnore
    @Transient
	public void setCount(final int count) {
		setProperty(SUBJECT_COUNT_PROPERTY, String.valueOf(count));
	}

}
