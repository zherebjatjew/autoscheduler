package org.autoschedule.service.limitations;

import lombok.NonNull;
import org.autoschedule.domain.Limitation;
import org.autoschedule.service.LimitationHandler;
import org.autoschedule.service.SolverContext;
import org.chocosolver.solver.variables.IntVar;

import java.util.List;
import java.util.Map;


/**
 * Strategy of applying limitation to CP solver.
 */
public interface LimitationStrategy {
    /**
     * Goal of this method is to add to the context's {@link org.chocosolver.solver.Solver} constraints,
     * corresponding to given limitations.
     *
     * @param context solver context
     * @param limitations limitation instances grouped by handler
     * @return optional optimization variable
     */
    IntVar apply(@NonNull SolverContext context, @NonNull Map<LimitationHandler, List<Limitation>> limitations);
}
