package org.autoschedule.service.limitations;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


public final class AutomationUtils {
    private static final char SEPARATOR = '|';

    private AutomationUtils() {
    }

    /**
     * Create group from the expression given.
     *
     * @param argument expression
     * @return group
     */
    @NonNull
    public static String wrap(@NonNull final String argument) {
        if (argument.isEmpty()) {
            return "";
        }
        return '(' + argument + ')';
    }

    /**
     * Repeat sample n times.
     *
     * @param sample expected string
     * @param n how many times
     * @return regular expression
     */
    @NonNull
    public static String times(@NonNull final String sample, final int n) {
        if (n == 0) {
            return "";
        }
        if (n == 1) {
            return sample;
        }
        return wrap(sample) + '{' + n + '}';
    }

    /**
     * Add item to list.
     *
     * @param list list of items
     * @param item item ot add
     * @return concatenated list
     */
    @NonNull
    public static String or(@NonNull final String list, @NonNull final Integer item) {
        if (list.isEmpty()) {
            return intToReg(item);
        }
        return intToReg(item) + SEPARATOR + list;
    }

    /**
     * Combines items using OR.
     *
     * @param a item list
     * @return expression
     */
    @NonNull
    public static String or(@NonNull final String ... a) {
        return StringUtils.join(a, SEPARATOR);
    }

    /**
     * Converts list of integers to regular expression for
     * {@link org.chocosolver.solver.constraints.nary.automata.FA.FiniteAutomaton}.
     *
     * @param allowed list of allowed integers
     * @param exclude item to exclude form the list (<code>null</code> to get all items)
     * @return regular expression that means 'one of the list'
     */
    @NonNull
    public static String oneOf(@NonNull final Iterable<Integer> allowed, final Integer exclude) {
        // No streams because of possible NumberFormatException
        final List<String> allowedNames = new ArrayList<>();
        for (final Integer id : allowed) {
            if (exclude == null || !exclude.equals(id)) {
                allowedNames.add(intToReg(id));
            }
        }
        return StringUtils.join(allowedNames, SEPARATOR);
    }

    /**
     * Combines items using OR.
     *
     * @param set item list
     * @return expression
     */
    @NonNull
    public static String oneOf(@NonNull final Iterable<Object> set) {
        return StringUtils.join(set, SEPARATOR);
    }

    /**
     * Escapes single number for regular expression.
     *
     * <p>
     *     {@link org.chocosolver.solver.constraints.nary.automata.FA.FiniteAutomaton}
     *     class handles numbers in an especial way:
     *     http://choco-solver.org/user_guide/2_modelling.html#automaton-based-constraints
     * </p>
     * @param value number to escape
     * @return string ready to use in regexp
     */
    @NonNull
    public static String intToReg(final int value) {
        if (value < 0) {
            throw new NumberFormatException("Negative indexes are not allowed: " + value);
        }
        if (value < 10) {
            return Integer.toString(value);
        } else {
            return '<' + Integer.toString(value) + '>';
        }
    }
}
