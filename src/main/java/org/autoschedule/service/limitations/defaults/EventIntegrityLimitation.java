package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

/**
 * Filters out incomplete solutions (e.g. having teacherId but no roomId).
 * <p>The solution is simple:</p>
 * <code>
 *     Count(DAY#.HOUR#.ROOM.TEACHER=0, DAY#.HOUR#.ROOM.SUBJECT=0, DAY#.HOUR#.ROOM.TEAM=0) ∈ {0, 3}
 * </code>
 */
@Entity
@Inheritance
@DiscriminatorValue("EventIntegrity")
public class EventIntegrityLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = 2073245562437126625L;

}
