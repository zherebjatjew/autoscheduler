package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

/**
 * Limitation that disables event interferences (assigning same room/teacher/team for different classes).
 */
@Entity
@Inheritance
@DiscriminatorValue("Overlapping")
public class OverlappingLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = -4483797944957123434L;

}
