package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Minimizes number of empty hours between classes for each teacher.
 */
@Entity
@Inheritance
@DiscriminatorValue("TeacherHoleMinimizer")
public class TeacherHoleMinimizingLimitation extends AbstractLimitation implements DefaultLimitation {
    private static final long serialVersionUID = -337933585899127757L;
}
