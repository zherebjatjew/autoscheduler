package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Denies assigning one teacher to multiple rooms of the same hour.
 * <p>Solution: <code>count(DAYi.HOURj.ROOM#.TEACHER=teacherId) <= 1</code>.
 </p>
 */
@Entity
@Inheritance
@DiscriminatorValue("TeacherCollision")
public class TeacherTimeCollisionLimitation extends AbstractLimitation implements DefaultLimitation {
	private static final long serialVersionUID = -4173889022917432008L;
}
