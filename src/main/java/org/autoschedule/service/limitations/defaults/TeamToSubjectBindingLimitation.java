package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Maps team to subject.
 * <p>The limitations tells what are allowed combinations of
 * <code>{DAY#.HOUR#.ROOM#.GROUP, DAY#.HOUR#.ROOM#.SUBJECT}</code></p>
 */

@Entity
@Inheritance
@DiscriminatorValue("TeamToSubjectBinding")
public class TeamToSubjectBindingLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = -5386197312626407568L;

}
