package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * One student can belong to multiple groups. These groups can not be assigned to the same hour,
 * because the student would not be able to sit in two rooms at the same time.
 * <p/>
 * <a href="http://choco-solver.org/?q=node/20">solution explanation</a>
 */
@Entity
@Inheritance
@DiscriminatorValue("CompetitiveTeam")
public class CompetitiveTeamLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = 1763363119747484508L;

}
