package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Controls whether team fits room.
 */
@Entity
@Inheritance
@DiscriminatorValue("RoomCapacity")
public class RoomCapacityLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = -3801988357143859425L;

}
