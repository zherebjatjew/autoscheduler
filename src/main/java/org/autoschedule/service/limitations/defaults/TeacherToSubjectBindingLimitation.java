package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Sets teacher-to-subject relations in model.

 * <code>
 *     (DAYi.HOURj.ROOMk.TEACHER, DAYi.HOURj.ROOMk.SUBJECT) ∈ {(teacher1, subject1), (teacher2, subject3), ...}
 * </code>
 */
@Entity
@Inheritance
@DiscriminatorValue("TeacherToSubjectBinding")
public class TeacherToSubjectBindingLimitation extends AbstractLimitation implements DefaultLimitation {

	private static final long serialVersionUID = -2738896692513459209L;

}
