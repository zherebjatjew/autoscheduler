package org.autoschedule.service.limitations.defaults;


import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

/**
 * Controls subject positions in timetable according {@link org.autoschedule.domain.SubjectType#getWeight()}.
 */
@Entity
@Inheritance
@DiscriminatorValue("SubjectWeight")
public class SubjectWeightLimitation extends AbstractLimitation implements DefaultLimitation {
    private static final long serialVersionUID = -6926794821737732468L;
}
