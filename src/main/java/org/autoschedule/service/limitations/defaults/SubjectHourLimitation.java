package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Sets which subjects may belong to which team and how many times it appears in solution.
 * <p>
 *     The limitations does not limit which subjects can be assigned to team. It is controlled by
 *     {@link TeamToSubjectBindingLimitation}.
 * </p>

 * <code>
 *     (DAYi.HOURj.ROOMk.GROUP, DAYi.HOURj.ROOMk.SUBJECT) ∈ {(group1, subject1), (group2, subject3), ...}
 * </code>
 */
@Entity
@Inheritance
@DiscriminatorValue("SubjectHour")
public class SubjectHourLimitation extends AbstractLimitation implements DefaultLimitation {
	private static final long serialVersionUID = 393276605592023910L;
}
