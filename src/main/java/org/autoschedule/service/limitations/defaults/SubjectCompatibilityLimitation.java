package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Defines subject type pair, where the types are not allowed to be in the same day.
 * <p>E.g., do not put gym and physics lab in one day.</p>
 * <p>To make it working, you should assign desired types to {@link org.autoschedule.domain.Subject}.</p>
 */
@Entity
@Inheritance
@DiscriminatorValue("SubjectCompatibility")
public class SubjectCompatibilityLimitation extends AbstractLimitation implements DefaultLimitation {
    private static final long serialVersionUID = -1209076878679295536L;
    private static final String TYPE_A = "type.a";
    private static final String TYPE_B = "type.b";

    public String getTypeA() {
        return getProperty(TYPE_A);
    }

    public void setTypeA(final String type) {
        setProperty(TYPE_A, type);
    }

    public String getTypeB() {
        return getProperty(TYPE_B);
    }

    public void setTypeB(final String type) {
        setProperty(TYPE_B, type);
    }
}
