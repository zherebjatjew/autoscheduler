package org.autoschedule.service.limitations.defaults;

import org.autoschedule.domain.DefaultLimitation;
import org.autoschedule.service.limitations.AbstractLimitation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;


/**
 * Minimizes number of empty hours between classes for each team.
 */
@Entity
@Inheritance
@DiscriminatorValue("TeamHoleMinimizer")
public class TeamHoleMinimizingLimitation extends AbstractLimitation implements DefaultLimitation {
    private static final long serialVersionUID = -1369909297909786519L;
}
