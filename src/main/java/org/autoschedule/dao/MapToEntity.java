package org.autoschedule.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Makes custom queries return typed objects.
 * <p>
 *     Synthetic queries like <code>select a.name, sum(a.price) from Aclass as a team by a.name</code>
 *     always return <code>Object[]</code>. The example above will produce <code>Iterable&lt;Object[]&gt;</code>
 *     where <code>Object[0]</code> stores name field, <code>Object[1]</code> stores count.
 * </p>
 * <p>
 *     This annotation allows to map the <code>Object[]</code> to non-persistent instance of required class:
 * </p>
 * <code>
 *     \@MapToEntity(targetClass=Aclass.class, value = {"name", "price"})
 *     \@Query("select a.name, sum(a.price) from Aclass as a team by a.name")
 *     Iterable&lt;Aclass&gt; getSubtotals();
 * </code>
 *
 * <p>
 *     Method result type can be <code>Object[]</code>, <code>Iterable</code> (each item is mapped) one of
 *     <code>targetClass</code> (returned as it is). Other types will cause <code>ClassCastException</code>
 * </p>
 *
 * <p>
 *     Type check is performed on assignment.
 *     {@link org.springframework.beans.TypeMismatchException} is thrown if value type is not compatible to field type.
 *     The only exception is values of type {@code Long}, which can be converted to {@code Integer} if
 *     there is no overflow.
 * </p>
 *
 * {@see http://blog.javaforge.net/post/76125490725/spring-aop-method-interceptor-annotation}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MapToEntity {
	String[] value();
	Class targetClass();
}
