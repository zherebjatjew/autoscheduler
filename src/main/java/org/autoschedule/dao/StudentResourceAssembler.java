package org.autoschedule.dao;

import org.autoschedule.domain.Student;
import org.autoschedule.domain.StudentRepository;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class StudentResourceAssembler extends ResourceAssemblerSupport<Student, StudentResource> {

    public StudentResourceAssembler() {
        super(StudentRepository.class, StudentResource.class);
    }

    @Override
    public StudentResource toResource(final Student student) {
        StudentResource resource = instantiateResource(student);
        resource.student = student;
        resource.add(ControllerLinkBuilder.linkTo(StudentRepository.class).slash(student.getId()).withRel("name"));
        return null;
    }
}
