package org.autoschedule.dao;

import lombok.NonNull;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.ConversionException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Map <code>Object[]</code> to given class.
 * @see MapToEntity
 */
@Component
public class MapToEntityInterceptor implements MethodInterceptor {

	@Override
	public Object invoke(final MethodInvocation methodInvocation) throws Throwable {
		final MapToEntity annotation = methodInvocation.getMethod().getAnnotation(MapToEntity.class);
		final Object result = methodInvocation.proceed();
		if (result != null && Iterable.class.isAssignableFrom(result.getClass())) {
			final Iterable collection = (Iterable) result;
			final ArrayList<Object> converted = new ArrayList<>();
			for (final Object item : collection) {
				converted.add(mapObjectToEntity(item, annotation));
			}
			return converted;
		} else {
			return mapObjectToEntity(methodInvocation.proceed(), annotation);
		}
	}

	protected Object mapObjectToEntity(final Object object, @NonNull final MapToEntity annotation)
			throws Throwable
	{
		if (object == null) {
			return null;
		}
		if (annotation.targetClass().isAssignableFrom(object.getClass())) {
			return object;
		}
		final Object[] fields = (Object[]) object;
		if (fields.length < annotation.value().length) {
			throw new ConversionException("Too many properties in @MapToEntity("
					+ annotation.targetClass().getName() + ')');
		}
		final Object result = ConstructorUtils.invokeConstructor(annotation.targetClass(), new Object[]{});
		for (int i = 0; i < annotation.value().length; i++) {
			final String name = annotation.value()[i];
			Object value = fields[i];
            final PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(result.getClass(), name);
            final Method writeMethod = descriptor.getWriteMethod();
            if (writeMethod == null) {
                throw new RuntimeException("Property " + name + " is read only");
            }
            if (value != null) {
                final Class<?> requiredType = writeMethod.getParameterTypes()[0];
                if (!value.getClass().isAssignableFrom(requiredType)) {
                    if (value instanceof Long && Integer.class.isAssignableFrom(requiredType)) {
                        if ((Long) value > Integer.MAX_VALUE) {
                            throw new RuntimeException("Integer overflow during conversion " + value + 'L');
                        }
                        value = ((Long) value).intValue();
                    } else {
                        throw new TypeMismatchException(value, requiredType);
                    }
				}
			}
            writeMethod.invoke(result, value);
		}
		return result;
	}

}
