package org.autoschedule.dao;

import org.autoschedule.api.Index;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Convert {@link Index} to DB-compatible representation.
 */
@Converter
public class IndexConverter implements AttributeConverter<Index, Integer> {
	@Override
	public Integer convertToDatabaseColumn(final Index index) {
		return index == null ? null : index.getRaw();
	}

	@Override
	public Index convertToEntityAttribute(final Integer raw) {
		return raw == null ? new Index() : new Index(raw);
	}
}
