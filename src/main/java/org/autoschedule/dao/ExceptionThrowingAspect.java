package org.autoschedule.dao;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.autoschedule.api.ObjectNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 * Spring data repositories return <code>null</code> when non-existing id is queried.
 * I'd like to have an exception instead.
 *
 * <p>Another point is not throwing {@link EmptyResultDataAccessException} during deleting non-existing entities.</p>
 *
 * {@see https://jira.spring.io/browse/DATAJPA-118?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel}
 */
@Aspect
@Component
public class ExceptionThrowingAspect {
	@Pointcut("execution(public * org.springframework.data.repository.Repository+.findOne(..))")
	public void findByIdRepositoryMethod() {
	}

	@Pointcut("execution(public void org.springframework.data.repository.Repository+.delete(!Iterable))")
	public void deleteRepositoryMethod() {
	}

	@Around("findByIdRepositoryMethod()")
	public Object throwIfIdNotFound(final ProceedingJoinPoint pjp) throws Throwable {
		Object result = pjp.proceed();
		if (result == null) {
			throw new ObjectNotFoundException("Entity with id=" + pjp.getArgs()[0] + " does not exists.");
		}
		return result;
	}

	@Around("deleteRepositoryMethod()")
	public Object catchDeleteUnexisting(final ProceedingJoinPoint pjp) throws Throwable {
		try {
			return pjp.proceed();
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
}
