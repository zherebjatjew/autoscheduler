package org.autoschedule.dao;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.data.repository.Repository;

import java.lang.reflect.Method;

/**
 * Advise {@link Repository}.
 *
 * <p>
 *     At first I tried to implement this feature via {@link org.aspectj.lang.annotation.Aspect}, but it didn't work,
 *     because target class is already proxied, and original annotation is not available.
 * </p>
 */
@Component
public class MapToEntityAdvisor extends AbstractPointcutAdvisor {
	private static final long serialVersionUID = -5535205721634773293L;

	private final StaticMethodMatcherPointcut pointcut = new StaticMethodMatcherPointcut() {
		@Override
		public boolean matches(final Method method, final Class<?> aClass) {
			return Repository.class.isAssignableFrom(aClass)
					&& method.isAnnotationPresent(MapToEntity.class);
		}
	};

	@Autowired
	private MapToEntityInterceptor interceptor;

	@Override
	public Pointcut getPointcut() {
		return pointcut;
	}

	@Override
	public Advice getAdvice() {
		return interceptor;
	}
}
