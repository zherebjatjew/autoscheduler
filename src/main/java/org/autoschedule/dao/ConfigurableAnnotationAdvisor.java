package org.autoschedule.dao;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.repository.Repository;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

/**
 * Support returning {@code @Configurable} objects from repository methods.
 */
@Component
public class ConfigurableAnnotationAdvisor extends AbstractPointcutAdvisor implements ApplicationContextAware {

    private static final long serialVersionUID = 90095664850518553L;

    private final StaticMethodMatcherPointcut pointcut = new StaticMethodMatcherPointcut() {
        @Override
        public boolean matches(final Method method, final Class<?> aClass) {
            return Repository.class.isAssignableFrom(aClass);
        }
    };

    private final MethodInterceptor interceptor = new MethodInterceptor() {
        @Override
        public Object invoke(final MethodInvocation methodInvocation) throws Throwable {
            final Object result = methodInvocation.proceed();
            initialize(result, methodInvocation.getMethod().getReturnType());
            return result;
        }

        private Object initialize(final Object object, final Class<?> returnType) {
            if (object != null) {
                if (Iterable.class.isAssignableFrom(returnType)) {
                    final Iterable collection = (Iterable) object;
                    final List<Object> converted = new ArrayList<>();
                    boolean changed = false;
                    for (final Object item : collection) {
                        final Object initialized = initialize(item, item.getClass());
                        changed = changed || initialized != item;
                        converted.add(initialized);
                    }
                    if (changed) {
                        return convertListTo(converted, object);
                    }
                } else {
                    Class temp = object.getClass();
                    do {
                        if (AnnotationUtils.getAnnotation(temp, Configurable.class) != null) {
                            AutowireCapableBeanFactory autowireCapableBeanFactory = appContext.getAutowireCapableBeanFactory();
                            autowireCapableBeanFactory.autowireBean(object);
                            return autowireCapableBeanFactory.initializeBean(object, object.getClass().getSimpleName().toLowerCase());
                        } else {
                            temp = temp.getSuperclass();
                        }
                    } while (temp != Object.class);
                }
            }
            return object;
        }

        private Object convertListTo(final List<Object> converted, final Object original) {
            if (original.getClass().isAssignableFrom(Page.class)) {
                final Page originalPage = (Page) original;
                return new PageImpl<>(converted, originalPage.nextPageable(), originalPage.getTotalElements());
            } else if (original.getClass().isAssignableFrom(HashSet.class)) {
                return new HashSet<>(converted);
            } else if (original.getClass().isAssignableFrom(Stream.class)) {
                return converted.stream();
            } else if (original.getClass().isAssignableFrom(List.class)) {
                return converted;
            }
            throw new UnsupportedClassVersionError("Class " + original.getClass().getName()
                    + " is not supported by ConfigurableAnnotationAdvisor");
        }
    };

    private ApplicationContext appContext;

    @Override
    public Pointcut getPointcut() {
        return pointcut;
    }

    @Override
    public Advice getAdvice() {
        return interceptor;
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }
}
