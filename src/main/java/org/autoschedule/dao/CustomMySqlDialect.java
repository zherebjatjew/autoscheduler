package org.autoschedule.dao;

import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.springframework.stereotype.Component;

/**
 * Extends MySQL5InnoDBDialect and sets the default charset to be UTF-8.
 * @author Sorin Postelnicu * @since Aug 13, 2007
 */
@Component
public class CustomMySqlDialect extends MySQL5InnoDBDialect {
	@Override
	public String getTableTypeString() {
		return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
	}

	@Override
	public String getDropSequenceString(final String sequenceName) {
		// Adding the "if exists" clause to avoid warnings
		return "DROP SEQUENCE IF EXISTS " + sequenceName;
	}

	@Override
	public boolean dropConstraints() {
		// We don't need to drop limitations before dropping tables, that just leads to error
		// messages about missing tables when we don't have a schema in the database
		return false;
	}
}
