package org.autoschedule;

import org.autoschedule.api.ObjectNotFoundException;
import org.autoschedule.domain.Account;
import org.autoschedule.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public void init(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    UserDetailsService userDetailsService() {
        return username -> {
            try {
                final Account account = accountRepository.findOne(username);
                return new User(account.getLogin(), account.getPassword(), true, true, true, true,
                        AuthorityUtils.createAuthorityList("ROLE_" + account.getRole().name()));
            } catch (ObjectNotFoundException e) {
                throw new UsernameNotFoundException("Can not find the user '" + username + "'", e);
            }
        };
    }
}
