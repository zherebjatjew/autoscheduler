package org.autoschedule.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;


/**
 * A {@link Collector}, that uses custom consumer method, so you can, e.g. apply selective reduction.
 * @param <T> type of items being collected
 */
public class CustomizableCollector<T extends Comparable> implements Collector<T, List<T>, List<T>> {
    private final Supplier<List<T>> supplier = ArrayList<T>::new;
    private BiConsumer<List<T>, T> accumulator;
    private final BinaryOperator<List<T>> combiner = (left, right) -> { left.addAll(right); return left; };
    private final Function<List<T>, List<T>> finisher = i -> i;
    private final Set<Characteristics> characteristics
            = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.IDENTITY_FINISH));

    public CustomizableCollector(final BiConsumer<List<T>, T> consumer) {
        accumulator = consumer;
    }

    @Override
    public Supplier<List<T>> supplier() {
        return supplier;
    }

    @Override
    public BiConsumer<List<T>, T> accumulator() {
        return accumulator;
    }

    @Override
    public BinaryOperator<List<T>> combiner() {
        return combiner;
    }

    @Override
    public Function<List<T>, List<T>> finisher() {
        return finisher;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return characteristics;
    }
}
