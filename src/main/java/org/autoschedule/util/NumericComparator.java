package org.autoschedule.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Compares string respecting numbers.
 * E.g. "10A" > "1A"
 * http://phi.lho.free.fr/serendipity/index.php?/archives/30-Sorting-strings-by-respecting-natural-order-for-numbers.html
 */
public class NumericComparator {
    protected NumericComparator() {
    }
    public static int compare(final String s1, final String s2) {
        if (StringUtils.isEmpty(s1) && StringUtils.isEmpty(s2)) {
            return 0; // Identical...
        }
        if (StringUtils.isEmpty(s1)) {
            return -1; // Empty is smaller than anything else
        }
        if (StringUtils.isEmpty(s2)) {
            return 1;
        }
        char c1 = s1.charAt(0);
        char c2 = s2.charAt(0);
        boolean b1 = Character.isDigit(c1);
        boolean b2 = Character.isDigit(c2);
        // Fast exit
        if (b1 && !b2 || !b1 && b2) {
            return s1.compareTo(s2); // One is a digit, the other isn't: do regular comparison
        }

        int len1 = s1.length();
        int len2 = s2.length();

        int pos1 = 0, pos2 = 0;
        boolean bIsDigit = b1;
        while (true) {
            if (bIsDigit) {
                // Do number comparison
                long n1 = 0;
                long n2 = 0;
                // Convert the sequence of digits to a long number
                do {
                    c1 = s1.charAt(pos1);
                    b1 = Character.isDigit(c1);
                    if (b1) {
                        n1 = n1 * 10 + (c1 - '0');
                    }
                    // Stop if one non-digit is found or if we reached the end of one string
                } while (b1 && ++pos1 < len1);
                // Idem, in the second string
                do {
                    c2 = s2.charAt(pos2);
                    b2 = Character.isDigit(c2);
                    if (b2) {
                        n2 = n2 * 10 + (c2 - '0');
                    }
                    // Stop if one non-digit is found or if we reached the end of one string
                } while (b2 && ++pos2 < len2);

                // Compare the numbers
                if (n1 < n2) {
                    return -1;
                } else if (n1 > n2) {
                    return 1;
                }
                // Here, the numbers are equal. If we reached the end of the strings,
                // we say they are equal, otherwise we continue on comparing strings
                if (pos1 == len1 && pos2 == len2) {
                    return 0;
                }
            } else {
                // Do string comparison, character by character
                do {
                    c1 = s1.charAt(pos1);
                    c2 = s2.charAt(pos2);
                    b1 = !Character.isDigit(c1);
                    b2 = !Character.isDigit(c2);

                    // Two non-digits, different
                    if (b1 && b2 && c1 != c2) {
                        return sign(c1 - c2);
                    }
                    // One is digit, and the other isn't one
                    if (b1 != b2) {
                        return sign(c1 - c2); // Just compare these different chars
                    }

                    // Next chars
                    ++pos1;
                    ++pos2;
                    // Stop if one digit is found or if we reached the end of one string
                } while (b1 && b2 && pos1 < len1 && pos2 < len2);

                if (b1 && pos1 == len1 && pos2 == len2) {
                    return 0; // At the end with non-digits without finding differences
                }
            }
            // Have we reached one end?
            if (pos1 == len1 && len1 < len2) {
                return -1; // s1 is shorter, so smaller (all chars were equal so far)
            }
            if (pos2 == len2 && len2 < len1) {
                return 1; // s2 is shorter, so smaller
            }

            // Not at the end, we stopped on different kind of char (digit vs. non-digits), let's process them
            if (!bIsDigit) { // Compared chars, we went one char too far, into digits
                // Put back current chars into the comparison
                --pos1;
                --pos2;
            }
            // Switch the comparion mode
            bIsDigit = !bIsDigit;
        }
    }

    private static int sign(final int i) {
        return i < 0 ? -1 : (i > 0 ? 1 : 0);
    }
}
