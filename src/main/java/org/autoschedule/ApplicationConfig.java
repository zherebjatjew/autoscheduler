package org.autoschedule;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@EnableConfigurationProperties
@Component
@ConfigurationProperties("org.autoschedule")
public class ApplicationConfig {
    private int period;
    private int dayLength;
    private int hourLength;
}
