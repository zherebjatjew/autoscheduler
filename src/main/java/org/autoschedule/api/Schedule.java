package org.autoschedule.api;

import org.autoschedule.domain.Event;

/**
 * Fragment of schedule.
 */
public interface Schedule extends Iterable<Event> {
	/**
	 * Returns sub-interval.
	 *
	 * @param from start index
	 * @param to end index
	 * @return sub-interval
	 */
	Schedule getSchedule(final Index from, final Index to);

	/**
	 * Splits current schedule into list of sub-schedules, each containing
	 * single hour.
	 *
	 * @return Sorted list of one-hour sub-schedules
	 */
	Iterable<Schedule> byHours();

	Iterable<Schedule> byDays();

	Event[][] asTable(int days, int hours);

	/**
	 * @return true if contains no events.
	 */
	boolean isEmpty();
}
