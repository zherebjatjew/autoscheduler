package org.autoschedule.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Thrown by DB getters if entity was not found.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Object not found")
public class ObjectNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -3961145086156073848L;

	public ObjectNotFoundException() {
		super();
	}

	public ObjectNotFoundException(final String message) {
		super(message);
	}

	public ObjectNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
