package org.autoschedule.api;

import java.util.Calendar;


/**
 * Constructs {@link Schedule} instances.
 *
 * There is an ability to override particular events in a schedule on non-regular basis,
 * so methods of the interface require {@link Calendar} rather than {@link org.autoschedule.api.Index}.
 */
public interface ScheduleFactory {
	/**
	 * Construct schedule for teachers.
	 *
	 * @param teacherId id of {@link org.autoschedule.domain.Teacher} to get schedule for
	 * @param from start time
	 * @param to end time
	 * @return schedule containing ordered events related to the given teacher
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 * @throws ObjectNotFoundException if there is no teacher having given <code>teacherId</code>
	 */
	Schedule forTeacher(final String teacherId, final Calendar from, final Calendar to);

	/**
	 * Construct schedule for student. Student can belong to multiple teams.
	 *
	 * @param studentId id of {@link org.autoschedule.domain.Student}
	 * @param from start time
	 * @param to end time
	 * @return schedule containing ordered events related to the given learning team
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 * @throws ObjectNotFoundException if there is no team having given <code>studentId</code>
	 */
	Schedule forStudent(final long studentId, final Calendar from, final Calendar to);

	/**
	 * Construct schedule for team.
	 *
	 * @param teamId id of {@link org.autoschedule.domain.Team}
	 * @param from start time
	 * @param to end time
	 * @return schedule containing ordered events related to the given learning team
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 * @throws ObjectNotFoundException if there is no team having given <code>teamId</code>
	 */
	Schedule forTeam(final long teamId, final Calendar from, final Calendar to);

	/**
	 * Construct schedule for room.
	 *
	 * @param roomId id of {@link org.autoschedule.domain.Room}
	 * @param from start time
	 * @param to end time
	 * @return schedule containing ordered events related to the given room
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 * @throws ObjectNotFoundException if there is no room having given <code>roomId</code>
	 */
	Schedule forRoom(final long roomId, final Calendar from, final Calendar to);

	/**
	 * Construct schedule for event administrator.
	 *
	 * @param from start time
	 * @param to end time
	 * @return schedule containing all events that take place within given time interval.
	 * Unlike the other methods of {@link ScheduleFactory}, the result here can contain overlapped events.
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 */
	Schedule forAdmin(final Calendar from, final Calendar to);

	/**
	 * Construct schedule for given object.
	 *
	 * @param object event target. Can be {@link org.autoschedule.domain.Student},
     * {@link org.autoschedule.domain.Team}, {@link org.autoschedule.domain.Room},
     * {@link org.autoschedule.domain.Teacher}
	 * oneOf <code>null</code> for {@link ScheduleFactory#forAdmin(java.util.Calendar, java.util.Calendar)}
	 * @param from start time
	 * @param to end time
	 * @return schedule
	 * @throws IllegalArgumentException if <code>from</code> is greater than <code>to</code>
	 * @throws ObjectNotFoundException if there is no room having given <code>roomId</code>
	 * @throws ClassCastException if <code>object</code>'s class is not supported
	 */
	Schedule auto(final Object object, final Calendar from, final Calendar to);
}
