package org.autoschedule.api;

import lombok.NonNull;

import java.util.Calendar;


/**
 * Represents day/hour coordinates for {@link org.autoschedule.domain.Event}.
 */
public class Index implements Comparable<Index> {
	private int raw;

	/**
	 * Short constructor.
	 */
	public Index() {
		raw = 0;
	}

	/**
	 * Constructor that takes raw position.
	 *
	 * @param raw day/hour
	 */
	public Index(final int raw) {
		this.raw = raw;
	}

	/**
	 * Explicit constructor that takes day and hour.
	 *
	 * @param day day number, starting from 0
	 * @param hour hour number, starting from 0
	 */
	public Index(final int day, final int hour) {
		int week = day/7;
		raw = week*700 + (day%7)*100 + hour%100;
	}

	/**
	 * @return raw position
	 */
	public int getRaw()	{
		return raw;
	}

	public void setRaw(final int raw) {
		this.raw = raw;
	}

	/**
	 * @return hour index
	 */
	public int getHour() {
		return raw%100;
	}

	/**
	 * @return day index
	 */
	public int getDay() {
		return (raw/100)%7;
	}

	/**
	 * @return week index
	 */
	public int getWeek() {
		return raw/700;
	}

	/**
	 * Instance relation in time.
	 *
	 * @param index other instance
	 * @return true if this instance goes later than <code>index</code>
	 */
	public boolean isAfter(@NonNull final Index index) {
		return index.getRaw() < raw;
	}

	/**
	 * Instance relation in time.
	 *
	 * @param index other instance
	 * @return true if this instance goes earlier than <code>index</code>
	 */
	public boolean isBefore(@NonNull final Index index) {
		return raw < index.getRaw();
	}

	/**
	 * Check whether the instance belong to given interval.
	 *
	 * @param lower lower (old) bound
	 * @param upper upper(new) bound
	 * @return true if the instance is fully within the interval
	 */
	public boolean isWithin(final Index lower, final Index upper) {
		return (lower == null || !isBefore(lower)) && (upper == null || !isAfter(upper));
	}

	/**
	 * Calculates absolute time of the instance.
	 *
	 * @param origin start of planning period
	 * @param durationMs length of abstract hour, ms. Thus, academic hour is 45 min, in university can be
	 *                   two academic hours: 90 min. May include break between hours.
	 * @return start time of the instance
	 */
	public Calendar getStartTime(final Calendar origin, final int durationMs) {
		final Calendar calendar = (Calendar) origin.clone();
		calendar.add(Calendar.DAY_OF_MONTH, 7*getWeek());
		calendar.add(Calendar.DAY_OF_MONTH, getDay());
		calendar.add(Calendar.MILLISECOND, durationMs*getHour());
		return calendar;
	}

	public boolean equals(final Object op) {
		if (op == null) {
			return false;
		}
		if (op == this) {
			return true;
		}
		if (op instanceof Integer) {
			return ((Integer) op) == getRaw();
		} else if (op instanceof Index) {
			return ((Index) op).getRaw() == getRaw();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return raw;
	}

	@Override
	public String toString() {
		return "Index{"
				+ "day=" + getDay()
				+ ", hour=" + getHour()
				+ '}';
	}

	@Override
	public int compareTo(@NonNull final Index o) {
		return Integer.compare(raw, o.raw);
	}
}
