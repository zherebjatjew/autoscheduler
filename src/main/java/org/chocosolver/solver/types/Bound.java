package org.chocosolver.solver.types;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chocosolver.solver.variables.IntVar;

import java.util.Collection;

@ToString
@Getter
@Setter
public class Bound {
    private int min;
    private int max;

    public Bound(final int min, final int max) {
        this.min = min;
        this.max = max;
    }

    public static Bound merge(final Collection<Bound> groups) {
        return groups.stream().reduce(Bound::union).get();
    }

    public boolean contains(final IntVar var) {
        return var.getLB() <= max && var.getUB() >= min;
    }

    public boolean isClosed() {
        return min == max;
    }

    public Bound union(final Bound b) {
        return new Bound(min + b.min, max + b.max);
    }

    public boolean isValid() {
        return min <= max;
    }
}
