package org.chocosolver.solver.constraints.map;


import gnu.trove.map.hash.THashMap;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.types.Bound;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * The propagator maps one set of variables to another, connecting them via intermediate variable.
 * <p>It ensures that</p>
 * <code>
 *     s = count(y[i] = vy && x[i] = vx);
 * </code>
 */
public class CountingPropagator extends Propagator<IntVar> {

    private static final long serialVersionUID = 4146775377113495351L;

    private IntVar[] x;
    private IntVar[] y;
    private IntVar s;
    private int vx;
    private int vy;

    public static CountingPropagator create(final IntVar[] x, final IntVar[] y, final int vx, final IntVar s,
                                            final int vy)
    {
        assert x != null;
        assert y != null;
        assert x.length > 0;
        assert y.length > 0;
        assert s != null;

        final IntVar[] vv = new IntVar[x.length + y.length + 1];
        vv[0] = s;
        System.arraycopy(x, 0, vv, 1, x.length);
        System.arraycopy(y, 0, vv, x.length + 1, y.length);
        final CountingPropagator result = new CountingPropagator(vv);
        result.x = x;
        result.y = y;
        result.vx = vx;
        result.vy = vy;
        result.s = s;
        return result;
    }

    @Override
    public void propagate(final int evtmask) throws ContradictionException {
        final Bound bound = getBound();
        s.updateLowerBound(bound.getMin(), aCause);
        s.updateUpperBound(bound.getMax(), aCause);
    }

    @Override
    public ESat isEntailed() {
        final Bound bound = getBound();
        if (!bound.contains(s)) {
            return ESat.FALSE;
        }
        if (bound.isClosed() && s.isInstantiated()) {
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    @Override
    public void duplicate(final Solver solver, final THashMap<Object, Object> identitymap) {
        if (!identitymap.containsKey(this)) {
            final IntVar[] cx = copyArray(x, solver, identitymap);
            final IntVar[] cy = copyArray(y, solver, identitymap);
            s.duplicate(solver, identitymap);
            final IntVar cs = (IntVar) identitymap.get(s);
            final CountingPropagator cthis = create(cx, cy, vx, cs, vy);
            identitymap.put(this, cthis);
        }
    }

    private IntVar[] copyArray(final IntVar[] array, final Solver solver, final THashMap<Object, Object> identitymap) {
        final IntVar[] cx = new IntVar[array.length];
        for (int i = 0; i < array.length; i++) {
            array[i].duplicate(solver, identitymap);
            cx[i] = (IntVar) identitymap.get(array[i]);
        }
        return cx;
    }

    @Override
    public String toString() {
        return s.getName() + " = Count({x, y} = (" + IntStream.range(0, x.length).boxed()
                .map(i -> "(" + x[i].getName() + ", " + y[i].getName() + ')')
                .collect(Collectors.joining(", ")) + ") where {x=" + vx + ", y=" + vy + "})";
    }

    protected CountingPropagator(final IntVar[] vv) {
        super(vv);
    }

    protected Bound getBound() {
        int min = 0;
        int max = x.length;
        for (int i = 0; i < x.length; i++) {
            if (x[i].contains(vx)) {
                final boolean definite = x[i].isInstantiated();
                if (y[i].contains(vy)) {
                    if (definite && y[i].isInstantiated()) {
                        min++;
                    }
                } else if (definite) {
                    max--;
                }
            } else {
                max--;
            }
        }
        return new Bound(min, max);
    }
}
