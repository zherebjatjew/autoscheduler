package org.chocosolver.solver.constraints;

import gnu.trove.map.hash.THashMap;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.types.Bound;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.iterators.DisposableValueIterator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Calculates 0s in chain of variables, except trailing 0s.
 */
public class TwoDimensionalWindowCountPropagator extends Propagator<IntVar> {
    private static final long serialVersionUID = 8561507588825122256L;
    private IntVar counter;
    private int key;
    private IntVar[][] table;

    protected static PropagatorPriority computePriority(final int nbvars) {
        if (nbvars == 1) {
            return PropagatorPriority.UNARY;
        } else if (nbvars == 2) {
            return PropagatorPriority.BINARY;
        } else if (nbvars == 3) {
            return PropagatorPriority.TERNARY;
        } else {
            return PropagatorPriority.LINEAR;
        }
    }

    public static TwoDimensionalWindowCountPropagator create(final IntVar[][] table, final IntVar counter, final int key) {
        int total = 1;
        for (final IntVar[] row : table) {
            total += row.length;
        }
        final IntVar[] vv = new IntVar[total];
        vv[0] = counter;
        int pos = 1;
        for (final IntVar[] row : table) {
            System.arraycopy(row, 0, vv, pos, row.length);
            pos += row.length;
        }
        final TwoDimensionalWindowCountPropagator result = new TwoDimensionalWindowCountPropagator(vv, table.length);
        result.counter = counter;
        result.key = key;
        result.table = table;
        return result;
    }

    @Override
    public void propagate(final int evtmask) throws ContradictionException {
        // Simplified implementation: define count by list but not the opposite.
        boolean again;
        do {
            final Bound bound = getBound();
            again = counter.updateLowerBound(bound.getMin(), aCause);
            again |= counter.updateUpperBound(bound.getMax(), aCause);
        } while (again);
    }

    @Override
    public int getPropagationConditions(final int vIdx) {
        return IntEventType.boundAndInst();
    }

    @Override
    public ESat isEntailed() {
        final Bound bound = getBound();
        if (!bound.contains(counter)) {
            return ESat.FALSE;
        }
        if (bound.isClosed() && counter.isInstantiated()) {
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    @Override
    public String toString() {
        return counter.getName() + " + HoleCount({"
                + Arrays.stream(table).map(row -> "{" + Arrays.stream(row)
                .map(Variable::getName)
                .collect(Collectors.joining(", ")) + "}")
                .collect(Collectors.joining(", ")) + "})";
    }

    @Override
    public void duplicate(final Solver solver, final THashMap<Object, Object> identitymap) {
        if (!identitymap.containsKey(this)) {
            final IntVar[][] tableCopy = new IntVar[table.length][];
            for (int i = 0; i < table.length; i++) {
                tableCopy[i] = new IntVar[table[i].length];
                for (int j = 0; j < table[i].length; j++) {
                    table[i][j].duplicate(solver, identitymap);
                    tableCopy[i][j] = (IntVar) identitymap.get(table[i][j]);
                }
            }
            counter.duplicate(solver, identitymap);
            final TwoDimensionalWindowCountPropagator copy = create(tableCopy, (IntVar) identitymap.get(counter), key);
            identitymap.put(this, copy);
        }
    }

    protected TwoDimensionalWindowCountPropagator(final IntVar[] vv, final int length) {
        super(vv, computePriority(length), false);
    }

    // O(n^4) facepalm.jpg
    protected Bound getBound() {
        Map<Integer, Bound> groups = getAllGroups();
        if (groups.isEmpty()) {
            return new Bound(0, 0);
        }
        groups.forEach((k, v) -> {
            boolean optionalAppeared = false;
            boolean obligateAppeared = false;
            for (int i = table.length - 1; i >= 0; i--) {
                boolean hasK = false;
                boolean hasKonly = false;
                for (int j = 0; j < table[i].length; j++) {
                    hasK |= table[i][j].contains(k);
                    hasKonly |= table[i][j].isInstantiatedTo(k);
                }
                if (hasK) {
                    if (optionalAppeared) {
                        if (hasKonly) {
                            v.setMax(v.getMax() - 1);
                        }
                    } else {
                        optionalAppeared = true;
                        v.setMin(0);
                        v.setMax(i);
                    }
                    obligateAppeared |= hasKonly;
                } else if (obligateAppeared) {
                    v.setMin(v.getMin() + 1);
                }
            }
            assert v.isValid();
        });
        return Bound.merge(groups.values());
    }

    private Map<Integer, Bound> getAllGroups() {
        final Map<Integer, Bound> result = new HashMap<>();
        for (IntVar[] row : table) {
            for (IntVar cell : row) {
                DisposableValueIterator iter = cell.getValueIterator(true);
                while (iter.hasNext()) {
                    final Integer v = iter.next();
                    if (!v.equals(key) && !result.containsKey(v)) {
                        result.put(v, new Bound(0, 0));
                    }
                }
                iter.dispose();
            }
        }
        return result;
    }
}
