package org.chocosolver.solver.constraints.arithm;

import gnu.trove.map.hash.THashMap;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;


public class ComparisonPropagator extends Propagator<IntVar> {
    private static final long serialVersionUID = 3963714340761610774L;

    private final int value;
    private final Comparator comparator;

    public ComparisonPropagator(final IntVar result, final IntVar operand, final String operator, final int value) {
        super(new IntVar[] {result, operand}, PropagatorPriority.BINARY, false);
        this.value = value;
        assert !operator.isEmpty();
        switch (operator) {
            case ">":
                comparator = new GtComparator();
                break;
            default:
                throw new IllegalArgumentException("Operator " + operator + " is not supported");
        }
    }

    @Override
    public void propagate(final int evtmask) throws ContradictionException {
        comparator.apply(vars[1], value, vars[0]);
    }

    @Override
    public ESat isEntailed() {
        if (!vars[0].isInstantiated()) {
            return ESat.UNDEFINED;
        } else {
            return comparator.satisfies(vars[0], vars[1], value) ? ESat.TRUE : ESat.FALSE;
        }
    }

    @Override
    public String toString() {
        return vars[0].getName() + " = " + vars[1].getName() + ' ' + comparator.toString() + ' ' + value;
    }

    @Override
    public void duplicate(final Solver solver, final THashMap<Object, Object> identitymap) {
        if (!identitymap.containsKey(this)) {
            vars[0].duplicate(solver, identitymap);
            vars[1].duplicate(solver, identitymap);
            identitymap.put(this, new ComparisonPropagator(
                    (IntVar) identitymap.get(vars[0]),
                    (IntVar) identitymap.get(vars[1]),
                    comparator.toString(), value));
        }
    }


    private interface Comparator extends Cloneable {
        void apply(IntVar op1, int op2, IntVar res) throws ContradictionException;
        boolean satisfies(IntVar res, IntVar op, int val);
        Comparator clone() throws CloneNotSupportedException;
    }

    private class GtComparator implements Comparator {
        @Override
        public void apply(final IntVar op1, final int op2, final IntVar res) throws ContradictionException {
            if (op1.getLB() > op2) {
                res.updateLowerBound(1, aCause);
            } else if (op1.getUB() <= op2) {
                res.updateUpperBound(0, aCause);
            }
            if (res.getLB() > 0) {
                op1.updateLowerBound(value + 1, aCause);
            } else if (res.getUB() == 0) {
                op1.updateUpperBound(value, aCause);
            }
        }

        @Override
        public boolean satisfies(final IntVar res, final IntVar op, final int val) {
            return (op.getValue() > val) == (res.getValue() != 0);
        }

        @Override
        public Comparator clone() throws CloneNotSupportedException {
            return (GtComparator) super.clone();
        }

        @Override
        public String toString() {
            return ">";
        }
    }
}
