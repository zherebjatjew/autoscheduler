package org.chocosolver.solver.constraints;


import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.types.Bound;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;
import org.chocosolver.util.tools.ArrayUtils;


/**
 * Measures degree of item order.
 *
 * <br/>Arguments:
 * <ul>
 *     <li>IntVar[n] VARS - array of variables</li>
 *     <li>int[n][2] WEIGHTS - array of values of VARS (possibly incomplete) mapped to weight of each value</li>
 *     <li>IntVar{0..n} DEGREE - number of desired positions, calculated as number of cases when item's weight is
 *     greater that weight of the nearest item above. Values not mentioned in WEIGHTS are ignored.
 *     </li>
 * </ul>
 *
 * Practical use case: we have a set of subjects. Some of them are preferred to be upper than others. Some are
 * order-insensitive. The propagator allows to say how close is the VARS list to fulfill the preference: the
 * bigger is DEGREE, the better is the solution.
 */
public class WeightPropagator extends Propagator<IntVar> {
    private static final long serialVersionUID = 440100527473432597L;

    private int[][] weights;

    public static WeightPropagator create(final IntVar[] vars, final int[][] weights, final IntVar dergee) {
        final WeightPropagator result = new WeightPropagator(ArrayUtils.append(new IntVar[]{dergee}, vars));
        result.weights = weights;
        return result;
    }

    protected WeightPropagator(final IntVar[] vv) {
        super(vv);
    }

    @Override
    public void propagate(final int evtmask) throws ContradictionException {
        final Bound bound = getBound();
        vars[0].updateLowerBound(bound.getMin(), aCause);
        vars[0].updateUpperBound(bound.getMax(), aCause);
    }

    @Override
    public ESat isEntailed() {
        final Bound bound = getBound();
        if (!bound.contains(vars[0])) {
            return ESat.FALSE;
        }
        if (bound.isClosed() && vars[0].isInstantiated()) {
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    protected Bound getBound() {
        // build possible weight according to current values
        final int[] bounds = new int[2*(vars.length - 1)];
        int pos = 0;
        for (int i = 1; i < vars.length; i++) {
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            final IntVar v = vars[i];
            for (int j = 0; j < weights.length; j++) {
                if (v.getUB() >= weights[j][0] && v.getLB() <= weights[j][0]) {
                    min = Math.min(min, weights[j][1]);
                    max = Math.max(max, weights[j][1]);
                }
            }
            if (min <= max) {
                bounds[pos*2 + 0] = min;
                bounds[pos*2 + 1] = max;
                pos++;
            }
        }
        // count correct positions
        int min = 0;
        int max = 0;
        for (int k = 1; k < pos; k++) {
            int idx = k * 2;
            int v1min = bounds[idx - 2];
            int v1max = bounds[idx - 1];
            int v2min = bounds[idx + 0];
            int v2max = bounds[idx + 1];
            if (v1max <= v2min) {
                min++;
            }
            if (v1min <= v2max) {
                max++;
            }
        }
        return new Bound(min, max);
    }
}

