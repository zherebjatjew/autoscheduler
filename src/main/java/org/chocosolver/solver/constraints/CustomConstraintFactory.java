package org.chocosolver.solver.constraints;

import org.chocosolver.solver.constraints.map.CountingPropagator;
import org.chocosolver.solver.variables.IntVar;


public final class CustomConstraintFactory {
    private CustomConstraintFactory() {
    }

    /**
     * Consider {@code x} and {@code y} as pairs of object fields: {@code object[0].x = x[0], object[0].y = y[0], ...}.
     * Then the constraint ensures that s = count(object.x=xv && object.y=yv)
     * @param x array of X values
     * @param y array of Y values in tha same orde as X
     * @param xv value of X to look for
     * @param yv value of Y to look for
     * @param s where to put the count
     * @return constraint
     */
    public static Constraint mapcount(final IntVar[] x, final IntVar[] y, final int xv, final int yv, final IntVar s) {
        return new Constraint("MapCount", CountingPropagator.create(x, y, xv, s, yv));
    }

    public static Constraint member(final IntVar variable, final int[] table) {
        assert variable != null;
        assert table.length > 0;
        if (table.length == 1) {
            return ICF.arithm(variable, "=", table[0]);
        } else {
            return ICF.member(variable, table);
        }
    }

    public static Constraint not_member(final IntVar variable, final int[] table) {
        assert variable != null;
        assert table.length > 0;
        if (table.length == 1) {
            return ICF.arithm(variable, "!=", table[0]);
        } else {
            return ICF.not_member(variable, table);
        }
    }
}
