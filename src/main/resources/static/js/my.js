function createNoty(message, type) {
    var text = message;
    if (message.responseText) {
        text = JSON.parse(message.responseText).message;
    } else if (message.responseJSON) {
        text = JSON.parse(message.responseJSON).message;
    }
    Materialize.toast(text, 4000);
}

function createSubitemSilently(selector, url, data) {
    var val = selector.val();
    if (val) {
        $(selector.children()[0]).attr('selected', 'selected');
        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            dataType: 'text',
            success: function (response) {
                onChipAdded(selector, val, response);
            },
            error: function (response) {
                Materialize.toast(response.responseJSON.message, 4000);
            }
        })
    }
}

function insertSorted(arr, item, comparator) {
    if (comparator == null) {
        // emulate the default Array.sort() comparator
        comparator = function(a, b) {
            if (typeof a !== 'string') a = String(a);
            if (typeof b !== 'string') b = String(b);
            return (a > b ? 1 : (a < b ? -1 : 0));
        };
    }

    // get the index we need to insert the item at
    var min = 0;
    var max = arr.length;
    var index = Math.floor((min + max) / 2);
    while (max > min) {
        if (comparator(item, arr[index]) < 0) {
            max = index;
        } else {
            min = index + 1;
        }
        index = Math.floor((min + max) / 2);
    }

    // insert the item
    arr.splice(index, 0, item);
    return arr;
}

function onChipAdded(selector, val, id) {
    var option = selector.find("option[value='" + val + "']");
    var chip = $("<div class='chip' data-id='" + id + "'><span>" + option.text() + "</span></div>");
    var next = selector.parent();
    var chips = $('.chip', next.parent());
    for (var i = 0; i < chips.length; i++) {
        if ($($(chips[i]).children()[0]).text() > option.text()) {
            next = $(chips[i]);
            break;
        }
    }
    next.before(chip
        .append($("<i class='material-icons'>close</i>"))
        .click(function (e) {
            removeLimitation(chip)
        })
    );
    option.hide();
}

function removeLimitation(chip) {
    var limitationId = chip.data('id');
    $.ajax({
        method: 'DELETE',
        url: '/api/rooms/' + $('.team').attr('id') + '/limitations/' + limitationId,
        success: function () {
            var subjectName = chip.find('span').text();
            $('#add-subject').find('option').filter(function () {
                    return $(this).html() == subjectName}
            ).show();
            chip.remove();
        },
        error: function (response) {
            Materialize.toast(response.responseJSON.message, 4000);
        }
    });
}

$(document).ready(function() {
    // Remember active tab
    $('li.tab a').click(function () {
        window.location.hash=$(this).attr('href').replace('#', '');
    });

    $('#calc').click(function (e) {
        e.preventDefault();
        var progress = $('.progress');
        progress.show();
        var button = $(this);
        button.addClass('disabled');
        $.ajax({
            method: 'POST',
            url: '/admin/schedule/' + $('#plan').data("id"),
            error: function(response) {
                button.removeClass('disabled');
                progress.hide();
                Materialize.toast(response.responseJSON.message, 4000);
            },
            success: function() {
                location.reload();
            }
        });
        return false;
    });

    $('#delete-active-item').click(function(e) {
        e.preventDefault();
        var activeEntity = $('a.collection-item.active');
        var link = $(this).data('link');
        $.ajax({
            method: 'DELETE',
            url: link + activeEntity.attr('id'),
            success: function() {
                var nextTeam = activeEntity.next();
                if (nextTeam.length == 0) {
                    nextTeam = activeEntity.prev();
                }
                if (nextTeam.length > 0) {
                    window.location.replace(nextTeam.attr('href'));
                } else {
                    window.location.replace(link);
                }
            },
            error: function(response) {
                Materialize.toast(response.responseJSON.message, 4000);
            }
        });
        return false;
    });

    // Inplace edits
    $('.editable').jinplace();

    // Notifications
    var msg = $('#noty-message').text();
    if (msg != undefined && msg != '') {
        createNoty(msg, $('#noty-type').text());
    }

    // Popup handling stuff
    $('.button-collapse').sideNav();

    $('body').append('<div id="blackout"></div>');
    function centerBox() {

        var winWidth = $(window).width();
        var winHeight = $(document).height();
        var scrollPos = $(window).scrollTop();

        var disHeight = scrollPos + 150;

        $('.popup-box').css({'width' : winWidth+'px', 'left' : 0, 'top' : disHeight+'px'});
        $('#blackout').css({'width' : winWidth+'px', 'height' : winHeight+'px'});

        return false;
    }

    $(window).resize(centerBox);
    $(window).scroll(centerBox);
    centerBox();

    $('[class*=popup-link]').click(function(e) {

        e.preventDefault();
        e.stopPropagation();

        var name = $(this).attr('class');
        var id = name[name.length - 1];
        var scrollPos = $(window).scrollTop();

        $('#popup-box-'+id).show();
        $('#blackout').show();
        $('html,body').css('overflow', 'hidden');

        $('html').scrollTop(scrollPos);
    });

    $('[class*=popup-box]').click(function(e) {
        e.stopPropagation();
    });
    $('html').click(function() {
        var scrollPos = $(window).scrollTop();
        $('[id^=popup-box-]').hide();
        $('#blackout').hide();
        $("html,body").css("overflow","auto");
        $('html').scrollTop(scrollPos);
    });
    $('.close').click(function() {
        var scrollPos = $(window).scrollTop();
        $('[id^=popup-box-]').hide();
        $('#blackout').hide();
        $("html,body").css("overflow","auto");
        $('html').scrollTop(scrollPos);
    });
});


function deleteEntity(url) {
    //var row = $(this).parents('.row');
    $.ajax({
        type: 'delete',
        url: url,
        success: function () {
            location.reload();
        },
        error: function(e) {
            Materialize.toast(response.responseJSON.message, 4000);
        }
    })
}

