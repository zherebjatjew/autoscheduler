/*************************************************************
 * Item picker from one list to another.
 *
 * Items are all nodes with class 'ls-item'.
 *
 * @param source JQuery object - source item list
 * @param target JQuery object - destination item list
 * @param onadd callback function(list, item) - after adding item
 * @param onremove callback function(list, item) - after removing item
 * @constructor
 */
function ListSelector(source, target, onadd, onremove) {
    var _onadd = onadd ? onadd : function(a,b) {};
    var _onremove = onremove ? onremove : function(a,b) {};

    function getMenaing(node) {
        var button = $(node).find('.ls-button');
        var text = button.next();
        if (text.length == 0) {
            text = $(button.parent().children()[0]);
        }
        return text;
    }

    function insertOrdered(item, list) {
        var items = list.find('.ls-item');
        for (var i = 0; i < items.length; i++) {
            if (getMenaing(items[i]).text() > getMenaing(item).text()) {
                $(items[i]).before(item);
                return;
            }
        }
        list.append(item);
    }

    function createAdditionButton(node) {
        node.find('div.ls-button').remove();
        var div = $('<div class="col s2 l1 ls-button"></div>');
        var link = $('<a href="#"><i class="material-icons">chevron_left</i></a>');
        link.click(function() {
            insertOrdered(node, target);
            createRemovalButton(node);
            _onadd(target, node);
        });
        div.append(link);
        node.prepend(div);
    }
    function createRemovalButton(node) {
        node.find('div.ls-button').remove();
        var div = $('<div class="col s2 l1 ls-button right-align"></div>');
        var link = $('<a href="#"><i class="material-icons">chevron_right</i></a>');
        link.click(function() {
            insertOrdered(node, source);
            createAdditionButton(node);
            _onremove(source, node);
        });
        div.append(link);
        node.append(div);
    }
    target.find('.ls-item').each(function() {
        createRemovalButton($(this))
    });
    source.find('.ls-item').each(function() {
        createAdditionButton($(this))
    });

    this._source = source;
    this._target = target;

    this.undo = function(node) {
        if ($.contains(this._source[0], node[0])) {
            insertOrdered(node, this._target);
            createRemovalButton(node);
        } else if ($.contains(this._target[0], node[0])) {
            insertOrdered(node, this._source);
            createAdditionButton(node);
        }
    }
}
    
