package org.autoschedule.api;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
public class IndexTest {
	@Test
	public void shouldBeConstructedFromRaw() {
		final Index index = new Index(103);
		assertEquals(103, index.getRaw());
	}

	@Test
	public void shouldReturnCorrectPositions() {
		final Index index = new Index(11, 2);
		assertEquals(1, index.getWeek());
		assertEquals(4, index.getDay());
		assertEquals(2, index.getHour());
	}

	@Test
	public void shouldReturnCorrectStartTime() {
		final Index index = new Index(1, 2);
		final java.util.Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.set(2014, Calendar.NOVEMBER, 15);
		final Calendar result = index.getStartTime(calendar, 60*60*1000);
		assertEquals(2014, result.get(Calendar.YEAR));
		assertEquals(Calendar.NOVEMBER, result.get(Calendar.MONTH));
		assertEquals(16, result.get(Calendar.DAY_OF_MONTH));
		assertEquals(2, result.get(Calendar.HOUR_OF_DAY) - calendar.get(Calendar.HOUR_OF_DAY));
	}
}
