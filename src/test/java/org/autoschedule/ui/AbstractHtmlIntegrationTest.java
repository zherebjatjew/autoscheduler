package org.autoschedule.ui;

import com.gargoylesoftware.htmlunit.WebClient;
import org.autoschedule.rest.AbstractResourceIntegrationTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebConnection;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;


@WithMockUser(username = "test", roles = {"SUPERUSER"})
public abstract class AbstractHtmlIntegrationTest extends AbstractResourceIntegrationTest {

    protected WebClient webClient;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        webClient = new WebClient();
        webClient.setWebConnection(new MockMvcWebConnection(mvc));
    }

    protected abstract String getAddress();

    @Test
    public void shouldHaveThePage() throws Exception {
        mvc.perform(get(getAddress()).with(csrf()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(xpath("/html/body").exists());
    }

    @Test
    public void shouldHaveMenu() throws Exception {
        mvc.perform(get(getAddress()))
                .andExpect(xpath("//header/nav").exists())
                .andExpect(xpath("//header//ul/li//a[@href]").nodeCount(12))
                .andExpect(xpath("//div[@id='nav-mobile']//a[@href]").nodeCount(6))
        ;
    }
}
