package org.autoschedule.ui;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.hamcrest.Matchers.greaterThan;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;


public class TeamPageTest extends AbstractHtmlIntegrationTest {

    @Override
    protected String getAddress() {
        return "/teams/00000000-0001-0000-0000-000000009999";
    }

    @Test
    public void shouldRedirectToFirstTeam() throws Exception {
        final MockHttpServletResponse response = mvc.perform(get("/teams"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse();
        matchesPattern(
                response.getRedirectedUrl(),
                "/teams/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/?"
        );
    }

    @Test
    public void shouldHaveTeamList() throws Exception {
        mvc.perform(get(getAddress()))
                .andExpect(xpath("//div[contains(concat(' ', normalize-space(@class), ' '), ' collection ')]/a[@id='00000000-0001-0000-0000-000000009999'][contains(concat(' ', normalize-space(@class), ' '), ' active ')]").nodeCount(1))
                .andExpect(xpath("//div[contains(concat(' ', normalize-space(@class), ' '), ' collection ')]/a[@class=contains(concat(' ', normalize-space(@class), ' '), ' collection-item ')]").nodeCount(greaterThan(2)));
    }
}
