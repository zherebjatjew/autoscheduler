package org.autoschedule.ui;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class TeacherPageTest extends AbstractHtmlIntegrationTest {

    @Override
    protected String getAddress() {
        return "/teachers/00000000-0138-0000-0000-00000000cccc";
    }

    @Test
    public void shouldRedirectToFirstTeacher() throws Exception {
        final MockHttpServletResponse response = mvc.perform(get("/teams"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse();
        matchesPattern(
                response.getRedirectedUrl(),
                "/teams/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/?"
        );
    }

}
