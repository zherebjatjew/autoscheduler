package org.autoschedule.ui;

public class StudentPageTest extends AbstractHtmlIntegrationTest {

    @Override
    protected String getAddress() {
        return "/students";
    }
}
