package org.autoschedule.domain;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.autoschedule.api.ObjectNotFoundException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.UUID;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

@DatabaseSetup("classpath:/sample-data-room.xml")
public class RoomRepositoryTest extends AbstractDaoIntegrationTest {
	@Autowired
	private RoomRepository dao;

	@Test
	public void shouldContainTeachers() {
		assertThat(dao.findAll(), is(not(emptyIterable())));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldThrowExceptionWhenIdNotFound() {
		final Room room = dao.findOne(UUID.fromString("00000000-0100-0000-0000-00000000aaaa"));
		assertNull(room);
	}

	@Test
	public void shouldGetById() {
		final Room room = dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000aaaa"));
		assertFalse(room == null);
		assertEquals("1-100", room.getName());
	}

	@Test
	public void shouldGetByName() {
		final Room room = dao.findByName("1-101");
		assertFalse(room == null);
		assertEquals(UUID.fromString("00000000-0002-0000-0000-00000000aaaa"), room.getId());
		assertEquals(20, room.getCapacity());
	}

	@Test
	public void shouldSaveInstances() {
		final Room room = new Room();
		room.setName("5-900");
		final UUID id = dao.save(room).getId();
		assertFalse(id == null);
		final Room stored = dao.findOne(id);
		assertEquals(room.getName(), stored.getName());
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldDeleteInstances() {
		dao.delete(UUID.fromString("00000000-0001-0000-0000-00000000aaaa"));
		dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000aaaa"));
	}

	@Test
	public void shouldSilentlySkipDeletingOfNonExisting() {
		dao.delete(UUID.fromString("ffffffff-ffff-ffff-ffff-ffffffffffff"));
	}

    @Test
    public void shouldWorkInUTF8() {
        Room room = new Room();
        room.setName("Лаборатория");
        room.setCapacity(10);
        room = dao.save(room);
        room = dao.findOne(room.getId());
        assertEquals("Лаборатория", room.getName());
    }

	@Test(expected = DataIntegrityViolationException.class)
    @Ignore("Does not work. Probably due to deferred commit")
	public void roomNamesShouldBeUnique() {
		final Room room = new Room();
		room.setName("1-100");
		dao.save(room);
	}
}
