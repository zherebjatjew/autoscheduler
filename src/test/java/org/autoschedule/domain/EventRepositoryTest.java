package org.autoschedule.domain;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.autoschedule.api.Index;
import org.autoschedule.api.Schedule;
import org.autoschedule.service.impl.ScheduleImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.not;

/**
 * Test event repository.
 */
@DatabaseSetup("classpath:/sample-data-event.xml")
public class EventRepositoryTest extends AbstractDaoIntegrationTest {

	@Autowired
	private EventRepository dao;

	@Autowired
	private PlanRepository planDao;

	@Test
	public void shouldCleanByPlanId() {
		assertThat(StreamSupport.stream(dao.findAll().spliterator(), false)
				.filter(event -> event.getPlan().getId().equals(UUID.fromString("00000000-0001-0000-0000-00000000eeee")))
                        .toArray(), not(emptyArray()));
		dao.clean(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		assertThat(StreamSupport.stream(dao.findAll().spliterator(), false)
				.filter(event -> event.getPlan().getId().equals(UUID.fromString("00000000-0001-0000-0000-00000000eeee")))
                        .toArray(), emptyArray());
	}

	@Test
	public void shouldSaveEvents() {
		assertThat(StreamSupport.stream(dao.findAll().spliterator(), false)
				.filter(event -> event.getPlan().getId().equals(UUID.fromString("00000000-0002-0000-0000-00000000eeee")))
                        .toArray(), emptyArray());
		final Schedule timetable = new ScheduleImpl(Arrays.asList(
                event(UUID.fromString("00000000-0002-0000-0000-00000000eeee")),
                event(UUID.fromString("00000000-0002-0000-0000-00000000eeee"))));
		dao.save(timetable);
		assertThat(StreamSupport.stream(dao.findAll().spliterator(), false)
				.filter(event -> event.getPlan().getId().equals(UUID.fromString("00000000-0002-0000-0000-00000000eeee")))
                        .toArray(), not(emptyArray()));
	}

	private Event event(final UUID planId) {
		final Event result = new Event();
		result.setPosition(new Index(1));
		result.setPlan(planDao.findOne(planId));
		return result;
	}
}
