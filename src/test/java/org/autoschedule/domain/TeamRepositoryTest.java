package org.autoschedule.domain;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import org.autoschedule.api.ObjectNotFoundException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Arrays;
import java.util.UUID;

public class TeamRepositoryTest extends AbstractDaoIntegrationTest {
	@Autowired
	private TeamRepository dao;

	@Test
	public void shouldContainGroups() {
		assertThat(dao.findAll(), is(not(emptyIterable())));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldThrowExceptionWhenNotFound() {
		dao.findOne(UUID.fromString("ffffffff-ffff-ffff-ffff-ffffffffffff"));
	}

	@Test
	public void shouldGetByName() {
		final Team team = dao.findByTitle("${year}A").iterator().next();
		assertFalse(team == null);
		assertEquals(UUID.fromString("00000000-0001-0000-0000-000000009999"), team.getId());
	}

	@Test
	public void shouldGetById() {
		final Team team = dao.findOne(UUID.fromString("00000000-0001-0000-0000-000000009999"));
		assertFalse(team == null);
		assertEquals("${year}A", team.getTitle());
	}

	@Test
	public void shouldSaveInstances() {
		final Team team = new Team();
		team.setTitle("D-900");
		final UUID id = dao.save(team).getId();
		assertFalse(id == null);
		final Team stored = dao.findOne(id);
		assertEquals(team.getTitle(), stored.getTitle());
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldDeleteInstances() {
		dao.delete(UUID.fromString("00000000-0002-0000-0000-000000009999"));
		dao.findOne(UUID.fromString("00000000-0002-0000-0000-000000009999"));
	}

	@Test
	public void shouldSilentlySkipDeletingOfNonExisting() {
		dao.delete(UUID.fromString("00000000-0001-ffff-ffff-000000009999"));
	}

	@Test
	public void shouldGetTeamSizes() {
		final Iterable<TeamRepository.TeamSize> teamSizes = dao.getTeamSizes(Arrays.asList(
                UUID.fromString("00000000-0001-0000-0000-000000009999"),
                UUID.fromString("00000000-0002-0000-0000-000000009999")
        ));
		assertThat(teamSizes, hasItem(hasProperty("studentCount", equalTo(10L))));
		assertThat(teamSizes, hasItem(hasProperty("studentCount", equalTo(20L))));
	}
}
