package org.autoschedule.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dzherebjatjew on 5/16/15.
 */
public class SubjectHourTest {
	@Test
	public void shouldCompareEmpty() {
		final SubjectHour first = new SubjectHour();
		final SubjectHour second = new SubjectHour();
		assertEquals(first, second);
	}
}
