package org.autoschedule.domain;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;
import java.util.stream.Collectors;

public class PlanRepositoryTest extends AbstractDaoIntegrationTest {
	@Autowired
	private PlanRepository dao;
	@Autowired
	private EventRepository eventRepository;

	@Test
	public void empty() {
		assertNotNull(dao);
	}

	@Test
	public void shouldContainPlan() {
		final Plan plan1 = dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		assertNotNull(plan1);
	}

	@Test
	public void shouldCalculateSubtotals() {
		final Iterable<SubjectHour> hours = dao.getHourSubtotals(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		assertThat(hours, is(iterableWithSize(2)));
		SubjectHour h = hours.iterator().next();
		assertEquals(UUID.fromString("00000000-0002-0000-0000-00000000dddd"), h.getSubject().getId());
		assertEquals(UUID.fromString("00000000-0001-0000-0000-00000000eeee"), h.getPlan().getId());
	}

	@Test
	public void shouldGetConcernedGroups() {
		final Iterable<UUID> teamIds = dao.getConcernedTeams(UUID.fromString("00000000-0001-0000-0000-00000000eeee")).stream()
                .map(Team::getId).collect(Collectors.toSet());
		assertThat(teamIds, is(iterableWithSize(2)));
		assertThat(teamIds, containsInAnyOrder(
                UUID.fromString("00000000-0001-0000-0000-000000009999"),
                UUID.fromString("00000000-0002-0000-0000-000000009999")));
	}

	@Test
	public void shouldGetConcernedSubjects() {
		final Iterable<Subject> subjectIds = dao.getConcernedSubjects(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		assertThat(subjectIds, is(iterableWithSize(1)));
		assertEquals(UUID.fromString("00000000-0002-0000-0000-00000000dddd"), subjectIds.iterator().next().getId());
	}

	@Test
	public void shouldSupportRemovalWithEvents() {
		final Plan plan = dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		final Event event = new Event();
		event.setPlan(plan);
		eventRepository.save(event);
		dao.delete(plan);
	}

/*
	@Test
	public void shouldCalculateCapacity() {
		final Iterable<PlanRepository.TeamSize> capacities = repository.getTeamSizes(3);
		assertThat(capacities, is(iterableWithSize(2)));
		assertEquals(10, (long) capacities.iterator().next().getStudentCount());
	}

	@Test
	public void shouldBeStable() {
		assertThat(repository.getTeamSizes(1), hasItem(hasProperty("studentCount", equalTo(10L))));
		repository.getConcernedTeams(1);
		assertThat(repository.getTeamSizes(1), hasItem(hasProperty("studentCount", equalTo(10L))));
		repository.getConcernedSubjects(1);
		assertThat(repository.getTeamSizes(1), hasItem(hasProperty("studentCount", equalTo(10L))));
	}

	@Test
	public void shouldFetchSubjectHours() {
		final Plan plan = repository.findOne(1);
		assertFalse(plan.getSubjects().isEmpty());
	}

	@Test(expected = EntityNotFoundException.class)
	public void shouldThrowExceptionWhenNotFound() {
		repository.findOne(-1);
	}
*/
}
