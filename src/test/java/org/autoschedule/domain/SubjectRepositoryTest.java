package org.autoschedule.domain;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import org.autoschedule.api.ObjectNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class SubjectRepositoryTest extends AbstractDaoIntegrationTest {
	@Autowired
	private SubjectRepository dao;

	@Test
	public void shouldContainGroups() {
		assertThat(dao.findAll(), is(not(emptyIterable())));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldThrowExceptionWhenNotFound() {
		dao.findOne(UUID.fromString("00000000-0000-0000-0000-000000000000"));
	}

	@Test
	public void shouldGetById() {
		final Subject subject = dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000dddd"));
		assertFalse(subject == null);
		assertEquals("math", subject.getName());
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldThrowExceptionWhenUuidNotFound() {
		dao.findOne(UUID.fromString("ffffffff-ffff-ffff-ffff-ffffffffffff"));
	}

	@Test
	public void shouldSaveInstances() {
		final Subject subject = new Subject();
		subject.setName("religion");
		final UUID id = dao.save(subject).getId();
		assertFalse(id == null);
		final Subject stored = dao.findOne(id);
		assertEquals(subject.getName(), stored.getName());
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldDeleteInstances() {
		dao.delete(UUID.fromString("00000000-0001-0000-0000-00000000dddd"));
		dao.findOne(UUID.fromString("00000000-0001-0000-0000-00000000dddd"));
	}

	@Test
	public void shouldSilentlySkipDeletingOfNonExisting() {
		dao.delete(UUID.fromString("00000000-ffff-0000-0000-00000000dddd"));
	}
}
