package org.autoschedule.domain;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.autoschedule.AuthoschedulerTest;
import org.autoschedule.TestWebSecurityConfiguration;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Abstract base for all DB integration tests.
 */
@ActiveProfiles("test")
@Transactional
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AuthoschedulerTest.class, TestWebSecurityConfiguration.class})
@TestExecutionListeners({
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:/sample-data-basic.xml")
@WithMockUser(username = "test", roles = {"SUPERUSER"})
public class AbstractDaoIntegrationTest {
    @Resource
    private AuthenticationManager authenticationManager;

	@Before
	public void setup() {
        final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken("test", "");
        final Authentication auth = authenticationManager.authenticate(authRequest);
        final SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(auth);
    }
}
