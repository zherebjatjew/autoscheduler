package org.autoschedule.domain;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import org.autoschedule.api.ObjectNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class TeacherRepositoryTest extends AbstractDaoIntegrationTest {
	@Autowired
	private TeacherRepository dao;

	@Test
	public void shouldContainTeachers() {
		assertThat(dao.findAll(), is(not(emptyIterable())));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldThrowExceptionWhenNotFound() {
		dao.findOne(UUID.fromString("00000000-0100-ffff-ffff-00000000cccc"));
	}

	@Test
	public void shouldGetById() {
		final Teacher teacher = dao.findOne(UUID.fromString("00000000-0101-0000-0000-00000000cccc"));
		assertFalse(teacher == null);
		assertEquals("Donette Koster", teacher.getName());
	}

	@Test
	public void shouldHaveSubjects() {
		final Teacher teacher = dao.findOne(UUID.fromString("00000000-0100-0000-0000-00000000cccc"));
		assertFalse(teacher.getSubjects().isEmpty());
	}

	@Test
	public void shouldSaveInstances() {
		final Teacher teacher = new Teacher();
		teacher.setName("John Doe");
		final UUID id = dao.save(teacher).getId();
		assertFalse(id == null);
		final Teacher stored = dao.findOne(id);
		assertEquals(teacher.getName(), stored.getName());
	}

	@Test(expected = ObjectNotFoundException.class)
	public void shouldDeleteInstances() {
		dao.delete(UUID.fromString("00000000-0120-0000-0000-00000000cccc"));
		dao.findOne(UUID.fromString("00000000-0120-0000-0000-00000000cccc"));
	}

	@Test
	public void shouldSilentlySkipDeletingOfNonExisting() {
		dao.delete(UUID.fromString("00000000-0100-ffff-ffff-00000000cccc"));
	}

}
