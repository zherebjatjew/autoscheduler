package org.autoschedule.domain;

import org.autoschedule.api.ObjectNotFoundException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class SubjectHourRepositoryTest extends AbstractDaoIntegrationTest {
    @Autowired
    private SubjectHourRepository dao;
    @Autowired
    private PlanRepository planRepository;

    @Test
    public void empty() {
        assertNotNull(dao);
    }

    @Test
    public void shouldContainRecord() {
        final SubjectHour record = dao.findOne(UUID.fromString("00000000-0003-0000-0000-00000000ffff"));
        assertNotNull(record);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldThrowExceptionWhenUuidNotFound() {
        dao.findOne(UUID.fromString("ffffffff-ffff-ffff-ffff-ffffffffffff"));
    }

    @Test
    @Ignore("You can not add item to plan this way. Use plan.setSubject.add instead")
    public void shouldSaveInstances() {
        final SubjectHour hours = new SubjectHour();
        hours.setHours(19);
        final Plan plan = planRepository.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee"));
        assertThat(plan.getSubjects(), emptyIterable());
        hours.setPlan(plan);
        dao.save(hours);
        assertThat(
                planRepository.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee")).getSubjects(),
                iterableWithSize(1));
    }

    @Test
    public void shouldSaveInstancesViaHourRepository() {
        final SubjectHour hours = new SubjectHour();
        hours.setHours(19);
        final Plan plan = planRepository.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee"));
        assertThat(plan.getSubjects(), emptyIterable());
        hours.setPlan(plan);
        plan.getSubjects().add(hours);
        planRepository.save(plan);
        assertThat(
                planRepository.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee")).getSubjects(),
                iterableWithSize(1));
    }

    @Test
    public void shouldUpdateHours() {
        final SubjectHour hour = dao.findOne(UUID.fromString("00000000-0003-0000-0000-00000000ffff"));
        hour.setHours(100);
        dao.save(hour);
        assertThat
                (planRepository.findOne(UUID.fromString("00000000-0003-0000-0000-00000000eeee")).getSubjects(),
                hasItem(hasProperty("hours", is(100))));
    }
}
