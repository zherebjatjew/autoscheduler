package org.autoschedule.domain;

import java.util.Random;

import org.autoschedule.domain.Plan;
import org.autoschedule.domain.Subject;
import org.autoschedule.domain.Team;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlanTest
{
	@Test
	public void setSubjectShouldOverwriteHours() {
		final Subject subject = new Subject();
		final Team team = new Team();
		final Plan plan = new Plan();
		plan.setHours(subject, team, 10);
		assertEquals(10, (int) plan.getSubjects().iterator().next().getHours());

		plan.setHours(subject, team, 2);
		assertEquals(1, plan.getSubjects().size());
		assertEquals(2, (int) plan.getSubjects().iterator().next().getHours());
	}

	@Test
	public void settingSubjectHoursTo0ShouldRemoveEntry() {
		final Subject subject = new Subject();
		final Team team = new Team();
		final Plan plan = new Plan();
		plan.setHours(subject, team, 10);
		assertFalse(plan.getSubjects().isEmpty());
		plan.setHours(subject, team, 0);
		assertTrue(plan.getSubjects().isEmpty());
	}
}