package org.autoschedule.domain;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.Matchers.iterableWithSize;
import static org.junit.Assert.assertThat;

/**
 * {@link LimitationRepository} test.
 */
@DatabaseSetup("classpath:/sample-data-limitation.xml")
public class LimitationRepositoryTest extends AbstractDaoIntegrationTest {

	@Autowired
	private LimitationRepository limitationRepository;

	@Test
	public void shouldReturnDefaultLimitations() {
		final Iterable<Limitation> constraints = limitationRepository.findByPlan(UUID.fromString("00000000-0002-0000-0000-00000000eeee"));
		assertThat(constraints, iterableWithSize(3));
	}

	@Test
	public void shouldJoinOwnLimitationsWithDefaults() {
		final Iterable<Limitation> constraints = limitationRepository.findByPlan(UUID.fromString("00000000-0001-0000-0000-00000000eeee"));
		assertThat(constraints, iterableWithSize(4));
	}
}
