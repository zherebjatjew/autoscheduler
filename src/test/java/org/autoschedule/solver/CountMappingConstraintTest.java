package org.autoschedule.solver;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.CustomConstraintFactory;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountMappingConstraintTest {

    @Test
    public void shouldCountWhatever() {
        final Solver solver = new Solver();
        final IntVar[] x = VF.integerArray("X", 1, 0, 2, solver);
        final IntVar[] y = VF.integerArray("Y", 1, 0, 2, solver);
        final IntVar s = VF.integer("S", 1, 1, solver);
        solver.post(CustomConstraintFactory.mapcount(x, y, 0, 1, s));
        Chatterbox.showSolutions(solver);
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(1, solver.getMeasures().getSolutionCount());
        final Solution solution = solver.getSolutionRecorder().getLastSolution();
        assertEquals(0, (int) solution.getIntVal(x[0]));
        assertEquals(1, (int) solution.getIntVal(y[0]));
    }

    @Test
    public void shouldResolveToZeroIfNoX() {
        final Solver solver = new Solver();
        final IntVar[] x = VF.integerArray("X", 1, 0, 2, solver);
        final IntVar[] y = VF.integerArray("Y", 1, 0, 2, solver);
        final IntVar s = VF.integer("S", 1, 1, solver);
        solver.post(CustomConstraintFactory.mapcount(x, y, 3, 1, s));
        Chatterbox.showSolutions(solver);
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(0, solver.getMeasures().getSolutionCount());
    }

    @Test
    public void shouldResolveToZeroIfNoY() {
        final Solver solver = new Solver();
        final IntVar[] x = VF.integerArray("X", 1, 0, 2, solver);
        final IntVar[] y = VF.integerArray("Y", 1, 0, 2, solver);
        final IntVar s = VF.integer("S", 1, 1, solver);
        solver.post(CustomConstraintFactory.mapcount(x, y, 0, 3, s));
        Chatterbox.showSolutions(solver);
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(0, solver.getMeasures().getSolutionCount());
    }
}
