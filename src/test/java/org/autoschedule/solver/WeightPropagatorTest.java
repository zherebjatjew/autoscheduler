package org.autoschedule.solver;

import org.apache.commons.lang3.ArrayUtils;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.WeightPropagator;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@ActiveProfiles("test")
public class WeightPropagatorTest {

    @Test
    public void whenAllWeightsAreDefined() {
        final Solver solver = new Solver();
        solver.set(new AllSolutionsRecorder(solver));
        final IntVar[] vars = VF.integerArray("V", 5, 0, 4, solver);
        final int[][] weights = new int[5][2];
        for (int i = 0; i < weights.length; i++) {
            weights[i][0] = i;
            weights[i][1] = i;
        }
        final IntVar degree = VF.integer("D", 0, 100, solver);
        solver.post(new Constraint("sort", WeightPropagator.create(vars, weights, degree)));
        assertThat(solver.findAllSolutions(), greaterThan(0L));
        solver.getSolutionRecorder().getSolutions().forEach(solution -> {
            final int[] values = new int[vars.length];
            int count = 0;
            for (int i = 0; i < vars.length; i++) {
                values[i] = solution.getIntVal(vars[i]);
                if (i > 0 && values[i-1] <= values[i]) {
                    count++;
                }
            }
            assertEquals(ArrayUtils.toString(values), count, (int) solution.getIntVal(degree));
        });
    }

    @Test
    public void whenSomeWeightsAreDefined() {
        final Solver solver = new Solver();
        solver.set(new AllSolutionsRecorder(solver));
        final IntVar[] vars = VF.integerArray("V", 5, 0, 4, solver);
        final int[][] weights = new int[3][2];
        for (int i = 0; i < weights.length; i++) {
            weights[i][0] = i;
            weights[i][1] = i;
        }
        final IntVar degree = VF.integer("D", 0, 100, solver);
        solver.post(new Constraint("sort", WeightPropagator.create(vars, weights, degree)));
        assertThat(solver.findAllSolutions(), greaterThan(0L));
        solver.getSolutionRecorder().getSolutions().forEach(solution -> {
            final int[] values = new int[vars.length];
            int count = 0;
            int pos = 0;
            for (final IntVar var : vars) {
                values[pos] = solution.getIntVal(var);
                if (values[pos] < weights.length) {
                    if (pos > 0 && values[pos - 1] <= values[pos]) {
                        count++;
                    }
                    pos++;
                }
            }
            assertEquals(ArrayUtils.toString(values), count, (int) solution.getIntVal(degree));
        });
    }
}
