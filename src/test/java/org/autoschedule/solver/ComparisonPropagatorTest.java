package org.autoschedule.solver;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.arithm.ComparisonPropagator;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ComparisonPropagatorTest {

    @Test
    public void shouldFindGreaterThan() {
        final Solver solver = new Solver();
        final IntVar r = VF.integer("RES", 0, 1, solver);
        final IntVar o = VF.integer("OPERATOR", 0, 100, solver);
        solver.post(new Constraint("Comparison", new ComparisonPropagator(r, o, ">", 99)));
        solver.post(ICF.arithm(r, "=", 1));
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(1, solver.getMeasures().getSolutionCount());
        assertEquals(100, (int) solver.getSolutionRecorder().getLastSolution().getIntVal(o));
    }

    @Test
    public void shouldFindNotGreaterThan() {
        final Solver solver = new Solver();
        final IntVar r = VF.integer("RES", 0, 1, solver);
        final IntVar o = VF.integer("OPERATOR", 0, 100, solver);
        solver.post(new Constraint("Comparison", new ComparisonPropagator(r, o, ">", 99)));
        solver.post(ICF.arithm(r, "=", 0));
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(100, solver.getMeasures().getSolutionCount());
        solver.getSolutionRecorder().getSolutions().forEach(solution -> {
            assertTrue(solution.getIntVal(o) <= 99);
        });
    }

    @Test
    public void shouldHaveNoSolutions() {
        final Solver solver = new Solver();
        final IntVar r = VF.integer("RES", 0, 1, solver);
        final IntVar o = VF.integer("OPERATOR", 0, 100, solver);
        solver.post(new Constraint("Comparison", new ComparisonPropagator(r, o, ">", 100)));
        solver.post(ICF.arithm(r, "=", 1));
        solver.set(new AllSolutionsRecorder(solver));
        solver.findAllSolutions();
        assertEquals(0, solver.getMeasures().getSolutionCount());
    }
}
