package org.autoschedule.solver;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.chocosolver.util.objects.graphs.MultivaluedDecisionDiagram;
import org.junit.Test;

/**
 * Created by dzherebjatjew on 4/3/15.
 */
public class MultivaluedDecisionDiagramTest {
	@Test
	public void shouldSolverSomething() {
		final Solver solver = new Solver();
		final IntVar[] vars = VF.enumeratedArray("X", 2, -2, 2, solver);
		final Tuples tuples = new Tuples();
		tuples.add(0, -1);
		tuples.add(1, -1);
		tuples.add(0, 1);
		solver.post(ICF.mddc(vars, new MultivaluedDecisionDiagram(vars, tuples)));
		Chatterbox.showSolutions(solver);
		solver.findAllSolutions();
	}
}
