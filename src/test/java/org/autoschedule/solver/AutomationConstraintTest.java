package org.autoschedule.solver;

import java.util.List;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.nary.automata.FA.FiniteAutomaton;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
public class AutomationConstraintTest {
	@Test
	/**
	 * An example of automation from <a href ="http://choco-solver.org/?q=node/110">choco forum</a>
	 * <p>
	 *     produces following solutions:
	 * </p>
	 *     <table>
	 *         <tr>
	 *             <th>CS1</th><th>CS2</th><th>CS3</th><th>CS4</th>
	 *         </tr>
	 *         <tr>
	 *             <td>1</td><td>3</td><td>3</td><td>4</td>
	 *             <td>1</td><td>3</td><td>3</td><td>5</td>
	 *             <td>2</td><td>3</td><td>3</td><td>4</td>
	 *             <td>2</td><td>3</td><td>3</td><td>5</td>
	 *         </tr>
	 *     </table>
	 */
	public void shouldSolveSomething() {
		Solver solver = new Solver();
		IntVar[] CS = VF.enumeratedArray("CS", 4, 1, 5, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("(1|2)(3*)(4|5)")));

		Chatterbox.showSolutions(solver);
		solver.set(new AllSolutionsRecorder(solver));
		solver.findAllSolutions();

		assertEquals(4, solver.getSolutionRecorder().getSolutions().size());
	}

	@Test
	/**
	 * Ensure than automation is able to work with values over one-byte size.
	 */
	public void shouldOperateWithIntegers() {
		Solver solver = new Solver();
		IntVar[] CS = VF.enumeratedArray("CS", 4, 1000, 1050, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("<1005><1010>*<1040>")));
		solver.set(new AllSolutionsRecorder(solver));
		solver.findAllSolutions();

		final List<Solution> solutions = solver.getSolutionRecorder().getSolutions();

		assertEquals(1, solutions.size());
		assertEquals(1005, (long) solutions.get(0).getIntVal(CS[0]));
		assertEquals(1010, (long) solutions.get(0).getIntVal(CS[1]));
		assertEquals(1010, (long) solutions.get(0).getIntVal(CS[2]));
		assertEquals(1040, (long) solutions.get(0).getIntVal(CS[3]));
	}

	@Test
	@Ignore("Choco incorrectly processes negative values")
	/**
	 * Ensure than automation is able to work with negative values.
	 */
	public void shouldOperateWithNegativeIntegers() {
		Solver solver = new Solver();
		IntVar[] CS = VF.enumeratedArray("CS", 4, -10, 10, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("<-9>1*")));
		Chatterbox.showSolutions(solver);
		solver.set(new AllSolutionsRecorder(solver));
		solver.findAllSolutions();

		final List<Solution> solutions = solver.getSolutionRecorder().getSolutions();

		assertEquals(1, solutions.size());
		assertEquals(-9, (long) solutions.get(0).getIntVal(CS[0]));
		assertEquals(1, (long) solutions.get(0).getIntVal(CS[1]));
		assertEquals(1, (long) solutions.get(0).getIntVal(CS[2]));
		assertEquals(-5, (long) solutions.get(0).getIntVal(CS[3]));
	}

	@Test(expected = NumberFormatException.class)
	/**
	 * Ensure than automation is able to work with values greater than 2^31.
	 */
	public void shouldOperateWithBigIntegers() {
		Solver solver = new Solver();
		IntVar[] CS = VF.enumeratedArray("CS", 4, -10, 10, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("<4294967294>1*<-5>")));
	}

	@Test
	public void shouldUnderstandMultipliers() {
		Solver solver = new Solver();
		final int[] values = { 1, 2, 3, 4, 4, 5, 6 };
		IntVar[] CS = VF.enumeratedArray("CS", 6, values, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("(12){2}")));
		Chatterbox.showSolutions(solver);
		solver.findAllSolutions();
		solver.getSolutionRecorder().getSolutions().forEach(solution -> {
			assertEquals(1L, (long) solution.getIntVal(CS[1]));
			assertEquals(2L, (long) solution.getIntVal(CS[2]));
			assertEquals(1L, (long) solution.getIntVal(CS[3]));
			assertEquals(2L, (long) solution.getIntVal(CS[4]));
		});
	}

	@Test
	@Ignore("Dead loop while reading regular expression")
	public void shouldRecognizeNegation() {
		Solver solver = new Solver();
		final int[] values = { 1, 2, 3, 4 };
		IntVar[] CS = VF.enumeratedArray("CS", 2, values, solver);
		solver.post(ICF.regular(CS, new FiniteAutomaton("[^123]1")));
		solver.set(new AllSolutionsRecorder(solver));
		Chatterbox.showSolutions(solver);
		solver.findAllSolutions();
		assertEquals(1, solver.getSolutionRecorder().getSolutions().size());
	}

	@Test
	/**
	 * An illustration to http://choco-solver.org/?q=comment/84#comment-84.
	 */
	public void teamToSubjectCount() {
		final Solver solver = new Solver();
		final int[] values = { 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15 };
		IntVar[] CS = VF.enumeratedArray("CS", 6, values, solver);
		final String A = "(1|2|3|4|5)";
		final String A_ = "(2|3|4|5)";
		final String B = "(<10>|<11>|<12>|<13>|<14>)";
		final String B_ = "(<10>|<11>|<12>|<14>)";
		final String anyButThis = "((" + A + B_ + ")|(" + A_ + B + "))";
		String regexp = "((" + anyButThis + ")*1<13>){2}" + anyButThis + '*';
		solver.post(ICF.regular(CS, new FiniteAutomaton(regexp)));
		solver.set(new AllSolutionsRecorder(solver));
		Chatterbox.showSolutions(solver);
		solver.findAllSolutions();
		assertEquals(72, solver.getSolutionRecorder().getSolutions().size());
	}
}
