package org.autoschedule.solver;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.search.solution.Solution;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Test of <a href="http://choco-solver.org/user_guide/5_elements.html#icstr-cou">count</a> constraint.
 *
 */
@ActiveProfiles("test")
public class CountConstrainTest {

	@Test
	public void shouldLimitByExactValue() {
		final Solver solver = new Solver();
		final IntVar[] vars = {VF.enumerated("TEST", 1, 2, solver)};
		solver.post(ICF.count(1, vars, VF.fixed(0, solver)));

		solver.set(new AllSolutionsRecorder(solver));
		solver.findAllSolutions();

		assertEquals(1L, (long) solver.getSolutionRecorder().getSolutions().size());
		final Solution solution = solver.getSolutionRecorder().getLastSolution();
		assertEquals(2L, (long) solution.getIntVal(vars[0]));
	}

	@Test
	public void shouldLimitBySet() {
		final Solver solver = new Solver();
		final IntVar[] vars = {VF.enumerated("TEST", 1, 2, solver)};
		final IntVar cardinality = VF.enumerated("CR", 0, 1, solver);
		solver.post(ICF.count(1, vars, cardinality));
		solver.set(new AllSolutionsRecorder(solver));

		solver.findAllSolutions();

		assertEquals(2, solver.getSolutionRecorder().getSolutions().size());
		List<Integer> results = solver.getSolutionRecorder().getSolutions().stream()
				.map(s -> s.getIntVal(vars[0]))
				.collect(Collectors.toList());
		assertThat(results, containsInAnyOrder(1, 2));
	}

}
