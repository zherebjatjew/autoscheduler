package org.autoschedule.solver;


import org.apache.commons.lang3.ArrayUtils;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.TwoDimensionalWindowCountPropagator;
import org.chocosolver.solver.search.solution.AllSolutionsRecorder;
import org.chocosolver.solver.trace.Chatterbox;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;


@ActiveProfiles("test")
public class TwoDimensionalWindowCountPropagatorTest {

    @Test
    public void shouldCalculateWhatever() {
        final Solver solver = new Solver();
        final IntVar[][] hours = new IntVar[3][];
        for (int i = 0; i < hours.length; i++) {
            hours[i] = VF.integerArray("ROOM." + i, 2, 0, 1, solver);
        }
        final IntVar counter = VF.integer("COUNTER", 0, hours.length, solver);
        solver.post(new Constraint("seq", TwoDimensionalWindowCountPropagator.create(hours, counter, 0)));
        solver.set(new AllSolutionsRecorder(solver));
        Chatterbox.showSolutions(solver);
        assertThat(solver.findAllSolutions(), greaterThan(0L));
        solver.getSolutionRecorder().getSolutions().forEach(solution -> {
            int count = 0;
            boolean started = false;
            for (int i = hours.length - 1; i >= 0; i--) {
                int[] hour = new int[hours[i].length];
                for (int j = 0; j < hour.length; j++) {
                    hour[j] = solution.getIntVal(hours[i][j]);
                }
                if (ArrayUtils.contains(hour, 1)) {
                    if (!started) {
                        started = true;
                        count = i;
                    } else {
                        count--;
                    }
                }
            }
            assertEquals(count, (int) solution.getIntVal(counter));
        });
    }

    @Test
    public void shouldCalculateRealExample() {
        final Solver solver = new Solver();
        final IntVar[][] hours = new IntVar[3][];
        for (int i = 0; i < hours.length; i++) {
            hours[i] = VF.integerArray("ROOM." + i, 3, 0, 2, solver);
        }
        final IntVar counter = VF.integer("COUNTER", 0, hours.length, solver);
        solver.post(new Constraint("seq", TwoDimensionalWindowCountPropagator.create(hours, counter, 0)));
        solver.set(new AllSolutionsRecorder(solver));
        Chatterbox.showSolutions(solver);
        assertThat(solver.findAllSolutions(), greaterThan(0L));
        solver.getSolutionRecorder().getSolutions().forEach(solution -> {
            int total = 0;
            for (int groupId = 1; groupId < 3; groupId++) {
                int count = 0;
                boolean started = false;
                for (int i = hours.length - 1; i >= 0; i--) {
                    for (int j = 0; j < hours[i].length; j++) {
                        if (solution.getIntVal(hours[i][j]) == groupId) {
                            if (started) {
                                count--;
                            } else {
                                started = true;
                                count = i;
                            }
                            break;
                        }
                    }
                }
                total += count;
            }
            assertEquals(total, (int) solution.getIntVal(counter));
        });
    }
}
