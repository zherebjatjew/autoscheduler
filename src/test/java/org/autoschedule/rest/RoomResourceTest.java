package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Room rest repository test.
 */
@Slf4j
@DatabaseSetup("classpath:/sample-data-rest.xml")  // simply update uuids to distinguish from ids.
public class RoomResourceTest extends AbstractResourceIntegrationTest {

	@Test
	public void shouldExposeRoot() throws Exception {
		mvc.perform(get("/api"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.rooms.href", notNullValue()));
	}

	@Test
	public void shouldExposeCollection() throws Exception {
		mvc.perform(get("/api/rooms"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.rooms[1].name", notNullValue()));
	}

	@Test
	public void shouldGetById() throws Exception {
		mvc.perform(get("/api/rooms/00000000-0001-0000-0000-00000000aaaa"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("1-100")))
				.andExpect(jsonPath("$.capacity", is(10)));
	}

	@Test
	public void shouldRespondNotFound() throws Exception {
		mvc.perform(get("/api/rooms/00000000-0003-ffff-0000-00000000eeee"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
}
