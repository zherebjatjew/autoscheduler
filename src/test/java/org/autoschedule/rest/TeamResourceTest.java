package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Team rest repository test.
 */
@Slf4j
@DatabaseSetup("classpath:/sample-data-rest.xml")
public class TeamResourceTest extends AbstractResourceIntegrationTest {

	@Test
	public void shouldExposeRoot() throws Exception {
		mvc.perform(get("/api"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.teams.href", notNullValue()));
	}

	@Test
	public void shouldExposeCollection() throws Exception {
		mvc.perform(get("/api/teams"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.teams[1].title", notNullValue()));
	}

	@Test
	public void shouldGetById() throws Exception {
		mvc.perform(get("/api/teams/00000000-0009-0000-0000-000000009999"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is("B-140")))
				.andExpect(jsonPath("$._links.students.href", containsString("students")));
	}

	@Test
	public void shouldRespondNotFound() throws Exception {
		mvc.perform(get("/api/teams/00000000-0010-ffff-0000-000000009999"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
}
