package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Subject rest repository test.
 */
@Slf4j
@DatabaseSetup("classpath:/sample-data-rest.xml")  // simply update uuids to distinguish from ids.
public class SubjectResourceTest extends AbstractResourceIntegrationTest {

	@Test
	public void shouldExposeRoot() throws Exception {
		mvc.perform(get("/api"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.subjects.href", notNullValue()));
	}

	@Test
	public void shouldExposeCollection() throws Exception {
		mvc.perform(get("/api/subjects"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.subjects[1].name", notNullValue()));
	}

	@Test
	public void shouldGetById() throws Exception {
		mvc.perform(get("/api/subjects/00000000-0002-0000-0000-00000000dddd"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("german language")));
	}

	@Test
	public void shouldRespondNotFound() throws Exception {
		mvc.perform(get("/api/subjects/00000000-0002-ffff-0000-00000000dddd"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
}
