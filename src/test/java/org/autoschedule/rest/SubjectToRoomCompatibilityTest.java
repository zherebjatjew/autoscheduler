package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Limitation tests.
 */
@DatabaseSetup("classpath:/sample-data-limitation.xml")
public class SubjectToRoomCompatibilityTest extends AbstractResourceIntegrationTest {

    @Test
    public void shouldHaveValueProviders() throws Exception {
        mvc.perform(get("/api/limitations/00000000-0103-0000-0000-000000001111"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._links.subject.href", containsString("/subject")))
                .andExpect(jsonPath("$._links.room.href", containsString("/room")));
    }

    @Test
    public void shouldUpdateSubjectProperty() throws Exception {
        mvc.perform(put("/api/limitations/00000000-0103-0000-0000-000000001111/subject")
                        .contentType("text/uri-list")
                        .content("/subjects/00000000-0023-0000-0000-00000000dddd")
                        .with(csrf()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNoContent());
        mvc.perform(put("/api/limitations/00000000-0103-0000-0000-000000001111/room")
                        .contentType("text/uri-list")
                        .content("/rooms/00000000-0002-0000-0000-00000000aaaa")
                        .with(csrf()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNoContent());
    }
}
