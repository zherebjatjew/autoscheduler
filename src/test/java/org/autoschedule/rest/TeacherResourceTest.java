package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Teacher rest repository test.
 */
@Slf4j
@DatabaseSetup("classpath:/sample-data-rest.xml")  // simply update uuids to distinguish from ids.
public class TeacherResourceTest extends AbstractResourceIntegrationTest {

	@Test
	public void shouldExposeRoot() throws Exception {
		mvc.perform(get("/api"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.teachers.href", notNullValue()));
	}

	@Test
	public void shouldExposeCollection() throws Exception {
		mvc.perform(get("/api/teachers"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.teachers[1].name", notNullValue()));
	}

	@Test
	public void shouldGetById() throws Exception {
		mvc.perform(get("/api/teachers/00000000-0100-0000-0000-00000000cccc"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("Tana Lall")))
				.andExpect(jsonPath("$._links.subjects.href", containsString("subjects")));
	}

	@Test
	public void shouldRespondNotFound() throws Exception {
		mvc.perform(get("/api/teachers/00000000-0100-ffff-0000-00000000cccc"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
}
