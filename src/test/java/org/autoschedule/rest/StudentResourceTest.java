package org.autoschedule.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Student rest repository test.
 */
@Slf4j
//@DatabaseSetup("classpath:/sample-data-rest.xml")  // simply update uuids to distinguish from ids.
public class StudentResourceTest extends AbstractResourceIntegrationTest {

	@Test
	public void shouldExposeRoot() throws Exception {
		mvc.perform(get("/api/"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.students.href", notNullValue()));
	}

	@Test
	public void shouldExposeCollection() throws Exception {
		mvc.perform(get("/api/students"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.students[1].name", notNullValue()));
	}

	@Test
	public void shouldGetById() throws Exception {
		mvc.perform(get("/api/students/00000000-1000-0000-0000-00000000bbbb"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("Elke Shotts")));
	}

	@Test
	public void shouldRespondNotFound() throws Exception {
		mvc.perform(get("/api/students/00000000-1000-ffff-0000-00000000bbbb"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
}
