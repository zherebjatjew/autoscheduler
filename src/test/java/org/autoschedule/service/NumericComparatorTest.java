package org.autoschedule.service;

import org.autoschedule.util.NumericComparator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumericComparatorTest {
    @Test
    public void shouldCompareNumbers() {
        assertEquals(0, NumericComparator.compare("10", "010"));
        assertEquals(1, NumericComparator.compare("1", "0"));
        assertEquals(-1, NumericComparator.compare("1", "2"));
    }

    @Test
    public void shouldCompareStartingWithNumber() {
        assertEquals(1, NumericComparator.compare("10A", "1A"));
        assertEquals(1, NumericComparator.compare("11B", "11A"));
    }

    @Test
    public void shouldCompareNonNumeric() {
        assertEquals(0, NumericComparator.compare("abc", "abc"));
        assertEquals(-1, NumericComparator.compare("abc", "abcd"));
        assertEquals(-1, NumericComparator.compare("Abc", "abc"));
    }

    @Test
    public void shouldCompareEmptyStrings() {
        assertEquals(0, NumericComparator.compare("", ""));
        assertEquals(-1, NumericComparator.compare("", "1"));
        assertEquals(1, NumericComparator.compare("1", ""));
    }

    @Test
    public void shouldCompareNullStrings() {
        assertEquals(0, NumericComparator.compare(null, null));
        assertEquals(-1, NumericComparator.compare(null, "1"));
        assertEquals(1, NumericComparator.compare("1", null));
    }

    @Test
    public void shouldCompareMixed() {
        assertEquals(1, NumericComparator.compare("10A", "1A"));
        assertEquals(-1, NumericComparator.compare("10A", "12"));
        assertEquals(1, NumericComparator.compare("abc12", "abc1"));
        assertEquals(-1, NumericComparator.compare("abc10", "abc12"));
        assertEquals(1, NumericComparator.compare("abc12", "abc1ff"));
        assertEquals(-1, NumericComparator.compare("abc10ff", "abc12"));
    }
}
