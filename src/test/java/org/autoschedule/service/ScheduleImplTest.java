package org.autoschedule.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.autoschedule.domain.Event;
import org.autoschedule.api.Index;
import org.autoschedule.api.Schedule;
import org.autoschedule.domain.AbstractDaoIntegrationTest;
import org.autoschedule.domain.EventRepository;
import org.autoschedule.service.impl.ScheduleImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * ScheduleImpl test.
 */
@DatabaseSetup("classpath:/sample-data-schedule.xml")
public class ScheduleImplTest extends AbstractDaoIntegrationTest {

	@Autowired
	private EventRepository repository;

	@Test
	public void shouldReturnEmpty() {
		assertTrue(new ScheduleImpl().isEmpty());
	}

	@Test
	public void shouldReturnNotEmpty() {
		assertFalse(new ScheduleImpl(Collections.singletonList(new Event())).isEmpty());
	}

	@Test
	public void shouldSplitByHours() {
		final Iterable<Schedule> schedules = new ScheduleImpl(
				StreamSupport.stream(repository.findAll().spliterator(), false)
						.collect(Collectors.toList())).byHours();
		assertThat(schedules, containsInAnyOrder(iterableWithSize(2), iterableWithSize(2), iterableWithSize(1)));
		// Assert that each item contains events of the same hour.
		schedules.forEach(hour -> {
			Index position = null;
			for (final Event item : hour) {
				if (position == null) {
					position = item.getPosition();
				} else {
					assertEquals(position, item.getPosition());
				}
			}
		});
	}
}
