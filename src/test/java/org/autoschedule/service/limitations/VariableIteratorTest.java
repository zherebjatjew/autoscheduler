package org.autoschedule.service.limitations;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.autoschedule.service.VariableIterator;
import org.autoschedule.service.VarAccessor;
import org.autoschedule.service.SolverContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VariableIteratorTest
{
	@Mock
	private SolverContext context;

	@Before
	public void setup()
	{
		when(context.getVar(any(String.class))).thenReturn(null);
	}

	@Test
	public void shouldIterate() {
		final VariableIterator iterator = new VariableIterator(context, VarAccessor.VARTYPE.GROUP,
				IntStream.range(1, 3).boxed().collect(Collectors.toSet()),
				IntStream.range(1, 2).boxed().collect(Collectors.toSet()),
				IntStream.range(100, 101).boxed().collect(Collectors.toSet()));
		int count = 0;
		while (iterator.hasNext()) {
			count++;
			iterator.next();
		}
		assertEquals((101-100)*(2-1)*(3-1), count);
	}
}
