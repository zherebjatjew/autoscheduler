package org.autoschedule.service.limitations;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableInt;
import org.autoschedule.ApplicationConfig;
import org.autoschedule.api.*;
import org.autoschedule.domain.*;
import org.autoschedule.domain.AbstractDaoIntegrationTest;
import org.autoschedule.service.SchedulerService;
import org.autoschedule.service.limitations.custom.TeacherLoadLimitation;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.*;

/**
 * Apply all limitations, then check evey limitations separately.
 */
@Slf4j
public class IntegratedLimitationTest extends AbstractDaoIntegrationTest {

	@Autowired
	private SchedulerService manager;
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private TeamRepository teamRepository;
	@Autowired
	private ApplicationConfig configuration;

    private static Plan plan;

	private static List<Schedule> solutions;

	@Before
	public void solve() {
		if (!solved()) {
			plan = planRepository.findOne(UUID.fromString("00000000-0003-0000-0000-00000000eeee"));
			solutions = manager.build(plan);
		}
	}


	@Test
	public void shouldHaveSolutions() {
		assertFalse(solutions.isEmpty());
	}

	@Test
	public void groupsShouldNotOverlap() {
		solutions.forEach(it -> assertDistinct(it, event -> event.getTeam().getId()));
	}

	@Test
	public void teachersShouldNotOverlap() {
		solutions.forEach(it -> assertDistinct(it, event -> event.getTeacher().getId()));
	}

	@Test
	public void groupShouldHaveRequiredNumberOfHours() {
		solutions.forEach(solution ->
            planRepository.getHourSubtotals(plan.getId()).forEach(hour -> {
                final long count = StreamSupport.stream(solution.spliterator(), false)
                        .filter(it -> hour.getTeam().equals(it.getTeam()))
                        .filter(it -> hour.getSubject().equals(it.getSubject()))
                        .count();
                assertEquals((long) hour.getHours(), count);
            })
        );
	}

	@Test
	public void groupShouldFitIntoRooms() {
		solutions.forEach(it ->
						it.forEach(event -> assertTrue(teamRepository.findOne(event.getTeam().getId()).count() <= event.getRoom().getCapacity()))
		);
	}

	@Test
	public void mathShouldBeInOtherDayThanGym() {
		solutions.forEach(solution -> StreamSupport.stream(solution.spliterator(), false)
                .map(Event::getTeam)
                .distinct()
                .forEach(team -> solution.byDays().forEach(day -> {
                    final boolean hasMath = StreamSupport.stream(day.spliterator(), false)
                            .filter(it -> it.getTeam().equals(team))
                            .map(it -> it.getSubject().getId())
                            .filter(it -> UUID.fromString("00000000-0018-0000-0000-00000000dddd").equals(it))
                            .count() > 0;
                    final boolean hasSwimming = StreamSupport.stream(day.spliterator(), false)
                            .filter(it -> it.getTeam().equals(team))
                            .map(it -> it.getSubject().getId())
                            .filter(it -> UUID.fromString("00000000-0023-0000-0000-00000000dddd").equals(it))
                            .count() > 0;
                    if (hasMath || hasSwimming) {
                        assertFalse(hasMath == hasSwimming);
                    }
                })));
	}

	@Test
	public void shouldSplitByHours() {
		solutions.forEach(solution -> solution.byHours().forEach(
				hour -> assertEquals(1, StreamSupport.stream(hour.spliterator(), false)
						.map(it -> it.getPosition().getRaw())
						.collect(Collectors.toSet()).size())));
	}

	/**
	 * Ensure that one student does not have two classes contemporaneously.
	 */
	@Test
	public void groupShouldNotInterfere() {
		solutions.forEach(solution -> {
			final Iterable<Schedule> hours = solution.byHours();
			hours.forEach(hour -> {
				final Map<UUID, Set<Student>> teams = new HashMap<>();
				final MutableInt count = new MutableInt(0);
				hour.forEach(event -> {
					final UUID teamId = event.getTeam().getId();
					if (!teams.containsKey(teamId)) {
						final Set<Student> students = event.getTeam().getStudents();
						teams.put(teamId, students);
						count.add(students.size());
					}
				});
				final Set<Student> fullList = new HashSet<>();
				teams.values().forEach(fullList::addAll);
				assertEquals(count.getValue().intValue(), fullList.size());
			});
		});
	}

	@Test
	public void teachersShouldNotInterfere() {
		solutions.forEach(solution -> {
			final Iterable<Schedule> hours = solution.byHours();
			hours.forEach(hour -> {
				final Set<Teacher> teachers = StreamSupport.stream(hour.spliterator(), false)
						.map(Event::getTeacher)
						.collect(Collectors.toSet());
				int count = 0;
				for (final Event ignored : hour) {
					count++;
				}
				assertEquals(count, teachers.size());
			});
		});
	}

	/**
	 * Test {@link TeacherLoadLimitation} constraint.
	 */
	@Test
	public void teacherLoadShouldBeControlled() {
		solutions.forEach(solution ->
			assertThat(StreamSupport.stream(solution.spliterator(), false)
					.filter(event -> event.getTeacher().getId().equals(UUID.fromString("00000000-0100-0000-0000-00000000cccc")))
                            .count(), greaterThanOrEqualTo(1L))
		);
	}

	@Test
	public void teacherBusyShouldBeRespected() {
		solutions.forEach(solution ->
			assertThat(StreamSupport.stream(solution.spliterator(), false)
					.filter(event -> event.getPosition().getHour() == 3)
					.filter(event -> event.getTeacher().getId().equals(UUID.fromString("00000000-0101-0000-0000-00000000cccc")))
                            .count(), is(0L))
		);
	}

	@Test
	public void subjectShouldGoInARow() {
		solutions.forEach(solution ->
						assertThat(StreamSupport.stream(solution.spliterator(), false)
								.map(it -> it.getSubject().getId())
								.collect(Collectors.toList()), hasSequenceLength(2, UUID.fromString("00000000-0023-0000-0000-00000000dddd")))
		);
	}

	@Test
	public void subjectSequenceShouldHaveConsistentRooms() {
		final UUID subjectId = UUID.fromString("00000000-0023-0000-0000-00000000dddd");
		solutions.stream().forEach(solution -> {
			final List<Event> events = StreamSupport.stream(solution.spliterator(), true)
					.filter(event -> event.getSubject().getId().equals(subjectId))
					.collect(Collectors.toList());
			assertThat(events, iterableWithSize(2));
            assertEquals(1, Math.abs(events.get(0).getPosition().getRaw() - events.get(1).getPosition().getRaw()));
			assertEquals(events.get(0).getRoom(), events.get(1).getRoom());
			assertEquals(events.get(0).getTeacher(), events.get(1).getTeacher());
			assertEquals(events.get(0).getTeam(), events.get(1).getTeam());
		});
	}

	@Test
	public void subjectsShouldBeSetToApplicableRooms() {
		final List<UUID> allowedRooms = Arrays.asList(
                UUID.fromString("00000000-0002-0000-0000-00000000aaaa"),
                UUID.fromString("00000000-0004-0000-0000-00000000aaaa"));
		solutions.forEach(solution ->
				assertEquals(0, StreamSupport.stream(solution.spliterator(), false)
						.filter(it -> it.getSubject().getId().equals(UUID.fromString("00000000-0023-0000-0000-00000000dddd")))
                        .map(Event::getRoom)
                        .map(Room::getId)
                        .filter(it -> !allowedRooms.contains(it))
						.count())
        );
	}

	@Test
	public void groupsShouldHaveNoWindows() {
        final Iterable<Team> teams = teamRepository.findAll();
        solutions.forEach(solution -> {
            for (int i = 0; i < configuration.getPeriod(); i++) {
                final Schedule day = solution.getSchedule(new Index(i, 0), new Index(i, 12));
                teams.forEach(team -> {
                    final int[] hrs = StreamSupport.stream(day.byHours().spliterator(), false)
                            .map(it -> it.iterator().next())
                            .filter(it -> it.getTeam().getId().equals(team.getId()))
                            .map(Event::getPosition)
                            .mapToInt(Index::getHour)
                            .toArray();
                    if (hrs.length > 0) {
                        assertEquals(0, hrs[0]);
                        for (int j = 1; j < hrs.length; j++) {
                            assertEquals(hrs[j - 1], hrs[j] - 1);
                        }
                    }
                });
            }
        });
    }

	@Test
	public void teachersShouldHaveNoWindows() {
        final Iterable<Team> teams = teamRepository.findAll();
        solutions.forEach(solution -> {
            for (int i = 0; i < configuration.getPeriod(); i++) {
                final Schedule day = solution.getSchedule(new Index(i, 0), new Index(i, 12));
                teams.forEach(team -> {
                    final int[] hrs = StreamSupport.stream(day.byHours().spliterator(), false)
                            .map(it -> it.iterator().next())
                            .filter(it -> it.getTeacher().getId().equals(team.getId()))
                            .map(Event::getPosition)
                            .mapToInt(Index::getHour)
                            .toArray();
                    if (hrs.length > 0) {
                        assertEquals(0, hrs[0]);
                        for (int j = 1; j < hrs.length; j++) {
                            assertEquals(hrs[j - 1], hrs[j] - 1);
                        }
                    }
                });
            }
        });
    }

	@SuppressWarnings("unchecked")
	private static Matcher<? super Iterable> hasSequenceLength(final int count, final Object id) {
		return new Matcher<Iterable>() {
			private int res = 0;

			@Override
			public boolean matches(Object list) {
				final Iterable<UUID> events = (Iterable<UUID>) list;
				int length = 0;
				for (final UUID item : events) {
					if (item.equals(id)) {
						length++;
					} else {
						if (length != 0 && length != count) {
							res = length;
							return false;
						}
						length = 0;
					}
				}
				return length == 0 || length == count;
			}

			@Override
			public void describeMismatch(Object o, Description description) {
				description.appendText("Found sequence of <")
						.appendValue(res)
						.appendText("> values of ")
						.appendValue(id)
						.appendText("> while expected sequence length is <")
						.appendValue(count)
						.appendText("> or <0>.");
			}

			@SuppressWarnings("deprecated")
			@Override
			public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Searches for max-length sequence of given values and matches against the value expected");
			}
		};
	}


	private boolean solved() {
		return solutions != null;
	}

	private void assertDistinct(final Schedule it, Function<Event, UUID> getter) {
		int i = 0;
		it.byHours().forEach(hour -> {
			final Map<UUID, UUID> found = new HashMap<>();
			for (final Event event : hour) {
				final UUID groupId = getter.apply(event);
				assertIsFirst(groupId, found);
			}
		});
		while (true) {
			final Schedule day = it.getSchedule(new Index(i, 0), new Index(i, 10));
			i++;
			if (day.isEmpty()) {
				break;
			}
		}
	}

	private void assertIsFirst(final UUID value, final Map<UUID, UUID> in) {
		final UUID existing = in.put(value, value);
		assertTrue("Overlapping id " + value, existing == null || existing.equals(value));
	}
}
