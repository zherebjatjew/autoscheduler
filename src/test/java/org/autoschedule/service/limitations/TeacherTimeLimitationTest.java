package org.autoschedule.service.limitations;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.autoschedule.domain.AbstractDaoIntegrationTest;
import org.autoschedule.domain.Limitation;
import org.autoschedule.domain.LimitationRepository;
import org.autoschedule.domain.Teacher;
import org.autoschedule.domain.TeacherRepository;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@Slf4j
@DatabaseSetup("classpath:/sample-data-teacher.xml")
public class TeacherTimeLimitationTest extends AbstractDaoIntegrationTest {

    @Resource
    private TeacherRepository teacherRepository;
    @Resource
    private LimitationRepository limitationRepository;

    @Test
    public void shouldLoadConstraints() {
        final Teacher teacher = teacherRepository.findOne(UUID.fromString("00000000-0100-0000-0000-00000000cccc"));
        assertThat(teacher.getLimitations(), hasItem(hasProperty("id", is(UUID.fromString("00000000-0101-0000-0000-000000001111")))));
    }

    @Test
    public void teacherShouldRemoveLimitations() {
        // Requires non-null version of limitations to prevent OptimisticLockFailure
        final Teacher teacher = teacherRepository.findOne(UUID.fromString("00000000-0100-0000-0000-00000000cccc"));
        teacher.getLimitations().clear();
        teacherRepository.save(teacher);
        final Iterable<Limitation> limitations = limitationRepository.findAll();
        assertThat(limitations, not(hasItem(hasProperty("id", is(UUID.fromString("00000000-0101-0000-0000-000000001111"))))));
    }
}
