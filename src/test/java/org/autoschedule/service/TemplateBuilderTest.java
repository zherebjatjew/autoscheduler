package org.autoschedule.service;

import org.autoschedule.service.util.TemplateBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemplateBuilderTest {
    @Test
    public void shouldReplaceSingleRef() {
        assertEquals(
                "This is fine",
                new TemplateBuilder().attribute("test", "fine").build("This is ${test}"));
    }

    @Test
    public void shouldDistinguishRefs() {
        assertEquals(
                "This is fine",
                new TemplateBuilder().attribute("test", "fine").attribute("test2", "bad").build("This is ${test}")
        );
    }

    @Test
    public void shouldIgnoreCase() {
        assertEquals(
                "This is fine",
                new TemplateBuilder().attribute("TEsT", "fine").build("This is ${test}"));
        assertEquals(
                "This is fine",
                new TemplateBuilder().attribute("test", "bad").attribute("TEsT", "fine").build("This is ${test}"));
    }

    @Test
    public void shouldAcceptNullValues() {
        assertEquals(
                "This is ",
                new TemplateBuilder().attribute("test", null).build("This is ${test}")
        );
    }

    @Test
    public void shouldAcceptNullTempalte() {
        assertEquals(
                "",
                new TemplateBuilder().attribute("test", null).build(null)
        );
    }

    @Test
    public void shouldAcceptAnyObjects() {
        assertEquals(
                "This is 42",
                new TemplateBuilder().attribute("test", 42).build("This is ${test}")
        );
    }

    @Test
    public void shouldIgnoreMalformedRefs() {
        assertEquals(
                "This is {test}",
                new TemplateBuilder().attribute("test", "fine").build("This is {test}"));
    }

    @Test
    public void shouldIgnoreEscapedRefs() {
        assertEquals(
                "This is $\\{test}",
                new TemplateBuilder().attribute("test", "fine").build("This is $\\{test}"));
        assertEquals(
                "This is \\${test}",
                new TemplateBuilder().attribute("test", "fine").build("This is \\${test}"));
        assertEquals(
                "This is ${test\\}",
                new TemplateBuilder().attribute("test", "fine").build("This is ${test\\}"));
        assertEquals(
                "This is $\\{test\\}",
                new TemplateBuilder().attribute("test", "fine").build("This is $\\{test\\}"));
        assertEquals(
                "This is \\$\\{test}",
                new TemplateBuilder().attribute("test", "fine").build("This is \\$\\{test}"));
        assertEquals(
                "This is \\$\\{test\\}",
                new TemplateBuilder().attribute("test", "fine").build("This is \\$\\{test\\}"));
    }

    @Test
    public void shouldEraseUndefinedRefs() {
        assertEquals(
                "This is ",
                new TemplateBuilder().build("This is ${test}")
        );
    }
}
