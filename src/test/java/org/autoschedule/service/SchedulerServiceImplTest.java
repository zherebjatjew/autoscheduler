package org.autoschedule.service;

import org.autoschedule.domain.Event;
import org.autoschedule.api.Index;
import org.autoschedule.domain.Plan;
import org.autoschedule.api.Schedule;
import org.autoschedule.domain.AbstractDaoIntegrationTest;
import org.autoschedule.domain.PlanRepository;
import org.autoschedule.service.impl.ScheduleImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.iterableWithSize;

/**
 * Scheduler service test.
 */
public class SchedulerServiceImplTest extends AbstractDaoIntegrationTest {

	@Autowired
	private PlanRepository dao;

	@Test
	public void shouldSaveEvents() {
		final Plan plan = dao.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee"));
		assertThat(plan.getEvents(), emptyIterable());
		final Schedule timetable = new ScheduleImpl(Arrays.asList(event(), event()));
		plan.setEvents(timetable);
		dao.save(plan);
		assertThat(dao.findOne(UUID.fromString("00000000-0002-0000-0000-00000000eeee")).getEvents(), iterableWithSize(2));
	}

	private Event event() {
		final Event result = new Event();
		result.setPosition(new Index(1));
		return result;
	}

}
