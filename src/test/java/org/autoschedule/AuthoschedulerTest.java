package org.autoschedule;

import org.autoschedule.domain.AbstractEntity;
import org.autoschedule.domain.StreamedRepository;
import org.hibernate.tool.hbm2ddl.ImportSqlCommandExtractor;
import org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Test configuration.
 */
@SpringBootApplication
@PropertySource({"classpath:/application-test.properties"})
@EnableAspectJAutoProxy
@EnableJpaRepositories(basePackageClasses = StreamedRepository.class)
@EntityScan(basePackageClasses = {AbstractEntity.class})
@ComponentScan(basePackageClasses = Autoscheduler.class)
@EnableWebSecurity
public class AuthoschedulerTest {

    @Bean
    public DefaultFormattingConversionService defaultConversionService() {
        return new DefaultFormattingConversionService();
    }

	@Bean
	public ImportSqlCommandExtractor getImportFilesSqlExtractor() {
		return new MultipleLinesSqlCommandExtractor();
	}

}
