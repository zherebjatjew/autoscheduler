#!/bin/bash

# Enable UTF8 between JPA and DB.
# Without these settings you'll have questions marks in database when saving objects,
# although direct DB edition will work fine.
#
# http://stackoverflow.com/questions/3010639/unicode-in-java-ee-and-save-question-mark-in-database
# http://mathiasrichter.blogspot.ru/2009/10/character-encoding-utf-8-with.html
# http://dev.mysql.com/doc/refman/5.5/en/server-options.html#option_mysqld_default-character-set
CONFIG=/etc/mysql/conf.d/encoding.cnf
echo '[mysqld]' > ${CONFIG}
echo 'init_connect=\'SET collation_connection = utf8_general_ci\'' >> ${CONFIG}
echo 'init_connect=\'SET NAMES utf8\'' >> ${CONFIG}
echo 'character-set-server=utf8' >> ${CONFIG}
echo 'collation-server=utf8_general_ci' >> ${CONFIG}

# Create empty database.
# Use /admin/init-db or /admin/sampledata to fill the database with objects.
echo "drop database if exists autoscheduler;
    create database autoscheduler;
    alter database autoscheduler default character set='UTF8';
    use autoscheduler;
    set names 'utf8';" | mysql -u root -proot